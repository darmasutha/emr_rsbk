﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Converter
{
    public static class IAttributes
    {
        public static string TableName<T>(this T model) where T : class
        {
            var result = ((TableAttribute)model.GetType().GetCustomAttributes(typeof(TableAttribute), true).First()).Name;
            return result;
        }

        public static string DisplayName<T>(this T model, string propertyName, bool uppercase = false) where T : class
        {
            var prop = model.GetType().GetProperty(propertyName);
            if (prop == null) throw new Exception("propertyName not found");
            var result = ((DisplayAttribute)prop.GetCustomAttributes(typeof(DisplayAttribute), true).First()).Name;
            return uppercase ? result.ToUpper() : result;
        }

        public static string ColumnName<T>(this T model, string propertyName, bool withTableName = false) where T : class
        {
            var prop = model.GetType().GetProperty(propertyName);
            if (prop == null) throw new Exception("propertyName not found");
            var result = ((ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).First()).Name;
            return withTableName ? $"{model.TableName()}.{result}" : result;
        }

        public static string DisplayName<T>(this T model, Expression<Func<T, object>> expression, bool uppercase = false) where T : class
        {
            string propNama;
            try { propNama = ((MemberExpression)expression.Body).Member.Name; }
            catch { propNama = ((MemberExpression)((UnaryExpression)expression.Body).Operand).Member.Name; }
            var prop = model.GetType().GetProperty(propNama);
            if (prop == null) throw new Exception("propertyName not found");
            var result = ((DisplayAttribute)prop.GetCustomAttributes(typeof(DisplayAttribute), true).First()).Name;
            return uppercase ? result.ToUpper() : result;
        }

        public static string ColumnName<T>(this T model, Expression<Func<T, object>> expression, bool withTableName = true) where T : class
        {
            string propNama;
            try { propNama = ((MemberExpression)expression.Body).Member.Name; }
            catch { propNama = ((MemberExpression)((UnaryExpression)expression.Body).Operand).Member.Name; }
            var prop = model.GetType().GetProperty(propNama);
            if (prop == null) throw new Exception("propertyName not found");
            var result = ((ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).First()).Name;
            return withTableName ? $"{model.TableName()}.{result}" : result;
        }
    }
}