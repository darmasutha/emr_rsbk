﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Property
{
    public class ResultStatus
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public int Count { get; set; }

        public string Status { get; set; }

        public object Data { get; set; }

        public ResultStatus() { }

        public ResultStatus(object resultss)
        {
            var typ = resultss.GetType();
            IsSuccess = (bool)typ.GetProperty("IsSuccess").GetValue(resultss);
            Message = (string)typ.GetProperty("Message").GetValue(resultss);
            Count = (int)typ.GetProperty("Count").GetValue(resultss);
        }

        public void Error(string message)
        {
            IsSuccess = false;
            Message = message;
        }
    }
}
