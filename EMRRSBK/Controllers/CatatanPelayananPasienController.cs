﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanPelayananPasienController : Controller
    {
        // GET: CatatanPelayananPasien
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanPelayananPasien.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.CttPlyananPsien = IConverter.Cast<CatatanPelayananPasienViewModel>(item);
                            var ttdperawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.TandaTanganPerawatYgMemulangkan);
                            if (ttdperawat != null)
                            {
                                model.CttPlyananPsien.TandaTanganPerawatYgMemulangkanNama = ttdperawat.NamaDOkter;
                            }
                            var ttdapotik = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.TandaTanganApotik);
                            if (ttdapotik != null)
                            {
                                model.CttPlyananPsien.TandaTanganApotikNama = ttdapotik.NamaDOkter;
                            }
                            var ttdverifikator = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.TandaTanganVerivikator);
                            if (ttdverifikator != null)
                            {
                                model.CttPlyananPsien.TandaTanganVerivikatorNama = ttdverifikator.NamaDOkter;
                            }
                            var ttdkasir = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.TandaTanganKasir);
                            if (ttdkasir != null)
                            {
                                model.CttPlyananPsien.TandaTanganKasirNama = ttdkasir.NamaDOkter;
                            }
                            model.CttPlyananPsien.NamaPasien = m.NamaPasien;
                            model.CttPlyananPsien.NoReg = noreg;
                            model.CttPlyananPsien.NRM = model.NRM;
                            model.CttPlyananPsien.SectionID = sectionid;
                            model.CttPlyananPsien.Report = 1;
                            model.CttPlyananPsien.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new CatatanPelayananPasien();
                            model.CttPlyananPsien = IConverter.Cast<CatatanPelayananPasienViewModel>(item);
                            model.CttPlyananPsien.Report = 0;
                            model.CttPlyananPsien.NamaPasien = m.NamaPasien;
                            model.CttPlyananPsien.NoReg = noreg;
                            model.CttPlyananPsien.NRM = model.NRM;
                            model.CttPlyananPsien.SectionID = sectionid;
                            model.CttPlyananPsien.Tanggal = DateTime.Today;
                            model.CttPlyananPsien.Jam = DateTime.Now;
                            model.CttPlyananPsien.TglLahir = m.TglLahir;
                        }

                        #region Detail Ruangan Pewarawatan
                        var List = eEMR.RuangPerawatan_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.CttPlyananPsien.RuangPrwt_List = new ListDetail<RuangPerawatanModelDetail>();

                            foreach (var x1 in List)
                            {
                                var y = IConverter.Cast<RuangPerawatanModelDetail>(x1);
                                var ttdmasuk = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.ParafVerivikator_RuangPerawatan);

                                if (ttdmasuk != null)
                                {
                                    y.ParafVerivikator_RuangPerawatanNama = ttdmasuk.NamaDOkter;
                                }
                                model.CttPlyananPsien.RuangPrwt_List.Add(false, y);
                            }

                        }
                        #endregion

                        #region Detail Pemeriksaan Penunjang
                        var List1 = eEMR.PemeriksaanPenunjang_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List1 != null)
                        {

                            model.CttPlyananPsien.PmrksaanPenunjang_List = new ListDetail<PemeriksaanPenunjangRIModelDetail>();

                            foreach (var x1 in List1)
                            {
                                var y = IConverter.Cast<PemeriksaanPenunjangRIModelDetail>(x1);
                                var ttdmasuk = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.ParafVerivikator_PemeriksaanPenunjang);

                                if (ttdmasuk != null)
                                {
                                    y.ParafVerivikator_PemeriksaanPenunjangNama = ttdmasuk.NamaDOkter;
                                }
                                model.CttPlyananPsien.PmrksaanPenunjang_List.Add(false, y);
                            }

                        }
                        #endregion

                        #region Detail Tindakan Kedokteran
                        var List2 = eEMR.TindakanKedokteran_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List2 != null)
                        {

                            model.CttPlyananPsien.TindakanKeprwtnPemeriksa_List = new ListDetail<TindakanKedokteranRIModelDetail>();

                            foreach (var x1 in List2)
                            {
                                var y = IConverter.Cast<TindakanKedokteranRIModelDetail>(x1);
                                var ttdmasuk = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Petugas_TindakanKedokteranKeperawatan);

                                if (ttdmasuk != null)
                                {
                                    y.Petugas_TindakanKedokteranKeperawatanNama = ttdmasuk.NamaDOkter;
                                }

                                var ttdmasuk1 = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.ParafVerivikator_TindakanKedokteranKeperawatan);

                                if (ttdmasuk1 != null)
                                {
                                    y.ParafVerivikator_TindakanKedokteranKeperawatanNama = ttdmasuk1.NamaDOkter;
                                }
                                model.CttPlyananPsien.TindakanKeprwtnPemeriksa_List.Add(false, y);
                            }

                        }
                        #endregion

                        #region Detail Catatan Tambahan
                        var List3 = eEMR.CatatanTambahan_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List3 != null)
                        {

                            model.CttPlyananPsien.CttTmbhn_List = new ListDetail<CatatanTambahanViewModelDetail>();

                            foreach (var x1 in List3)
                            {
                                var y = IConverter.Cast<CatatanTambahanViewModelDetail>(x1);
                                var ttdpetugas = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Petugas);
                                if (ttdpetugas != null)
                                {
                                    y.PetugasNama = ttdpetugas.NamaDOkter;
                                }

                                var ttdparaf = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.ParafVerifikator);
                                if (ttdparaf != null)
                                {
                                    y.ParafVerifikatorNama = ttdparaf.NamaDOkter;
                                }
                                model.CttPlyananPsien.CttTmbhn_List.Add(false, y);
                            }

                        }
                        #endregion
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanPelayananPasien_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.CttPlyananPsien.NoReg = item.CttPlyananPsien.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CttPlyananPsien.SectionID = sectionid;
                            item.CttPlyananPsien.NRM = item.CttPlyananPsien.NRM;
                            var model = eEMR.CatatanPelayananPasien.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.CttPlyananPsien.NoReg);

                            if (model == null)
                            {
                                model = new CatatanPelayananPasien();
                                var o = IConverter.Cast<CatatanPelayananPasien>(item.CttPlyananPsien);
                                eEMR.CatatanPelayananPasien.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanPelayananPasien>(item.CttPlyananPsien);
                                eEMR.CatatanPelayananPasien.AddOrUpdate(model);
                            }

                            #region ===== Detail Ruangan Perawatan

                            if (item.CttPlyananPsien.RuangPrwt_List == null) item.CttPlyananPsien.RuangPrwt_List = new ListDetail<RuangPerawatanModelDetail>();
                            item.CttPlyananPsien.RuangPrwt_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttPlyananPsien.RuangPrwt_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttPlyananPsien.NoReg;
                                x.Model.NRM = item.CttPlyananPsien.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.CttPlyananPsien.RuangPrwt_List;
                            var real_List = eEMR.RuangPerawatan_Detail.Where(x => x.SectionID == item.CttPlyananPsien.SectionID && x.NoReg == item.CttPlyananPsien.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.RuangPerawatan_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new RuangPerawatan_Detail();
                                    eEMR.RuangPerawatan_Detail.Add(IConverter.Cast<RuangPerawatan_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<RuangPerawatan_Detail>(x.Model);
                                    eEMR.RuangPerawatan_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion

                            #region ===== Detail Pemeriksaan Penunjang

                            if (item.CttPlyananPsien.PmrksaanPenunjang_List == null) item.CttPlyananPsien.PmrksaanPenunjang_List = new ListDetail<PemeriksaanPenunjangRIModelDetail>();
                            item.CttPlyananPsien.PmrksaanPenunjang_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttPlyananPsien.PmrksaanPenunjang_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttPlyananPsien.NoReg;
                                x.Model.NRM = item.CttPlyananPsien.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;
                                x.Model.Tgl_PemeriksaanPenunjang = DateTime.Now;

                            }
                            var new_List1 = item.CttPlyananPsien.PmrksaanPenunjang_List;
                            var real_List1 = eEMR.PemeriksaanPenunjang_Detail.Where(x => x.SectionID == item.CttPlyananPsien.SectionID && x.NoReg == item.CttPlyananPsien.NoReg).ToList();
                            foreach (var x in real_List1)
                            {
                                var z = new_List1.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.PemeriksaanPenunjang_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List1)
                            {

                                var _m = real_List1.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new PemeriksaanPenunjang_Detail();
                                    eEMR.PemeriksaanPenunjang_Detail.Add(IConverter.Cast<PemeriksaanPenunjang_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<PemeriksaanPenunjang_Detail>(x.Model);
                                    eEMR.PemeriksaanPenunjang_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion

                            #region ===== Detail Tindakan Kedokteran

                            if (item.CttPlyananPsien.TindakanKeprwtnPemeriksa_List == null) item.CttPlyananPsien.TindakanKeprwtnPemeriksa_List = new ListDetail<TindakanKedokteranRIModelDetail>();
                            item.CttPlyananPsien.TindakanKeprwtnPemeriksa_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttPlyananPsien.TindakanKeprwtnPemeriksa_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttPlyananPsien.NoReg;
                                x.Model.NRM = item.CttPlyananPsien.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;

                            }
                            var new_List2 = item.CttPlyananPsien.TindakanKeprwtnPemeriksa_List;
                            var real_List2 = eEMR.TindakanKedokteran_Detail.Where(x => x.SectionID == item.CttPlyananPsien.SectionID && x.NoReg == item.CttPlyananPsien.NoReg).ToList();
                            foreach (var x in real_List2)
                            {
                                var z = new_List2.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.TindakanKedokteran_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List2)
                            {

                                var _m = real_List2.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new TindakanKedokteran_Detail();
                                    eEMR.TindakanKedokteran_Detail.Add(IConverter.Cast<TindakanKedokteran_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<TindakanKedokteran_Detail>(x.Model);
                                    eEMR.TindakanKedokteran_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion

                            #region ===== Detail Catatan Tamabahan

                            if (item.CttPlyananPsien.CttTmbhn_List == null) item.CttPlyananPsien.CttTmbhn_List = new ListDetail<CatatanTambahanViewModelDetail>();
                            item.CttPlyananPsien.CttTmbhn_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttPlyananPsien.CttTmbhn_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttPlyananPsien.NoReg;
                                x.Model.NRM = item.CttPlyananPsien.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;
                                x.Model.TglUraian = DateTime.Now;

                            }
                            var new_List3 = item.CttPlyananPsien.CttTmbhn_List;
                            var real_List3 = eEMR.CatatanTambahan_Detail.Where(x => x.SectionID == item.CttPlyananPsien.SectionID && x.NoReg == item.CttPlyananPsien.NoReg).ToList();
                            foreach (var x in real_List3)
                            {
                                var z = new_List3.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanTambahan_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List3)
                            {

                                var _m = real_List3.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new CatatanTambahan_Detail();
                                    eEMR.CatatanTambahan_Detail.Add(IConverter.Cast<CatatanTambahan_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<CatatanTambahan_Detail>(x.Model);
                                    eEMR.CatatanTambahan_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanPelayananPasien(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanPelayananPasien.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanPelayananPasien", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanPelayananPasien;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_RuangPerawatan_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_RuangPerawatan_Detail;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_PemeriksaanPenunjang_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_PemeriksaanPenunjang_Detail;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_TindakanKedokteran_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_TindakanKedokteran_Detail;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanTambahan_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanTambahan_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}