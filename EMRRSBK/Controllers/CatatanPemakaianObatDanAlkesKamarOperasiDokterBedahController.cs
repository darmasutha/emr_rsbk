﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahController : Controller
    {
        // GET: CatatanPemakaianObatDanAlkesKamarOperasiDokterBedah

        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanPemakaianObatdanAlkes.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.CttObatDanAlksKmr = IConverter.Cast<CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterBedah);
                            if (dokter != null)
                            {
                                model.CttObatDanAlksKmr.DokterBedahNama = dokter.NamaDOkter;
                            }

                            var dokter1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PetugasOK);
                            if (dokter1 != null)
                            {
                                model.CttObatDanAlksKmr.PetugasOKNama = dokter1.NamaDOkter;
                            }
                            model.CttObatDanAlksKmr.Report = 1;
                            model.CttObatDanAlksKmr.NamaPasien = m.NamaPasien;
                            var fingercttobatalkes = eEMR.CatatanPemakaianObatdanAlkes.Where(x => x.DokterBedah == model.CttObatDanAlksKmr.DokterBedah && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingercttobatalkes != null)
                            {
                                model.CttObatDanAlksKmr.SudahRegDokter = 1;
                            }
                            else
                            {
                                model.CttObatDanAlksKmr.SudahRegDokter = 0;
                            }
                            if (model.CttObatDanAlksKmr.DokterBedah != null)
                            {
                                ViewBag.UrlFingerDokter = GetEncodeUrlDokter(model.CttObatDanAlksKmr.DokterBedah, model.CttObatDanAlksKmr.NoReg, sectionid);
                            }

                            var fingercttobatalkespetugas = eEMR.CatatanPemakaianObatdanAlkes.Where(x => x.PetugasOK == model.CttObatDanAlksKmr.PetugasOK && x.TandaTanganPetugas == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingercttobatalkespetugas != null)
                            {
                                model.CttObatDanAlksKmr.SudahRegPetugas = 1;
                            }
                            else
                            {
                                model.CttObatDanAlksKmr.SudahRegPetugas = 0;
                            }
                            if (model.CttObatDanAlksKmr.PetugasOK != null)
                            {
                                ViewBag.UrlFingerPetugas = GetEncodeUrlPetugas(model.CttObatDanAlksKmr.PetugasOK, model.CttObatDanAlksKmr.NoReg, sectionid);
                            }

                            var fingercttobatalkespasien = eEMR.CatatanPemakaianObatdanAlkes.Where(x => x.NRM == model.CttObatDanAlksKmr.NRM && x.TandaTangan == null).FirstOrDefault();
                            if (fingercttobatalkespasien != null)
                            {
                                model.CttObatDanAlksKmr.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.CttObatDanAlksKmr.SudahRegPasien = 0;
                            }
                            ViewBag.UrlFingerPasien = GetEncodeUrlPasien(model.CttObatDanAlksKmr.NRM, model.CttObatDanAlksKmr.NoReg, sectionid);
                        }
                        else
                        {
                            item = new CatatanPemakaianObatdanAlkes();
                            model.CttObatDanAlksKmr = IConverter.Cast<CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahViewModel>(item);
                            ViewBag.UrlFingerDokter = GetEncodeUrlDokter(model.CttObatDanAlksKmr.DokterBedah, model.CttObatDanAlksKmr.NoReg, sectionid);
                            ViewBag.UrlFingerPetugas = GetEncodeUrlPetugas(model.CttObatDanAlksKmr.PetugasOK, model.CttObatDanAlksKmr.NoReg, sectionid);
                            ViewBag.UrlFingerPasien = GetEncodeUrlPasien(model.CttObatDanAlksKmr.NRM, model.CttObatDanAlksKmr.NoReg, sectionid);
                            model.CttObatDanAlksKmr.NamaPasien = m.NamaPasien;
                            model.CttObatDanAlksKmr.NoReg = noreg;
                            model.CttObatDanAlksKmr.NRM = model.NRM;
                            model.CttObatDanAlksKmr.SectionID = sectionid;
                            model.CttObatDanAlksKmr.Tanggal = DateTime.Today;
                            model.CttObatDanAlksKmr.Jam = DateTime.Now;
                            model.CttObatDanAlksKmr.Report = 0;
                        }

                        var List = eEMR.CatatanPemakaianObatdanAlkes_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.CttObatDanAlksKmr.ObtAlks_List = new ListDetail<CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahModelDetail>(x);
                                var ttdmasuk = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang.ToString() == y.KodeObat);

                                if (ttdmasuk != null)
                                {
                                    y.KodeObatNama = ttdmasuk.Nama_Barang;
                                }
                                model.CttObatDanAlksKmr.ObtAlks_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanPemakaianObatDanAlkesKamarOperasiDokterBedah_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.CttObatDanAlksKmr.NoReg = item.CttObatDanAlksKmr.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CttObatDanAlksKmr.SectionID = sectionid;
                            item.CttObatDanAlksKmr.NRM = item.CttObatDanAlksKmr.NRM;
                            var model = eEMR.CatatanPemakaianObatdanAlkes.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.CttObatDanAlksKmr.NoReg);

                            if (model == null)
                            {
                                model = new CatatanPemakaianObatdanAlkes();
                                var o = IConverter.Cast<CatatanPemakaianObatdanAlkes>(item.CttObatDanAlksKmr);
                                eEMR.CatatanPemakaianObatdanAlkes.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanPemakaianObatdanAlkes>(item.CttObatDanAlksKmr);
                                eEMR.CatatanPemakaianObatdanAlkes.AddOrUpdate(model);
                            }

                            #region ===== DETAIL

                            if (item.CttObatDanAlksKmr.ObtAlks_List == null) item.CttObatDanAlksKmr.ObtAlks_List = new ListDetail<CatatanPemakaianObatDanAlkesKamarOperasiDokterBedahModelDetail>();
                            item.CttObatDanAlksKmr.ObtAlks_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttObatDanAlksKmr.ObtAlks_List)
                            {
                                x.Model.Username = User.Identity.GetUserName();
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttObatDanAlksKmr.NoReg;
                                x.Model.NRM = item.CttObatDanAlksKmr.NRM;

                            }
                            var new_List = item.CttObatDanAlksKmr.ObtAlks_List;
                            var real_List = eEMR.CatatanPemakaianObatdanAlkes_Detail.Where(x => x.SectionID == sectionid && x.NoReg == item.CttObatDanAlksKmr.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanPemakaianObatdanAlkes_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new CatatanPemakaianObatdanAlkes_Detail();
                                    eEMR.CatatanPemakaianObatdanAlkes_Detail.Add(IConverter.Cast<CatatanPemakaianObatdanAlkes_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<CatatanPemakaianObatdanAlkes_Detail>(x.Model);
                                    eEMR.CatatanPemakaianObatdanAlkes_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanPemakaianObatDanAlkesKamarOperasiDokterBedah(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanPemakaianObatdanAlkes.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanPemakaianObatdanAlkes", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanPemakaianObatdanAlkes;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanPemakaianObatdanAlkes_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanPemakaianObatdanAlkes_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlDokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanPemakaianObatAlkes/get_keycttpemakaiandokter";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlPasien(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanPemakaianObatAlkes/get_keycttpemakaiandokter";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlPetugas(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifCatatanPemakaianObatAlkes/get_keycttpemakaianpetugas";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}