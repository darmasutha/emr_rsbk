﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class CatatanPemberianObatController : Controller
    {
        // GET: CatatanPemberianObat

        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.CatatanPemberianObat.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        var getkamar = eSIM.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.CttPmbrianObt = IConverter.Cast<CatatanPemberianObatViewModel>(item);
                            model.CttPmbrianObt.Report = 1;
                        }
                        else
                        {
                            item = new CatatanPemberianObat();
                            model.CttPmbrianObt = IConverter.Cast<CatatanPemberianObatViewModel>(item);
                            model.CttPmbrianObt.NoReg = noreg;
                            model.CttPmbrianObt.NRM = model.NRM;
                            model.CttPmbrianObt.SectionID = sectionid;
                            model.CttPmbrianObt.Tanggal = DateTime.Today;
                            model.CttPmbrianObt.Report = 0;
                            model.CttPmbrianObt.Ruangan = Request.Cookies["SectionNamaPelayanan"].Value;
                            model.CttPmbrianObt.Kamar = (getkamar == null ? "-" : getkamar.Kamar);
                        }

                        var List = eEMR.CatatanPemberianObat_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.CttPmbrianObt.Pmberian_List = new ListDetail<CatatanPemberianObatModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<CatatanPemberianObatModelDetail>(x);
                                var obat = eSIM.mBarang.FirstOrDefault(z => z.Kode_Barang.ToString() == y.Obat);
                                if (obat != null)
                                {
                                    y.ObatNama = obat.Nama_Barang;
                                }

                                var paraf = eSIM.mDokter.FirstOrDefault(z => z.DokterID.ToString() == y.Paraf);
                                if (paraf != null)
                                {
                                    y.ParafNama = paraf.NamaDOkter;
                                }
                                model.CttPmbrianObt.Pmberian_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string CatatanPemberianObatKamarOperasiDokterBedah_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.CttPmbrianObt.NoReg = item.CttPmbrianObt.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.CttPmbrianObt.SectionID = sectionid;
                            item.CttPmbrianObt.NRM = item.CttPmbrianObt.NRM;
                            var model = eEMR.CatatanPemberianObat.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.CttPmbrianObt.NoReg);

                            if (model == null)
                            {
                                model = new CatatanPemberianObat();
                                var o = IConverter.Cast<CatatanPemberianObat>(item.CttPmbrianObt);
                                eEMR.CatatanPemberianObat.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<CatatanPemberianObat>(item.CttPmbrianObt);
                                eEMR.CatatanPemberianObat.AddOrUpdate(model);
                            }

                            #region ===== DETAIL

                            if (item.CttPmbrianObt.Pmberian_List == null) item.CttPmbrianObt.Pmberian_List = new ListDetail<CatatanPemberianObatModelDetail>();
                            item.CttPmbrianObt.Pmberian_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.CttPmbrianObt.Pmberian_List)
                            {
                                x.Model.Username = User.Identity.GetUserName();
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.CttPmbrianObt.NoReg;
                                x.Model.NRM = item.CttPmbrianObt.NRM;

                            }
                            var new_List = item.CttPmbrianObt.Pmberian_List;
                            var real_List = eEMR.CatatanPemberianObat_Detail.Where(x => x.SectionID == sectionid && x.NoReg == item.CttPmbrianObt.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.CatatanPemberianObat_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new CatatanPemberianObat_Detail();
                                    eEMR.CatatanPemberianObat_Detail.Add(IConverter.Cast<CatatanPemberianObat_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<CatatanPemberianObat_Detail>(x.Model);
                                    eEMR.CatatanPemberianObat_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCatatanPemberianObatKamarOperasiDokterBedah(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_CatatanPemberianObat.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_CatatanPemberianObat", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_CatatanPemberianObat;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_CatatanPemberianObat_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_CatatanPemberianObat_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}