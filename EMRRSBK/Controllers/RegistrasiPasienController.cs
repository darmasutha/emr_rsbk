﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class RegistrasiPasienController : Controller
    {
        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    using (var e = new EMR_Entities())
                    {
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        if (Request.Cookies["TipePelayanan"].Value == "RI")
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg;
                            if (filter[28] != "True")
                            {
                                p = p.Where(x => x.StatusBayar == "Belum" && x.Out == false);
                            }
                            //else
                            //{
                            //    p = p.Where(x => x.Out == false);
                            //}
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else if (Request.Cookies["TipePelayanan"].Value == "RJ" && Request.Cookies["SectionIDPelayanan"].Value != "SEC002")
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg;
                            //var sdhpernahperiksa = e.History.Where(x => x.NoReg == filter[3] && x.NRM == filter[6]).FirstOrDefault();
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            }
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                var cekwarna = e.History_ListRegistrasi.Where(a => a.SectionID == sectionid && a.NoReg == x.NoReg).ToList();
                                foreach (var z in cekwarna) {
                                    if (z.Kelompok == "Asesmen")
                                    {
                                        m.SudahIsiAsesmen = true;
                                    }
                                    else if(z.Kelompok == "General")
                                    {
                                        m.SudahIsiGeneral = true;
                                    }
                                    else if (z.Kelompok == "KunjunganPoliklinik")
                                    {
                                        m.SudahIsiKunjunganPoliklinik = true;
                                    }
                                }
                                
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else if (Request.Cookies["TipePelayanan"].Value == "PENUNJANG2")
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg;
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            }
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                var cekwarna = e.History_ListRegistrasi.Where(a => a.SectionID == sectionid && a.NoReg == x.NoReg).ToList();
                                foreach (var z in cekwarna)
                                { 
                                    if (z.Kelompok == "Asesmen")
                                    {
                                        m.SudahIsiAsesmen = true;
                                    }
                                    else
                                    {
                                        m.SudahIsiAsesmen = false;
                                    }

                                    if (z.Kelompok == "General")
                                    {
                                        m.SudahIsiGeneral = true;
                                    }
                                    else
                                    {
                                        m.SudahIsiGeneral = false;
                                    }

                                    if (z.Kelompok == "KunjunganPoliklinik")
                                    {
                                        m.SudahIsiKunjunganPoliklinik = true;
                                    }
                                    else
                                    {
                                        m.SudahIsiKunjunganPoliklinik = false;
                                    }
                                }
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg/*.Where(x => x.StatusBayar == "Belum" && x.Batal == false)*/;
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                var cekwarna = e.History_ListRegistrasi.Where(a => a.SectionID == sectionid && a.NoReg == x.NoReg).ToList();
                                foreach (var z in cekwarna)
                                {
                                    if (z.Kelompok == "Asesmen")
                                    {
                                        m.SudahIsiAsesmen = true;
                                    }
                                    else if (z.Kelompok == "General")
                                    {
                                        m.SudahIsiGeneral = true;
                                    }
                                    else if (z.Kelompok == "KunjunganPoliklinik")
                                    {
                                        m.SudahIsiKunjunganPoliklinik = true;
                                    }
                                }
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== T A B L E P A S I E N

        [HttpPost]
        public string ListRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    using (var emr = new EMR_Entities())
                    {
                        if (Request.Cookies["TipePelayanan"].Value == "REGISTRASI")
                        {
                            IQueryable<VW_Registrasi> p = s.VW_Registrasi;
                            if (filter[24] != "True")
                            {
                                p = p.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                            }
                            if (filter[26] != null && filter[26] != "")
                            {
                                p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            }
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                var delete = emr.mFinger.Where(y => y.userid == m.NRM).FirstOrDefault();
                                if (delete != null)
                                {
                                    m.DeleteMfinger = 1;
                                }
                                else
                                {
                                    m.DeleteMfinger = 0;
                                }
                                m.Jam_View = x.JamReg.ToString("HH\":\"mm");
                                m.Jam_View = x.JamReg.ToString("HH\":\"mm");
                                //m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg/*.Where(x => x.StatusBayar == "Belum" && x.Batal == false)*/;
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion 

        #region ===== T A B L E  P E G A W A I

        [HttpPost]
        public string ListRegistrasiPegawai(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    using (var emr = new EMR_Entities())
                    {
                        if (Request.Cookies["TipePelayanan"].Value == "REGISTRASI")
                        {
                            IQueryable<mDokter> p = s.mDokter;
                            //if (filter[24] != "True")
                            //{
                            //    p = p.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                            //    p = p.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                            //}
                            //if (filter[26] != null && filter[26] != "")
                            //{
                            //    p = p.Where($"DokterID.Contains(@0)", filter[26]);
                            //}
                            //if (filter[25] == "1")
                            //    p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            //else if (filter[25] == "2")
                            //    p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            //else if (filter[25] == "3")
                            //    p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            //else if (filter[25] == "4")
                            //    p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            p = p.Where($"DokterID.Contains(@0)", filter[3]);
                            //p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaDOkter.Contains(@0)", filter[7]);
                            p = p.Where($"Alamat.Contains(@0)", filter[8]);
                            p = p.Where($"NoKontak.Contains(@0)", filter[9]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                var delete = emr.mFinger.Where(y => y.userid == m.DokterID).FirstOrDefault();
                                if (delete != null)
                                {
                                    m.DeleteMfinger = 1;
                                }
                                else
                                {
                                    m.DeleteMfinger = 0;
                                }
                                //m.Jam_View = x.JamReg.ToString("HH\":\"mm");
                                //m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                //m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                        else
                        {
                            IQueryable<VW_DataPasienReg> p = s.VW_DataPasienReg/*.Where(x => x.StatusBayar == "Belum" && x.Batal == false)*/;
                            if (filter[24] != "True")
                            {
                                p = p.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                                p = p.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                            }
                            p = p.Where($"SectionID.Contains(@0)", Request.Cookies["SectionIDPelayanan"].Value);
                            if (filter[25] == "1")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", false);
                            else if (filter[25] == "2")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Belum", true);
                            else if (filter[25] == "3")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", true);
                            else if (filter[25] == "4")
                                p = p.Where($"StatusBayar=@0 AND SudahPeriksa=@1", "Sudah Bayar", false);
                            if (!string.IsNullOrEmpty(filter[0]))
                                p = p.Where($"NoAntri = @0", IFilter.F_short(filter[0]));
                            p = p.Where($"NoReg.Contains(@0)", filter[3]);
                            p = p.Where($"NRM.Contains(@0)", filter[6]);
                            p = p.Where($"NamaPasien.Contains(@0)", filter[7]);

                            var totalcount = p.Count();
                            var models = p.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                            result = new ResultSS(models.Length, null, totalcount, pageIndex);
                            var datas = new List<RegistrasiPasienViewModel>();
                            foreach (var x in models.ToList())
                            {
                                var m = IConverter.Cast<RegistrasiPasienViewModel>(x);
                                m.Jam_View = x.Jam.ToString("HH\":\"mm");
                                m.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                                m.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                                datas.Add(m);
                            }
                            result.Data = datas;
                        }
                    }
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string noreg, int nomor, string alasan)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == noreg && x.Nomor == nomor);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    m.Batal = true;
                    m.Active = false;
                    m.Out = true;
                    m.AlasanBatal = alasan;
                    result = new ResultSS(s.SaveChanges());
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Batal SIMtrDataRegPasien noreg:{noreg} nomor:{nomor}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E  M F I N G E R

        [HttpPost]
        public string DeleteFingerPasien(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var m = s.mFinger.Where(x => x.userid == id).ToList();
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    foreach (var x in m)
                    {
                        if (m != null) s.mFinger.Remove(x);
                    }
                    result = new ResultSS(s.SaveChanges());

                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}