﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class LembarObservasiAnestesiLokalController : Controller
    {
        // GET: LembarObservasiAnestesiLokal

        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.LembarObservasiAnastesiLokal.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.LmbrObsrvsiAnestesi = IConverter.Cast<LembarObservasiAnastesiLokalViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Dokter);
                            if (dokter != null)
                            {
                                model.LmbrObsrvsiAnestesi.DokterNama = dokter.NamaDOkter;
                            }
                            model.LmbrObsrvsiAnestesi.Report = 1;
                        }
                        else
                        {
                            item = new LembarObservasiAnastesiLokal();
                            model.LmbrObsrvsiAnestesi = IConverter.Cast<LembarObservasiAnastesiLokalViewModel>(item);
                            model.LmbrObsrvsiAnestesi.Report = 0;
                            model.LmbrObsrvsiAnestesi.NoReg = noreg;
                            model.LmbrObsrvsiAnestesi.NRM = model.NRM;
                            model.LmbrObsrvsiAnestesi.SectionID = sectionid;
                            model.LmbrObsrvsiAnestesi.Tanggal = DateTime.Today;
                            model.LmbrObsrvsiAnestesi.Jam = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_Kesadaran = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_Kesadaran = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Kesadaran_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Kesadaran_Sampai = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_Tensi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_Tensi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Tensi_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Tensi_Sampai = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_Nadi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_Nadi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Nadi_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Nadi_Sampai = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_Respirasi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_Respirasi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Respirasi_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Respirasi_Sampai = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_Suhu = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_Suhu = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Suhu_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_Suhu_Sampai = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_RiwayatAlergi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_ReaksiAlergi = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_ReaksiAlergi_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_ReaksiAlergi_Sampai = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Pre_SkalaNyeri = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Intra_SkalaNyeri = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_SkalaNyeri_Dari = DateTime.Now;
                            model.LmbrObsrvsiAnestesi.Post_SkalaNyeri_Sampai = DateTime.Now;
                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string LembarObservasiAnestesiLokal_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.LmbrObsrvsiAnestesi.NoReg);
                            item.LmbrObsrvsiAnestesi.NoReg = item.LmbrObsrvsiAnestesi.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.LmbrObsrvsiAnestesi.SectionID = sectionid;
                            item.LmbrObsrvsiAnestesi.NRM = item.LmbrObsrvsiAnestesi.NRM;
                            var model = eEMR.LembarObservasiAnastesiLokal.FirstOrDefault(x => x.SectionID == item.LmbrObsrvsiAnestesi.SectionID && x.NoReg == item.LmbrObsrvsiAnestesi.NoReg);

                            if (model == null)
                            {
                                model = new LembarObservasiAnastesiLokal();
                                var o = IConverter.Cast<LembarObservasiAnastesiLokal>(item.LmbrObsrvsiAnestesi);
                                eEMR.LembarObservasiAnastesiLokal.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<LembarObservasiAnastesiLokal>(item.LmbrObsrvsiAnestesi);
                                eEMR.LembarObservasiAnastesiLokal.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFLembarObservasiAnestesiLokal(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LembarObservasiAnastesiLokal.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_LembarObservasiAnastesiLokal", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_LembarObservasiAnastesiLokal;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}