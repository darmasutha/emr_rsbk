﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class MonitorTranfusiDarahController : Controller
    {
        // GET: MonitorTranfusiDarah
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.MonitoringTransfusiDarah.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.MntrTrnfsiDrh = IConverter.Cast<MonitorTransfusiDarahViewModel>(item);
                            var perawatdokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DoubleCheckPerawatDokter_SebelumTransfusi);
                            if (perawatdokter != null)
                            {
                                model.MntrTrnfsiDrh.DoubleCheckPerawatDokter_SebelumTransfusiNama = perawatdokter.NamaDOkter;
                            }
                            var perawatdokter1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DoubleCheckPerawatDokter_SetelahTransfusi);
                            if (perawatdokter1 != null)
                            {
                                model.MntrTrnfsiDrh.DoubleCheckPerawatDokter_SetelahTransfusiNama = perawatdokter1.NamaDOkter;
                            }
                            var perawatdokter2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DoubleCheckPerawatDokter_TransfusiBerakhir);
                            if (perawatdokter2 != null)
                            {
                                model.MntrTrnfsiDrh.DoubleCheckPerawatDokter_TransfusiBerakhirNama = perawatdokter2.NamaDOkter;
                            }
                            var perawatdokter3 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DoubleCheckPerawatDokter_Keluhan);
                            if (perawatdokter3 != null)
                            {
                                model.MntrTrnfsiDrh.DoubleCheckPerawatDokter_KeluhanNama = perawatdokter3.NamaDOkter;
                            }
                            model.MntrTrnfsiDrh.NoReg = noreg;
                            model.MntrTrnfsiDrh.NRM = model.NRM;
                            model.MntrTrnfsiDrh.SectionID = sectionid;
                            model.MntrTrnfsiDrh.Report = 1;
                            model.MntrTrnfsiDrh.NamaPasien = m.NamaPasien;
                            model.MntrTrnfsiDrh.JenisKelamin = m.JenisKelamin;
                            model.MntrTrnfsiDrh.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new MonitoringTransfusiDarah();
                            model.MntrTrnfsiDrh = IConverter.Cast<MonitorTransfusiDarahViewModel>(item);
                            model.MntrTrnfsiDrh.Report = 0;
                            model.MntrTrnfsiDrh.NamaPasien = m.NamaPasien;
                            model.MntrTrnfsiDrh.JenisKelamin = m.JenisKelamin;
                            model.MntrTrnfsiDrh.TglLahir = m.TglLahir;
                            model.MntrTrnfsiDrh.NoReg = noreg;
                            model.MntrTrnfsiDrh.NRM = model.NRM;
                            model.MntrTrnfsiDrh.SectionID = sectionid;
                            model.MntrTrnfsiDrh.Tanggal = DateTime.Today;
                            model.MntrTrnfsiDrh.Waktu_Penerima = DateTime.Now;
                            model.MntrTrnfsiDrh.Waktu_Pengirim = DateTime.Now;
                            model.MntrTrnfsiDrh.Waktu_SebelumTransfusi = DateTime.Now;
                            model.MntrTrnfsiDrh.Waktu_SetelahTransfusi = DateTime.Now;
                            model.MntrTrnfsiDrh.Waktu_TransfusiBerakhir = DateTime.Now;
                            model.MntrTrnfsiDrh.Waktu_Keluhan = DateTime.Now;
                        }


                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string MonitoringTransfusiDarah_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.MntrTrnfsiDrh.NoReg = item.MntrTrnfsiDrh.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.MntrTrnfsiDrh.SectionID = sectionid;
                            item.MntrTrnfsiDrh.NRM = item.MntrTrnfsiDrh.NRM;
                            var model = eEMR.MonitoringTransfusiDarah.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.MntrTrnfsiDrh.NoReg);

                            if (model == null)
                            {
                                model = new MonitoringTransfusiDarah();
                                var o = IConverter.Cast<MonitoringTransfusiDarah>(item.MntrTrnfsiDrh);
                                eEMR.MonitoringTransfusiDarah.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<MonitoringTransfusiDarah>(item.MntrTrnfsiDrh);
                                eEMR.MonitoringTransfusiDarah.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFMonitoringTransfusiDarah(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_MonitoringTransfusiDarah.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_MonitoringTransfusiDarah", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_MonitoringTransfusiDarah;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_MonitoringTransfusiDarah_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_MonitoringTransfusiDarah_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}