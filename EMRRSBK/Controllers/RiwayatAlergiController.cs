﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RiwayatAlergiController : Controller
    {
        // GET: RiwayatAlergi
        public ActionResult Index(string nrm)
        {
            var model = new EWSScorePemantauanViewModel();

            try
            {
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                using (var s = new EMR_Entities())
                {
                    using (var eSIM = new SIM_Entities())
                    {
                        //DETAIL 1
                        model.NRM = nrm;
                        var List = eSIM.SIMtrRiwayatAlergi.Where(x => x.NRM == nrm).ToList();
                        var cek = eSIM.SIMtrRiwayatAlergi.Where(x => x.NRM == nrm).FirstOrDefault();
                        if (cek != null)
                        {
                            model.AlergiObat_List = new ListDetail<SIMtrRiwayatAlergiViewModelDetail>();

                            foreach (var x in List)
                            {
                                var y = IConverter.Cast<SIMtrRiwayatAlergiViewModelDetail>(x);
                                var petugas = eSIM.mBarang.FirstOrDefault(d => d.Barang_ID == y.BarangID);
                                if (petugas != null)
                                {
                                    y.KodeBarang = petugas.Kode_Barang;
                                    y.BarangNama = petugas.Nama_Barang;
                                }
                                model.AlergiObat_List.Add(false, y);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("RiwayatAlergi")]
        public string RiwayatAlergi_Post()
        {
            try
            {
                var item = new EWSScorePemantauanViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                            #region ==== D E T A I L
                            if (item.AlergiObat_List == null) item.AlergiObat_List = new ListDetail<SIMtrRiwayatAlergiViewModelDetail>();
                            item.AlergiObat_List.RemoveAll(x => x.Remove);

                            foreach (var x in item.AlergiObat_List)
                            {
                                x.Model.NRM = item.NRM;
                            }

                            var new_List = item.AlergiObat_List;
                            var real_List = eSIM.SIMtrRiwayatAlergi.Where(x => x.NRM == item.NRM).ToList();
                            // delete | delete where (real_list not_in new_list)
                            foreach (var x in real_List)
                            {
                                var m = new_List.FirstOrDefault(y => y.Model.DeskripsiAlergi == x.DeskripsiAlergi);
                                if (m == null)
                                {
                                    eSIM.SIMtrRiwayatAlergi.Remove(x);
                                }

                            }


                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.DeskripsiAlergi == x.Model.DeskripsiAlergi);
                                //new DataColumn("Tanggal", typeof(DateTime));
                                //new DataColumn("Jam", typeof(TimeSpan));
                                // add | add where (new_list not_in raal_list)
                                var barang = eSIM.mBarang.Where(xx => xx.Kode_Barang == x.Model.KodeBarang).FirstOrDefault();
                                x.Model.BarangID = barang.Barang_ID;
                                if (_m == null)
                                {

                                    _m = new SIMtrRiwayatAlergi();
                                    eSIM.SIMtrRiwayatAlergi.Add(IConverter.Cast<SIMtrRiwayatAlergi>(x.Model));
                                    eSIM.SaveChanges();
                                }
                                else
                                {

                                    _m = IConverter.Cast<SIMtrRiwayatAlergi>(x.Model);
                                    _m.BarangID = x.Model.BarangID;
                                    eSIM.SIMtrRiwayatAlergi.AddOrUpdate(_m);
                                    eSIM.SaveChanges();
                                }

                            }
                            #endregion

                            //var nomorreport = eEMR.PemantauanAEWS_Detail.Where(y => y.NRM == item.NRM).ToList();
                            //if (nomorreport != null)
                            //{
                            //    var ii = 1;
                            //    var zz = 1;
                            //    foreach (var x in nomorreport)
                            //    {
                            //        if (ii == 6)
                            //        {
                            //            ii = 1;
                            //            ++zz;
                            //        }
                            //        x.NomorReport = ii;
                            //        x.NomorReport_Group = zz;
                            //        ++ii;
                            //    }
                            //}

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFPemantauanAEWSScore(string noreg, string section)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PemantauanAEWSDetail.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"PemantauanAEWSDetail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", section));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"PemantauanAEWSDetail;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("ImplementasiRisikoJatuhAnak", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["ImplementasiRisikoJatuhAnak;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}