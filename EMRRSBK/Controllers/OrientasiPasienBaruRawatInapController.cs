﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace EMRRSBK.Controllers
{
    public class OrientasiPasienBaruRawatInapController : Controller
    {
        // GET: OrientasiPasienBaruRawatInap
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    var n = eSIM.VW_DataPasienReg.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;
                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.OrientasiPasienBaruRawatInap.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.OrentasiPsnBaruRawatInap = IConverter.Cast<OrientasiPasienBaruRawatInapViewModel>(item);
                            model.OrentasiPsnBaruRawatInap.NoReg = m.NoReg;
                            model.OrentasiPsnBaruRawatInap.NRM = m.NRM;
                            model.OrentasiPsnBaruRawatInap.Tanggal = DateTime.Today;
                            model.OrentasiPsnBaruRawatInap.Jam = DateTime.Now;
                            model.OrentasiPsnBaruRawatInap.Nama = m.NamaPasien;
                            model.OrentasiPsnBaruRawatInap.TglLahir = m.TglLahir;
                            model.OrentasiPsnBaruRawatInap.Kamar = n.Kamar;
                            model.OrentasiPsnBaruRawatInap.Ruangan = n.NoRuang;
                            model.OrentasiPsnBaruRawatInap.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.OrentasiPsnBaruRawatInap.Report = 1;

                        } else
                        {
                            item = new OrientasiPasienBaruRawatInap();
                            model.OrentasiPsnBaruRawatInap = IConverter.Cast<OrientasiPasienBaruRawatInapViewModel>(item);
                            model.OrentasiPsnBaruRawatInap.NoReg = m.NoReg;
                            model.OrentasiPsnBaruRawatInap.NRM = m.NRM;
                            model.OrentasiPsnBaruRawatInap.Tanggal = DateTime.Today;
                            model.OrentasiPsnBaruRawatInap.Jam = DateTime.Now;
                            model.OrentasiPsnBaruRawatInap.Nama = m.NamaPasien;
                            model.OrentasiPsnBaruRawatInap.TglLahir = m.TglLahir;
                            model.OrentasiPsnBaruRawatInap.Kamar = n.Kamar;
                            model.OrentasiPsnBaruRawatInap.Ruangan = n.NoRuang;
                            model.OrentasiPsnBaruRawatInap.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.OrentasiPsnBaruRawatInap.Report = 0;
                        }
                    }
                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
                if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string OrientasiPasienBaruRawatInap_Post(string noreg, string sectionid)
        {
             try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.OrentasiPsnBaruRawatInap.NoReg);
                            item.OrentasiPsnBaruRawatInap.NoReg = item.OrentasiPsnBaruRawatInap.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.OrentasiPsnBaruRawatInap.SectionID = sectionid;
                            item.OrentasiPsnBaruRawatInap.NRM = item.OrentasiPsnBaruRawatInap.NRM;
                            var model = eEMR.OrientasiPasienBaruRawatInap.FirstOrDefault(x => x.SectionID == item.OrentasiPsnBaruRawatInap.SectionID && x.NoReg == item.OrentasiPsnBaruRawatInap.NoReg);

                            if (model == null)
                            {
                                model = new OrientasiPasienBaruRawatInap();
                                var o = IConverter.Cast<OrientasiPasienBaruRawatInap>(item.OrentasiPsnBaruRawatInap);
                                eEMR.OrientasiPasienBaruRawatInap.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<OrientasiPasienBaruRawatInap>(item.OrentasiPsnBaruRawatInap);
                                eEMR.OrientasiPasienBaruRawatInap.AddOrUpdate(model);

                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}