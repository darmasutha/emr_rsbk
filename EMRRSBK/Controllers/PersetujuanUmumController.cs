﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class PersetujuanUmumController : Controller
    {
        // GET: PersetujuanUmum
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RegPersetujuanUmum = IConverter.Cast<RegPersetujuanUmumViewModel>(item);
                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Perawat);

                            if (perawat != null)
                            {
                                model.RegPersetujuanUmum.PerawatNama = perawat.NamaDOkter;
                            }
                            ViewBag.UrlFinger = GetEncodeUrl(model.NRM, model.NoReg, model.SectionID);
                            if (model.RegPersetujuanUmum.Perawat != null)
                            {
                                ViewBag.UrlFingerDokter = GetEncodeUrl(model.RegPersetujuanUmum.Perawat, model.NoReg, model.SectionID);
                            }
                            model.RegPersetujuanUmum.Report = 1;
                        }
                        else
                        {
                            item = new PersetujuanUmumRegistrasi();
                            model.RegPersetujuanUmum = IConverter.Cast<RegPersetujuanUmumViewModel>(item);
                            ViewBag.UrlFinger = GetEncodeUrl(model.NRM, model.NoReg, model.SectionID);
                            model.RegPersetujuanUmum.Pasien = model.NamaPasien;
                            model.RegPersetujuanUmum.Report = 0;
                            model.RegPersetujuanUmum.NoReg = noreg;
                            model.RegPersetujuanUmum.NRM = model.NRM;
                            model.RegPersetujuanUmum.SectionID = sectionid;
                            model.RegPersetujuanUmum.Tanggal = DateTime.Today;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string PersetujuanUmum_Post()
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var model = eEMR.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.SectionID == item.RegPersetujuanUmum.SectionID && x.NoReg == item.RegPersetujuanUmum.NoReg);

                            if (model == null)
                            {
                                model = new PersetujuanUmumRegistrasi();
                                var o = IConverter.Cast<PersetujuanUmumRegistrasi>(item.RegPersetujuanUmum);
                                o.Tanggal = DateTime.Today;
                                eEMR.PersetujuanUmumRegistrasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PersetujuanUmumRegistrasi>(item.RegPersetujuanUmum);
                                model = IConverter.Cast<PersetujuanUmumRegistrasi>(item.RegPersetujuanUmum);
                                model.Tanggal = DateTime.Today;
                                eEMR.PersetujuanUmumRegistrasi.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region === SURAT PERNYATAAN REGISTRASI
        public ActionResult ExportPDFIdentitasPasien_Registrasi(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"IdentitasPasien_Registrasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("IdentitasPasien_Registrasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"IdentitasPasien_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region === PERSETUJUAN UMUM 
        public ActionResult ExportPDFRegPersetujuanUmum(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PersetujuanUmum_Registrasi.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("PersetujuanUmum_Registrasi", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"PersetujuanUmum_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        [HttpGet]
        public string CheckBerhasil(string noreg, string section, string id)
        {
            var berhasil = false;
            using (var s = new EMR_Entities())
            {
                var m = s.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                if (m != null)
                {
                    if (m.TandaTangan != null)
                    {
                        berhasil = true;
                    }
                }

            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = berhasil,
            });
        }

        #region ==== G E T  U R L 

        public string GetEncodeUrl(string id, string noreg, string section)
        {
            var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority +
        Request.ApplicationPath.TrimEnd('/') + "/PersetujuanUmum/get_key";
            //var a = Server.MapPath("~/RegisterFinger/GetKey");
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
            var url = System.Convert.ToBase64String(plainTextBytes);
            return url;
        }

        public string GetEncodeUrl_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == id);
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/PersetujuanUmum/get_key";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = new StandardViewModel()
                {
                    Kode = System.Convert.ToBase64String(plainTextBytes),
                    Nama = dokter == null ? "" : dokter.NamaDOkter
                };
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = url,
                });

            }

            //var cek = System.Convert.FromBase64String(url);
            //var cek2 = Encoding.UTF8.GetString(cek);

        }

        #endregion

        #region ===== G E T  K E Y
        [HttpGet]
        public string get_key(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                fdata = f.finger_data;
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/PersetujuanUmum/checkverif";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        #endregion

        #region ===== C H E C K  V E R I F
        public string checkverif(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC000")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f)
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.PersetujuanUmumRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion

                                #region === Verif Surat Pernyataan
                                //var datassrtpenyataan = s.SuratPernyataanRegistrasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                //if (datassrtpenyataan != null)
                                //{
                                //    if (dokter != null)
                                //    {
                                //        datassrtpenyataan.TandaTangan = i.Gambar1;
                                //    }
                                //    else
                                //    {
                                //        datassrtpenyataan.TandaTangan = i.Gambar1;
                                //    }
                                //    s.SaveChanges();
                                //}
                                #endregion

                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion
    }
}