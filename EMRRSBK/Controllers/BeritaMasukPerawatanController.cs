﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace EMRRSBK.Controllers
{
    public class BeritaMasukPerawatanController : Controller
    {
        // GET: BeritaMasukPerawatan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.BeritaMasukPerawatan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.BeritaMskPerawatan = IConverter.Cast<BeritaMasukPerawatanViewModel>(item);
                            model.BeritaMskPerawatan.NoReg = m.NoReg;
                            model.BeritaMskPerawatan.Nama = m.NamaPasien;
                            model.BeritaMskPerawatan.Umur = m.UmurThn;
                            model.BeritaMskPerawatan.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.BeritaMskPerawatan.Alamat = m.Alamat;
                            model.BeritaMskPerawatan.Pekrjaan = m.Pekerjaan;
                            model.BeritaMskPerawatan.Report = 1;

                        }
                        else
                        {
                            item = new BeritaMasukPerawatan();
                            model.BeritaMskPerawatan = IConverter.Cast<BeritaMasukPerawatanViewModel>(item);
                            model.BeritaMskPerawatan.NoReg = m.NoReg;
                            model.BeritaMskPerawatan.NRM = m.NRM;
                            model.BeritaMskPerawatan.Tanggal = DateTime.Today;
                            model.BeritaMskPerawatan.Jam = DateTime.Now;
                            model.BeritaMskPerawatan.Nama = m.NamaPasien;
                            model.BeritaMskPerawatan.Umur = m.UmurThn;
                            model.BeritaMskPerawatan.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.BeritaMskPerawatan.Pekrjaan = m.Pekerjaan;
                            model.BeritaMskPerawatan.Alamat = m.Alamat;
                            model.BeritaMskPerawatan.Report = 0;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string BeritaMasukPerawatan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.BeritaMskPerawatan.NoReg);
                            item.BeritaMskPerawatan.NoReg = item.BeritaMskPerawatan.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.BeritaMskPerawatan.SectionID = sectionid;
                            item.BeritaMskPerawatan.NRM = item.BeritaMskPerawatan.NRM;
                            var model = eEMR.BeritaMasukPerawatan.FirstOrDefault(x => x.SectionID == item.BeritaMskPerawatan.SectionID && x.NoReg == item.BeritaMskPerawatan.NoReg);

                            if (model == null)
                            {
                                model = new BeritaMasukPerawatan();
                                var o = IConverter.Cast<BeritaMasukPerawatan>(item.BeritaMskPerawatan);
                                eEMR.BeritaMasukPerawatan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<BeritaMasukPerawatan>(item.BeritaMskPerawatan);
                                eEMR.BeritaMasukPerawatan.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }

}