﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace EMRRSBK.Controllers
{
    public class SuratJawabanKonsulController : Controller
    {
        // GET: SuratJawabanKonsul
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    
                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.SuratJawabanKonsultasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                       
                        if (item != null)
                        {
                            model.SuratJwbKonsul = IConverter.Cast<SuratJawabanKonsulViewModel>(item);

                            model.SuratJwbKonsul.Nama_Pasien_Konsul = m.NamaPasien;
                            model.SuratJwbKonsul.NoReg = noreg;
                            model.SuratJwbKonsul.NRM = model.NRM;

                            var dpjp = eSIM.mDokter.FirstOrDefault(x => x.DokterID == model.SuratJwbKonsul.TS_Dr_JawabanKonsul);
                            if (dpjp != null)
                            {
                                model.SuratJwbKonsul.TS_Dr_JawabanKonsulNama = dpjp.NamaDOkter;
                            }

                        }
                        else
                        {
                            item = new SuratJawabanKonsultasi();
                            model.SuratJwbKonsul = IConverter.Cast<SuratJawabanKonsulViewModel>(item);
                            model.SuratJwbKonsul.Nama_Pasien_Konsul = m.NamaPasien;
                            model.SuratJwbKonsul.NoReg = noreg;
                            model.SuratJwbKonsul.NRM = model.NRM;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string SuratJawabanKonsul_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                          
                            item.SuratJwbKonsul.NoReg = item.SuratJwbKonsul.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.SuratJwbKonsul.SectionID = sectionid;
                            item.SuratJwbKonsul.NRM = item.SuratJwbKonsul.NRM;
                            var model = eEMR.SuratJawabanKonsultasi.FirstOrDefault(x => x.SectionID == item.SuratJwbKonsul.SectionID && x.NoReg == item.SuratJwbKonsul.NoReg);

                            if (model == null)
                            {
                                model = new SuratJawabanKonsultasi();
                                var o = IConverter.Cast<SuratJawabanKonsultasi>(item.SuratJwbKonsul);
                                eEMR.SuratJawabanKonsultasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratJawabanKonsultasi>(item.SuratJwbKonsul);
                                eEMR.SuratJawabanKonsultasi.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}