﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class SuratKeteranganDokterController : Controller
    {
        // GET: SuratKeteranganDokter
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();
            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.SuratKeteranganDokter.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.SKDokter = IConverter.Cast<SuratKeteranganDokterViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Dokter);
                            if (dokter != null)
                            {
                                model.SKDokter.DokterNama = dokter.NamaDOkter;
                            }
                            model.SKDokter.NoReg = noreg;
                            model.SKDokter.NRM = model.NRM;
                            model.SKDokter.SectionID = sectionid;
                            model.SKDokter.Report = 1;
                            model.SKDokter.NamaPasien = m.NamaPasien;
                            model.SKDokter.UmurThn = m.UmurThn;
                            model.SKDokter.JenisKelamin = m.JenisKelamin;
                            model.SKDokter.Alamat = m.Alamat;

                            var fingerpngjnkprwtn = eEMR.SuratKeteranganDokter.Where(x => x.Dokter == model.SKDokter.Dokter && x.TandaTanganDokter == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpngjnkprwtn != null)
                            {
                                model.SKDokter.SudahRegDokter = 1;
                            }
                            else
                            {
                                model.SKDokter.SudahRegDokter = 0;
                            }
                            if (model.SKDokter.Dokter != null)
                            {
                                ViewBag.UrlFingerSKDokterDokter = GetEncodeUrlSKDokter_Dokter(model.SKDokter.Dokter, model.SKDokter.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new SuratKeteranganDokter();
                            model.SKDokter = IConverter.Cast<SuratKeteranganDokterViewModel>(item);
                            ViewBag.UrlFingerSKDokterDokter = GetEncodeUrlSKDokter_Dokter(model.SKDokter.Dokter, model.SKDokter.NoReg, sectionid);
                            model.SKDokter.Report = 0;
                            model.SKDokter.Alamat = m.Alamat;
                            model.SKDokter.NamaPasien = m.NamaPasien;
                            model.SKDokter.UmurThn = m.UmurThn;
                            model.SKDokter.NoReg = noreg;
                            model.SKDokter.NRM = model.NRM;
                            model.SKDokter.SectionID = sectionid;
                            model.SKDokter.JenisKelamin = m.JenisKelamin;
                            model.SKDokter.Tgl = DateTime.Today;
                            model.SKDokter.Tgl_SuratKeteranganDokter = DateTime.Today;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string SuratKeteranganDokter_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.SKDokter.NoReg = item.SKDokter.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.SKDokter.SectionID = sectionid;
                            item.SKDokter.NRM = item.SKDokter.NRM;
                            var model = eEMR.SuratKeteranganDokter.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.SKDokter.NoReg);

                            if (model == null)
                            {
                                model = new SuratKeteranganDokter();
                                var o = IConverter.Cast<SuratKeteranganDokter>(item.SKDokter);
                                eEMR.SuratKeteranganDokter.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratKeteranganDokter>(item.SKDokter);
                                eEMR.SuratKeteranganDokter.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFSuratKeteranganDokter(string noreg,string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_SuratKeteranganDokter.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_SuratKeteranganDokter", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_SuratKeteranganDokter;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlSKDokter_Dokter(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifSuratKeteranganDokter/get_keyskdokter";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}