﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RencanaKeperawatanRisikoKekuranganVolumeCairanRIController : Controller
    {
        // GET: RencanaKeperawatanRisikoKekuranganVolumeCairanRI
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RencanaKeperawatanRisikoKekuranganVolumeCairan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RncanaKprwtRskKrngCair = IConverter.Cast<RencanaKeperawatanRisikoKekuranganVolumeCairanViewModel>(item);
                            //var paraf = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Paraf);
                            //if (paraf != null)
                            //{
                            //    model.RncanaKprwtRskKrngCair.ParafNama = paraf.NamaDOkter;
                            //}
                            model.RncanaKprwtRskKrngCair.NoReg = noreg;
                            model.RncanaKprwtRskKrngCair.NRM = model.NRM;
                            model.RncanaKprwtRskKrngCair.SectionID = sectionid;
                            model.RncanaKprwtRskKrngCair.Report = 1;
                            model.RncanaKprwtRskKrngCair.NamaTerang = m.NamaPasien;
                            model.RncanaKprwtRskKrngCair.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtRskKrngCair.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new RencanaKeperawatanRisikoKekuranganVolumeCairan();
                            model.RncanaKprwtRskKrngCair = IConverter.Cast<RencanaKeperawatanRisikoKekuranganVolumeCairanViewModel>(item);
                            model.RncanaKprwtRskKrngCair.Report = 0;
                            model.RncanaKprwtRskKrngCair.NamaTerang = m.NamaPasien;
                            model.RncanaKprwtRskKrngCair.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtRskKrngCair.TglLahir = m.TglLahir;
                            model.RncanaKprwtRskKrngCair.NoReg = noreg;
                            model.RncanaKprwtRskKrngCair.NRM = model.NRM;
                            model.RncanaKprwtRskKrngCair.SectionID = sectionid;
                            model.RncanaKprwtRskKrngCair.Tanggal = DateTime.Today;
                            model.RncanaKprwtRskKrngCair.Jam = DateTime.Now;
                            model.RncanaKprwtRskKrngCair.Tgl_DiagnosaKeperawatan = DateTime.Today;
                            model.RncanaKprwtRskKrngCair.RencanaTindakan_Jam_MonitorStatusHidrasi = DateTime.Now;
                        }


                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RencanaKeperawatanRisikoKekuranganVolumeCairan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RncanaKprwtRskKrngCair.NoReg = item.RncanaKprwtRskKrngCair.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.RncanaKprwtRskKrngCair.SectionID = sectionid;
                            item.RncanaKprwtRskKrngCair.NRM = item.RncanaKprwtRskKrngCair.NRM;
                            var model = eEMR.RencanaKeperawatanRisikoKekuranganVolumeCairan.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.RncanaKprwtRskKrngCair.NoReg);

                            if (model == null)
                            {
                                model = new RencanaKeperawatanRisikoKekuranganVolumeCairan();
                                var o = IConverter.Cast<RencanaKeperawatanRisikoKekuranganVolumeCairan>(item.RncanaKprwtRskKrngCair);
                                eEMR.RencanaKeperawatanRisikoKekuranganVolumeCairan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RencanaKeperawatanRisikoKekuranganVolumeCairan>(item.RncanaKprwtRskKrngCair);
                                eEMR.RencanaKeperawatanRisikoKekuranganVolumeCairan.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRencanaKeperawatanRisikoKekuranganVolumeCairan(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RencanaKeperawatanRisikoKekuranganVolumeCairan.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();
                var i = 0;
                cmd.Add(new SqlCommand("Rpt_RencanaKeperawatanRisikoKekuranganVolumeCairan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_RencanaKeperawatanRisikoKekuranganVolumeCairan;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand($"Rpt_RencanaKeperawatanRisikoKekuranganVolumeCairan_Detail", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables[$"Rpt_RencanaKeperawatanRisikoKekuranganVolumeCairan_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}