﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class VerifChckListKslmtnPsienOprsController : Controller
    {
        // GET: VerifChckListKslmtnPsienOprs
        #region === SURAT CHECKLIST KESELAMATAN PASIEN OK
        [HttpGet]
        public string CheckBerhasilChckKeselamatanPasienOK(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTanganDokterAnestesi != null)
                        {
                            result.DokterAnestesi = "1";
                        }
                        else
                        {
                            result.DokterAnestesi = "0";
                        }

                        if (m.TandaTanganDokterAnak != null)
                        {
                            result.DokterAnak = "1";
                        }
                        else
                        {
                            result.DokterAnak = "0";
                        }

                        if (m.TandaTanganPerawatAnestesi != null)
                        {
                            result.PerawatAnestesi = "1";
                        }
                        else
                        {
                            result.PerawatAnestesi = "0";
                        }

                        if (m.TandaTanganPerawatSirkuler != null)
                        {
                            result.PerawatSirkuler = "1";
                        }
                        else
                        {
                            result.PerawatSirkuler = "0";
                        }

                        if (m.TandaTanganOperator != null)
                        {
                            result.Operator = "1";
                        }
                        else
                        {
                            result.Operator = "0";
                        }

                        if (m.TandaTanganInstrumen != null)
                        {
                            result.Instrumen = "1";
                        }
                        else
                        {
                            result.Instrumen = "0";
                        }

                        if (m.TandaTanganAsisten != null)
                        {
                            result.Asisten = "1";
                        }
                        else
                        {
                            result.Asisten = "0";
                        }
                    }
                }
                else
                {
                    result.DokterAnestesi = "0";
                    result.DokterAnak = "0";
                    result.PerawatAnestesi = "0";
                    result.PerawatSirkuler = "0";
                    result.Operator = "0";
                    result.Instrumen = "0";
                    result.Asisten = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        [HttpGet]
        public string get_keychckliskslmntdokteranestesi(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifdokteranestesi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keychckliskslmntdokteranak(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifdokteranak";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keychckliskslmntperawatanestesi(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifperawatanestesi";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keychckliskslmntperawatsirkuler(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifperawatsirkuler";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keychckliskslmntoperator(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifoperator";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keychckliskslmntistrumen(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifinstrumen";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keychckliskslmntasisten(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/checkverifasisten";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifdokteranestesi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistdokteranestesi = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistdokteranestesi != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistdokteranestesi.TandaTanganDokterAnestesi = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistdokteranestesi.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifdokteranak(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistdokteraanak = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistdokteraanak != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistdokteraanak.TandaTanganDokterAnak = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistdokteraanak.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifperawatanestesi(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistperawatanestesi = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistperawatanestesi != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistperawatanestesi.TandaTanganPerawatAnestesi = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistperawatanestesi.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifperawatsirkuler(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistperawatsirkuler = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistperawatsirkuler != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistperawatsirkuler.TandaTanganPerawatSirkuler = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistperawatsirkuler.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifoperator(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistoperator = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistoperator != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistoperator.TandaTanganOperator = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistoperator.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifinstrumen(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistisntrumen = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistisntrumen != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistisntrumen.TandaTanganInstrumen = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistisntrumen.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifasisten(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                    if (dokter != null)
                    {
                        user_id = dokter.DokterID;
                    }

                    var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                    var f = s.mFinger.Where(x => x.userid == user_id);
                    foreach (var i in f.ToList())
                    {
                        var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                        if (salt.ToUpper() == vStamp.ToUpper())
                        {
                            #region === Verif 
                            var dataschcklistasisten = s.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                            if (dataschcklistasisten != null)
                            {
                                if (dokter != null)
                                {
                                    dataschcklistasisten.TandaTanganAsisten = i.Gambar1;
                                }
                                else
                                {
                                    dataschcklistasisten.TandaTangan = i.Gambar1;
                                }
                                s.SaveChanges();
                            }
                            #endregion
                            return "empty";
                        };
                    }

                }

            }
            return "Gagal";
        }
        #endregion
    }
}