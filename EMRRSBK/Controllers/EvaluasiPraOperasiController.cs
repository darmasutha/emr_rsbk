﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class EvaluasiPraOperasiController : Controller
    {
        // GET: EvaluasiPraOperasi
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.EvaluasiPraOperasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.EvPraOpr = IConverter.Cast<EvaluasiPraOperasiViewModel>(item);
                            var dokteroperator1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodeOperator);
                            if (dokteroperator1 != null)
                            {
                                model.EvPraOpr.KodeOperatorNama = dokteroperator1.NamaDOkter;
                            }

                            var dokteroperator2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodeAnestesi);
                            if (dokteroperator2 != null)
                            {
                                model.EvPraOpr.KodeAnestesiNama = dokteroperator2.NamaDOkter;
                            }

                            var spesialis1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.SpesialisTerkait_1);
                            if (spesialis1 != null)
                            {
                                model.EvPraOpr.SpesialisTerkait_1Nama = spesialis1.NamaDOkter;
                            }

                            var spesialis2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.SpesialisTerkait_2);
                            if (spesialis2 != null)
                            {
                                model.EvPraOpr.SpesialisTerkait_2Nama = spesialis2.NamaDOkter;
                            }

                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodePerawat);
                            if (perawat != null)
                            {
                                model.EvPraOpr.KodePerawatNama = perawat.NamaDOkter;
                            }

                            model.EvPraOpr.NamaPasien = m.NamaPasien;
                            model.EvPraOpr.JenisKelamin = m.JenisKelamin;
                            model.EvPraOpr.Umur = m.UmurThn;
                            model.EvPraOpr.Report = 1;

                            var fingeroperator = eEMR.EvaluasiPraOperasi.Where(x => x.KodeOperator == model.EvPraOpr.KodeOperator && x.TandaTanganOperator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingeroperator != null)
                            {
                                model.EvPraOpr.SudahRegOperator = 1;
                            }
                            else
                            {
                                model.EvPraOpr.SudahRegOperator = 0;
                            }
                            if (model.EvPraOpr.KodeOperator != null)
                            {
                                ViewBag.UrlFingerEvPraOprKodeOperator = GetEncodeUrlEvPraOpr_KodeOperator(model.EvPraOpr.KodeOperator, model.EvPraOpr.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new EvaluasiPraOperasi();
                            model.EvPraOpr = IConverter.Cast<EvaluasiPraOperasiViewModel>(item);
                            ViewBag.UrlFingerEvPraOprKodeOperator = GetEncodeUrlEvPraOpr_KodeOperator(model.EvPraOpr.KodeOperator, model.EvPraOpr.NoReg, sectionid);
                            model.EvPraOpr.NoReg = noreg;
                            model.EvPraOpr.NamaPasien = m.NamaPasien;
                            model.EvPraOpr.JenisKelamin = m.JenisKelamin;
                            model.EvPraOpr.Umur = m.UmurThn;
                            model.EvPraOpr.NRM = m.NRM;
                            model.EvPraOpr.SectionID = sectionid;
                            model.EvPraOpr.Tanggal = DateTime.Today;
                            model.EvPraOpr.Jam = DateTime.Now;
                            model.EvPraOpr.Report = 0;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string EvaluasiPraOperasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.EvPraOpr.NoReg);
                            item.EvPraOpr.NoReg = item.EvPraOpr.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.EvPraOpr.SectionID = sectionid;
                            item.EvPraOpr.NRM = item.EvPraOpr.NRM;
                            var model = eEMR.EvaluasiPraOperasi.FirstOrDefault(x => x.SectionID == item.EvPraOpr.SectionID && x.NoReg == item.EvPraOpr.NoReg);

                            if (model == null)
                            {
                                model = new EvaluasiPraOperasi();
                                var o = IConverter.Cast<EvaluasiPraOperasi>(item.EvPraOpr);
                                eEMR.EvaluasiPraOperasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<EvaluasiPraOperasi>(item.EvPraOpr);
                                eEMR.EvaluasiPraOperasi.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFEvaluasiPraOperasi(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_EvaluasiPraOperasi.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_EvaluasiPraOperasi", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_EvaluasiPraOperasi;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlEvPraOpr_KodeOperator(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifEvaluasiPraOperasi/get_keyevaluasipraoperasioperator";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}