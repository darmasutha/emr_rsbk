﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class RegistrasiPegawaiController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

    }
}