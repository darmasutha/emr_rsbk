﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace EMRRSBK.Controllers
{
    public class CheckListKeselamatanPasienOperasiOKController : Controller
    {
        // GET: CheckListKeselamatanPasienOperasiOK

        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.ChckListKslmtnPsienOprs = IConverter.Cast<CheckListKslamatanPasienOKViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterAnestesi);
                            if (dokter != null)
                            {
                                model.ChckListKslmtnPsienOprs.DokterAnestesiNama = dokter.NamaDOkter;
                            }
                            var dokteranak = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterAnak);
                            if (dokteranak != null)
                            {
                                model.ChckListKslmtnPsienOprs.DokterAnakNama = dokteranak.NamaDOkter;
                            }
                            var perawatanestesi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PerawatAnestesi);
                            if (perawatanestesi != null)
                            {
                                model.ChckListKslmtnPsienOprs.PerawatAnestesiNama = perawatanestesi.NamaDOkter;
                            }
                            var perawatsirkuler = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PerawatSirkuler);
                            if (perawatsirkuler != null)
                            {
                                model.ChckListKslmtnPsienOprs.PerawatSirkulerNama = perawatsirkuler.NamaDOkter;
                            }
                            var operator1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Operator);
                            if (operator1 != null)
                            {
                                model.ChckListKslmtnPsienOprs.OperatorNama = operator1.NamaDOkter;
                            }
                            var istrumen = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Instrumen);
                            if (istrumen != null)
                            {
                                model.ChckListKslmtnPsienOprs.InstrumenNama = istrumen.NamaDOkter;
                            }
                            var asisten = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Asisten);
                            if (asisten != null)
                            {
                                model.ChckListKslmtnPsienOprs.AsistenNama = asisten.NamaDOkter;
                            }

                            model.ChckListKslmtnPsienOprs.Report = 1;
                            var fingerdokteranestesi = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.DokterAnestesi == model.ChckListKslmtnPsienOprs.DokterAnestesi && x.TandaTanganDokterAnestesi == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdokteranestesi != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegDokterAnestesi = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegDokterAnestesi = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.DokterAnestesi != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsDokterAnestesi = GetEncodeUrlChckListKslmtnPsienOprs_DokterAnestesi(model.ChckListKslmtnPsienOprs.DokterAnestesi, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }

                            var fingerdokteranak = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.DokterAnak == model.ChckListKslmtnPsienOprs.DokterAnak && x.TandaTanganDokterAnak == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdokteranak != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegDokterAnak = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegDokterAnak = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.DokterAnak != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsDokterAnak = GetEncodeUrlChckListKslmtnPsienOprs_DokterAnak(model.ChckListKslmtnPsienOprs.DokterAnak, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }

                            var fingerperawatanestesi = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.PerawatAnestesi == model.ChckListKslmtnPsienOprs.PerawatAnestesi && x.TandaTanganPerawatAnestesi == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawatanestesi != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegPerawatAnestesi = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegPerawatAnestesi = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.PerawatAnestesi != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsPerawatAnestesi = GetEncodeUrlChckListKslmtnPsienOprs_PerawatAnestesi(model.ChckListKslmtnPsienOprs.PerawatAnestesi, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }

                            var fingerperawatsirkuler = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.PerawatSirkuler == model.ChckListKslmtnPsienOprs.PerawatSirkuler && x.TandaTanganPerawatSirkuler == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerperawatsirkuler != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegPerawatSirkuler = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegPerawatSirkuler = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.PerawatSirkuler != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsPerawatSirkuler = GetEncodeUrlChckListKslmtnPsienOprs_PerawatSirkuler(model.ChckListKslmtnPsienOprs.PerawatSirkuler, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }

                            var fingeroperator = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.Operator == model.ChckListKslmtnPsienOprs.Operator && x.TandaTanganOperator == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingeroperator != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegOperator = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegOperator = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.Operator != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsOperator = GetEncodeUrlChckListKslmtnPsienOprs_Operator(model.ChckListKslmtnPsienOprs.Operator, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }

                            var fingerinstrumen = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.Instrumen == model.ChckListKslmtnPsienOprs.Instrumen && x.TandaTanganInstrumen == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerinstrumen != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegInstrumen = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegInstrumen = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.Instrumen != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsInstrumen = GetEncodeUrlChckListKslmtnPsienOprs_Instrumen(model.ChckListKslmtnPsienOprs.Instrumen, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }

                            var fingerasisten = eEMR.ChecklistKeselamatanPasienOperasi_OK.Where(x => x.Asisten == model.ChckListKslmtnPsienOprs.Asisten && x.TandaTanganAsisten == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerasisten != null)
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegAsisten = 1;
                            }
                            else
                            {
                                model.ChckListKslmtnPsienOprs.SudahRegAsisten = 0;
                            }
                            if (model.ChckListKslmtnPsienOprs.Asisten != null)
                            {
                                ViewBag.UrlFingerChckListKslmtnPsienOprsAsisten = GetEncodeUrlChckListKslmtnPsienOprs_Asisten(model.ChckListKslmtnPsienOprs.Asisten, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new ChecklistKeselamatanPasienOperasi_OK();
                            model.ChckListKslmtnPsienOprs = IConverter.Cast<CheckListKslamatanPasienOKViewModel>(item);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsDokterAnestesi = GetEncodeUrlChckListKslmtnPsienOprs_DokterAnestesi(model.ChckListKslmtnPsienOprs.DokterAnestesi, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsDokterAnak = GetEncodeUrlChckListKslmtnPsienOprs_DokterAnak(model.ChckListKslmtnPsienOprs.DokterAnak, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsPerawatAnestesi = GetEncodeUrlChckListKslmtnPsienOprs_PerawatAnestesi(model.ChckListKslmtnPsienOprs.PerawatAnestesi, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsPerawatSirkuler = GetEncodeUrlChckListKslmtnPsienOprs_PerawatSirkuler(model.ChckListKslmtnPsienOprs.PerawatSirkuler, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsOperator = GetEncodeUrlChckListKslmtnPsienOprs_Operator(model.ChckListKslmtnPsienOprs.Operator, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsInstrumen = GetEncodeUrlChckListKslmtnPsienOprs_Instrumen(model.ChckListKslmtnPsienOprs.Instrumen, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            ViewBag.UrlFingerChckListKslmtnPsienOprsAsisten = GetEncodeUrlChckListKslmtnPsienOprs_Asisten(model.ChckListKslmtnPsienOprs.Asisten, model.ChckListKslmtnPsienOprs.NoReg, sectionid);
                            model.ChckListKslmtnPsienOprs.Report = 0;
                            model.ChckListKslmtnPsienOprs.NoReg = noreg;
                            model.ChckListKslmtnPsienOprs.NRM = model.NRM;
                            model.ChckListKslmtnPsienOprs.SectionID = sectionid;
                            model.ChckListKslmtnPsienOprs.Tanggal = DateTime.Today;
                            model.ChckListKslmtnPsienOprs.Tgl_SignIn = DateTime.Today;
                            model.ChckListKslmtnPsienOprs.Tgl_SignOut = DateTime.Today;
                            model.ChckListKslmtnPsienOprs.Tgl_TimeOut = DateTime.Today;
                            model.ChckListKslmtnPsienOprs.Jam_SignIn = DateTime.Now;
                            model.ChckListKslmtnPsienOprs.Jam_SignOut = DateTime.Now;
                            model.ChckListKslmtnPsienOprs.Jam_TimeOut = DateTime.Now;
                           
                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string ChecklistKeselamatanPasienOperasi_OK_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.ChckListKslmtnPsienOprs.NoReg);
                            item.ChckListKslmtnPsienOprs.NoReg = item.ChckListKslmtnPsienOprs.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.ChckListKslmtnPsienOprs.SectionID = sectionid;
                            item.ChckListKslmtnPsienOprs.NRM = item.ChckListKslmtnPsienOprs.NRM;
                            var model = eEMR.ChecklistKeselamatanPasienOperasi_OK.FirstOrDefault(x => x.SectionID == item.ChckListKslmtnPsienOprs.SectionID && x.NoReg == item.ChckListKslmtnPsienOprs.NoReg);

                            if (model == null)
                            {
                                model = new ChecklistKeselamatanPasienOperasi_OK();
                                var o = IConverter.Cast<ChecklistKeselamatanPasienOperasi_OK>(item.ChckListKslmtnPsienOprs);
                                eEMR.ChecklistKeselamatanPasienOperasi_OK.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<ChecklistKeselamatanPasienOperasi_OK>(item.ChckListKslmtnPsienOprs);
                                eEMR.ChecklistKeselamatanPasienOperasi_OK.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFCheckListKeselamatanPasienOperasiOK(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_ChecklistKeselamatanPasienOperasi_OK.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_ChecklistKeselamatanPasienOperasi_OK", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_ChecklistKeselamatanPasienOperasi_OK;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_DokterAnestesi(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntdokteranestesi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_DokterAnak(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntdokteranak";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_PerawatAnestesi(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntperawatanestesi";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_PerawatSirkuler(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntperawatsirkuler";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_Operator(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntoperator";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_Instrumen(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntistrumen";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }

        public string GetEncodeUrlChckListKslmtnPsienOprs_Asisten(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifChckListKslmtnPsienOprs/get_keychckliskslmntasisten";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}