﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class SuratPersetujuanPemeriksaanUntukKepentinganPenyidikanController : Controller
    {
        // GET: SuratPersetujuanPemeriksaanUntukKepentinganPenyidikan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.SectionID = sectionid;
                    model.NoReg = m.NoReg;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.SPPemeriksaanKepentinganPenyidikan = IConverter.Cast<SuratPersetujuanPemeriksaanPenyidikanViewModel>(item);
                            var pernyataan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.MemberiPernyataan);
                            if (pernyataan != null)
                            {
                                model.SPPemeriksaanKepentinganPenyidikan.MemberiPernyataanNama = pernyataan.NamaDOkter;
                            }
                            model.SPPemeriksaanKepentinganPenyidikan.Tanggal = DateTime.Today;
                            model.SPPemeriksaanKepentinganPenyidikan.NamaBertandaTangan = m.NamaPasien;
                            model.SPPemeriksaanKepentinganPenyidikan.JKBertandaTangan = m.JenisKelamin;
                            model.SPPemeriksaanKepentinganPenyidikan.UmurBertandaTangan = m.UmurThn.ToString();
                            model.SPPemeriksaanKepentinganPenyidikan.AlamatBertandaTangan = m.Alamat;
                            model.SPPemeriksaanKepentinganPenyidikan.PekerjaanBertandaTangan = m.Pekerjaan;

                            var fingerdpjp = eEMR.SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan.Where(x => x.MemberiPernyataan == model.SPPemeriksaanKepentinganPenyidikan.MemberiPernyataan && x.TandaTanganPetugas == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdpjp != null)
                            {
                                model.SPPemeriksaanKepentinganPenyidikan.SudahRegPetugas = 1;
                            }
                            else
                            {
                                model.SPPemeriksaanKepentinganPenyidikan.SudahRegPetugas = 0;
                            }

                            if (model.SPPemeriksaanKepentinganPenyidikan.MemberiPernyataan != null)
                            {
                                ViewBag.UrlFingerSPPemeriksaanKepentinganPenyidikanMemberiPernyataan = GetEncodeUrlSPPemeriksaanKepentinganPenyidikan(model.SPPemeriksaanKepentinganPenyidikan.MemberiPernyataan, model.SPPemeriksaanKepentinganPenyidikan.NoReg, sectionid);
                            }
                        }
                        else
                        {
                            item = new SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan();
                            model.SPPemeriksaanKepentinganPenyidikan = IConverter.Cast<SuratPersetujuanPemeriksaanPenyidikanViewModel>(item);
                            ViewBag.UrlFingerSPPemeriksaanKepentinganPenyidikanMemberiPernyataan = GetEncodeUrlSPPemeriksaanKepentinganPenyidikan(model.SPPemeriksaanKepentinganPenyidikan.MemberiPernyataan, model.SPPemeriksaanKepentinganPenyidikan.NoReg, sectionid);
                            model.SPPemeriksaanKepentinganPenyidikan.Tanggal = DateTime.Today;
                            model.SPPemeriksaanKepentinganPenyidikan.NamaBertandaTangan = m.NamaPasien;
                            model.SPPemeriksaanKepentinganPenyidikan.JKBertandaTangan = m.JenisKelamin;
                            model.SPPemeriksaanKepentinganPenyidikan.UmurBertandaTangan = m.UmurThn.ToString();
                            model.SPPemeriksaanKepentinganPenyidikan.AlamatBertandaTangan = m.Alamat;
                            model.SPPemeriksaanKepentinganPenyidikan.PekerjaanBertandaTangan = m.Pekerjaan;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string SuratPersetujuanPemeriksaanUntukKepentinganPenyidikan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.SPPemeriksaanKepentinganPenyidikan.NoReg = noreg;
                            item.SPPemeriksaanKepentinganPenyidikan.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.SPPemeriksaanKepentinganPenyidikan.NRM = m.NRM;
                            item.SPPemeriksaanKepentinganPenyidikan.Tanggal = DateTime.Now;
                            var model = eEMR.SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan.FirstOrDefault(x => x.SectionID == item.SPPemeriksaanKepentinganPenyidikan.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan();
                                var o = IConverter.Cast<SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan>(item.SPPemeriksaanKepentinganPenyidikan);
                                eEMR.SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan>(item.SPPemeriksaanKepentinganPenyidikan);
                                eEMR.SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan_IGD.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan_IGD", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"SuratPersetujuanPemeriksaanKedokteranUntukPenyidikan_IGD;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlSPPemeriksaanKepentinganPenyidikan(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifSuratPersetujuanPemeriksaanUntukKepentinganPenyidikan/get_keysppeyidikankedokter";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}