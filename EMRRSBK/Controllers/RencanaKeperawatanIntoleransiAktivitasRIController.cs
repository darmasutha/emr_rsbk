﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RencanaKeperawatanIntoleransiAktivitasRIController : Controller
    {
        // GET: RencanaKeperawatanIntoleransiAktivitasRI
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RencanaKeperawatanIntoleransiAktivitas.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RncanaKprwtIntolrnsi = IConverter.Cast<RencanaKeperawatanIntoleransiAktivitasViewModel>(item);
                            var paraf = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodeDokter);
                            if (paraf != null)
                            {
                                model.RncanaKprwtIntolrnsi.KodeDokterNama = paraf.NamaDOkter;
                            }
                            model.RncanaKprwtIntolrnsi.NoReg = noreg;
                            model.RncanaKprwtIntolrnsi.NRM = model.NRM;
                            model.RncanaKprwtIntolrnsi.SectionID = sectionid;
                            model.RncanaKprwtIntolrnsi.Report = 1;
                            model.RncanaKprwtIntolrnsi.NamaPasien = m.NamaPasien;
                            model.RncanaKprwtIntolrnsi.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtIntolrnsi.TglLahir = m.TglLahir;
                        }
                        else
                        {
                            item = new RencanaKeperawatanIntoleransiAktivitas();
                            model.RncanaKprwtIntolrnsi = IConverter.Cast<RencanaKeperawatanIntoleransiAktivitasViewModel>(item);
                            model.RncanaKprwtIntolrnsi.Report = 0;
                            model.RncanaKprwtIntolrnsi.NamaPasien = m.NamaPasien;
                            model.RncanaKprwtIntolrnsi.JenisKelamin = m.JenisKelamin;
                            model.RncanaKprwtIntolrnsi.TglLahir = m.TglLahir;
                            model.RncanaKprwtIntolrnsi.NoReg = noreg;
                            model.RncanaKprwtIntolrnsi.NRM = model.NRM;
                            model.RncanaKprwtIntolrnsi.SectionID = sectionid;
                            model.RncanaKprwtIntolrnsi.Tanggal = DateTime.Today;
                            model.RncanaKprwtIntolrnsi.Jam = DateTime.Now;
                        }


                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RencanaKeperawatanIntoleransiAktivitas_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RncanaKprwtIntolrnsi.NoReg = item.RncanaKprwtIntolrnsi.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.RncanaKprwtIntolrnsi.SectionID = sectionid;
                            item.RncanaKprwtIntolrnsi.NRM = item.RncanaKprwtIntolrnsi.NRM;
                            var model = eEMR.RencanaKeperawatanIntoleransiAktivitas.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.RncanaKprwtIntolrnsi.NoReg);

                            if (model == null)
                            {
                                model = new RencanaKeperawatanIntoleransiAktivitas();
                                var o = IConverter.Cast<RencanaKeperawatanIntoleransiAktivitas>(item.RncanaKprwtIntolrnsi);
                                eEMR.RencanaKeperawatanIntoleransiAktivitas.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RencanaKeperawatanIntoleransiAktivitas>(item.RncanaKprwtIntolrnsi);
                                eEMR.RencanaKeperawatanIntoleransiAktivitas.AddOrUpdate(model);
                            }


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRencanaKeperawatanIntoleransiAktivitas(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RencanaKeperawatanIntoleransiAktivitas.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                var i = 0;
                cmd.Add(new SqlCommand("Rpt_RencanaKeperawatanIntoleransiAktivitas", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_RencanaKeperawatanIntoleransiAktivitas;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand($"Rpt_RencanaKeperawatanIntoleransiAktivitas_Detail", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables[$"Rpt_RencanaKeperawatanIntoleransiAktivitas_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}