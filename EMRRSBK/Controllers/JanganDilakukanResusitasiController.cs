﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class JanganDilakukanResusitasiController : Controller
    {
        // GET: JanganDilakukanResusitasi
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = m.NoReg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.JanganDilakukanResusitasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.JngnRisusitasi = IConverter.Cast<JanganDilakukanResusitasiViewModel>(item);
                            var pernyataan = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DPJP);
                            if (pernyataan != null)
                            {
                                model.JngnRisusitasi.DPJPNama = pernyataan.NamaDOkter;
                            }
                            model.JngnRisusitasi.SectionID = sectionid;
                            model.JngnRisusitasi.Tanggal = DateTime.Today;
                            model.JngnRisusitasi.NamaPasien = m.NamaPasien;
                            model.JngnRisusitasi.TglLahir = m.TglLahir;
                            model.JngnRisusitasi.JenisKelamin = m.JenisKelamin;

                            var fingerdpjp = eEMR.JanganDilakukanResusitasi.Where(x => x.DPJP == model.JngnRisusitasi.DPJP && x.TandaTanganDPJP == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerdpjp != null)
                            {
                                model.JngnRisusitasi.SudahRegDPJP = 1;
                            }
                            else
                            {
                                model.JngnRisusitasi.SudahRegDPJP = 0;
                            }

                            var fingerpasien = eEMR.JanganDilakukanResusitasi.Where(x => x.NRM == model.JngnRisusitasi.NRM && x.TandaTangan == null && x.NRM == model.NRM).FirstOrDefault();
                            if (fingerpasien != null)
                            {
                                model.JngnRisusitasi.SudahRegPasien = 1;
                            }
                            else
                            {
                                model.JngnRisusitasi.SudahRegPasien = 0;
                            }

                            if (model.JngnRisusitasi.DPJP != null)
                            {
                                ViewBag.UrlFingerJngnRisusitasiDPJP = GetEncodeUrlJngnRisusitasi(model.JngnRisusitasi.DPJP, model.JngnRisusitasi.NoReg, sectionid);
                            }

                            ViewBag.UrlFingerJngnRisusitasiPasien = GetEncodeUrlJngnRisusitasi(model.JngnRisusitasi.NRM, model.JngnRisusitasi.NoReg, sectionid);

                        }
                        else
                        {
                            item = new JanganDilakukanResusitasi();
                            model.JngnRisusitasi = IConverter.Cast<JanganDilakukanResusitasiViewModel>(item);
                            ViewBag.UrlFingerJngnRisusitasiDPJP = GetEncodeUrlJngnRisusitasi(model.JngnRisusitasi.DPJP, model.JngnRisusitasi.NoReg, sectionid);
                            ViewBag.UrlFingerJngnRisusitasiPasien = GetEncodeUrlJngnRisusitasi(model.JngnRisusitasi.NRM, model.JngnRisusitasi.NoReg, sectionid);
                            model.JngnRisusitasi.Tanggal = DateTime.Today;
                            model.JngnRisusitasi.NamaPasien = m.NamaPasien;
                            model.JngnRisusitasi.TglLahir = m.TglLahir;
                            model.JngnRisusitasi.JenisKelamin = m.JenisKelamin;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string JanganDilakukanResusitasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.JngnRisusitasi.NoReg = noreg;
                            item.JngnRisusitasi.SectionID = sectionid;
                            item.JngnRisusitasi.NRM = m.NRM;
                            item.JngnRisusitasi.Tanggal = DateTime.Now;
                            var model = eEMR.JanganDilakukanResusitasi.FirstOrDefault(x => x.SectionID == item.JngnRisusitasi.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new JanganDilakukanResusitasi();
                                var o = IConverter.Cast<JanganDilakukanResusitasi>(item.JngnRisusitasi);
                                eEMR.JanganDilakukanResusitasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<JanganDilakukanResusitasi>(item.JngnRisusitasi);
                                eEMR.JanganDilakukanResusitasi.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFJanganDilakukanResusitasi(string noreg, string sectionid)
        {
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            var rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"JanganDilakukanResusitasi_IGD.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand($"JanganDilakukanResusitasi_IGD", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"JanganDilakukanResusitasi_IGD;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                //cmd.Add(new SqlCommand("ImplementasiRisikoJatuhAnak", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["ImplementasiRisikoJatuhAnak;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }

        public string GetEncodeUrlJngnRisusitasi(string id, string noreg, string section)
        {
            using (var eSIM = new SIM_Entities())
            {
                var urlgetkey = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifJanganLakukanRestuisi/get_keyskdpjp";
                //var a = Server.MapPath("~/RegisterFinger/GetKey");
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(urlgetkey + "?userid=" + id + "&noreg=" + noreg + "&section=" + section);
                var url = System.Convert.ToBase64String(plainTextBytes);
                return url;

            }
        }
    }
}