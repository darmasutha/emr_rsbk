﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class TindakanKedokteranController : Controller
    {
        // GET: TindakanKedokteran
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_IdentitasPasien.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsesmenTindakanKedokteran.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.tndkkedokteranIGD = IConverter.Cast<TindakanKedokteranIGDViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.DokterPelaksananTindakan);
                            if (dokter != null)
                            {
                                model.tndkkedokteranIGD.DokterPelaksananTindakanNama = dokter.NamaDOkter;
                            }
                            model.tndkkedokteranIGD.Penerima_Tgl = DateTime.Today;
                            model.tndkkedokteranIGD.Penolakan_Tgl = DateTime.Today;
                            model.tndkkedokteranIGD.Persetujuan_Tgl = DateTime.Today;
                            model.tndkkedokteranIGD.Tanggal = DateTime.Today;
                            model.tndkkedokteranIGD.NamaPasien = m.NamaPasien;
                            model.tndkkedokteranIGD.NamaPasien1 = m.NamaPasien;
                            model.tndkkedokteranIGD.TglLahir = m.TglLahir;
                            model.tndkkedokteranIGD.TglLahir1 = m.TglLahir;
                            model.tndkkedokteranIGD.JenisKelamin = m.JenisKelamin;
                            model.tndkkedokteranIGD.JenisKelamin1 = m.JenisKelamin;
                            model.tndkkedokteranIGD.AlamatPasien = m.Alamat;
                            model.tndkkedokteranIGD.AlamatPasien1 = m.Alamat;
                        }
                        else
                        {
                            item = new AsesmenTindakanKedokteran();
                            model.tndkkedokteranIGD = IConverter.Cast<TindakanKedokteranIGDViewModel>(item);
                            model.tndkkedokteranIGD.Penerima_Tgl = DateTime.Today;
                            model.tndkkedokteranIGD.Penolakan_Tgl = DateTime.Today;
                            model.tndkkedokteranIGD.Penolakan_TglLahir = m.TglLahir;
                            model.tndkkedokteranIGD.Persetujuan_Tgl = DateTime.Today;
                            model.tndkkedokteranIGD.Persetujuan_TglLahir = m.TglLahir;
                            model.tndkkedokteranIGD.Tanggal = DateTime.Today;
                            model.tndkkedokteranIGD.NamaPasien = m.NamaPasien;
                            model.tndkkedokteranIGD.NamaPasien1 = m.NamaPasien;
                            model.tndkkedokteranIGD.TglLahir = m.TglLahir;
                            model.tndkkedokteranIGD.TglLahir1 = m.TglLahir;
                            model.tndkkedokteranIGD.JenisKelamin = m.JenisKelamin;
                            model.tndkkedokteranIGD.JenisKelamin1 = m.JenisKelamin;
                            model.tndkkedokteranIGD.AlamatPasien = m.Alamat;
                            model.tndkkedokteranIGD.AlamatPasien1 = m.Alamat;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string TindakanKedokteran_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.tndkkedokteranIGD.NoReg = noreg;
                            item.tndkkedokteranIGD.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.tndkkedokteranIGD.NRM = m.NRM;
                            item.tndkkedokteranIGD.Tanggal = DateTime.Now;
                            var model = eEMR.AsesmenTindakanKedokteran.FirstOrDefault(x => x.SectionID == item.tndkkedokteranIGD.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new AsesmenTindakanKedokteran();
                                var o = IConverter.Cast<AsesmenTindakanKedokteran>(item.tndkkedokteranIGD);
                                eEMR.AsesmenTindakanKedokteran.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsesmenTindakanKedokteran>(item.tndkkedokteranIGD);
                                eEMR.AsesmenTindakanKedokteran.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFTindakanKedokteran(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"AssTindakanKedokteran.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"AssTindakanKedokteran", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"AssTindakanKedokteran;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}