﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class AsuhanKeperawatanPeriOperatifController : Controller
    {
        // GET: AsuhanKeperawatanPeriOperatif
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsuhanKeperawatanPreOperatif.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AshnPreOpera = IConverter.Cast<AsuhanKeperawatanPeriOperatifViewModel>(item);
                           
                            model.AshnPreOpera.NamaPasien = m.NamaPasien;
                            model.AshnPreOpera.JenisKelamin = m.JenisKelamin;
                            model.AshnPreOpera.Umur = m.UmurThn;
                            model.AshnPreOpera.Report = 1;
                        }
                        else
                        {
                            item = new AsuhanKeperawatanPreOperatif();
                            model.AshnPreOpera = IConverter.Cast<AsuhanKeperawatanPeriOperatifViewModel>(item);
                            model.AshnPreOpera.NoReg = noreg;
                            model.AshnPreOpera.NamaPasien = m.NamaPasien;
                            model.AshnPreOpera.JenisKelamin = m.JenisKelamin;
                            model.AshnPreOpera.Umur = m.UmurThn;
                            model.AshnPreOpera.NRM = m.NRM;
                            model.AshnPreOpera.SectionID = sectionid;
                            model.AshnPreOpera.Tanggal = DateTime.Today;
                            model.AshnPreOpera.MulaiAnastesi = DateTime.Now;
                            model.AshnPreOpera.SelesaiAnastesi = DateTime.Now;
                            model.AshnPreOpera.JAM_DiagnosaKeperawatan = DateTime.Now;
                            model.AshnPreOpera.JAM_DiagnosaKeperawatanNyeri = DateTime.Now;
                            model.AshnPreOpera.JAM_DiagnosaKeperawatanResiko = DateTime.Now;
                            model.AshnPreOpera.JAM_Evaluasi = DateTime.Now;
                            model.AshnPreOpera.JAM_EvaluasiPasienMelaporkan = DateTime.Now;
                            model.AshnPreOpera.JAM_EvaluasiPasienMengatakan = DateTime.Now;
                            model.AshnPreOpera.JAM_PerencanaanImplementasi = DateTime.Now;
                            model.AshnPreOpera.JAM_PerencanaanImplementasiKajiderajat = DateTime.Now;
                            model.AshnPreOpera.JAM_PerencanaanImplementasiKajitingkat = DateTime.Now;
                            model.AshnPreOpera.Report = 0;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string AsuhanKeperawatanPreOperatif_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.AshnPreOpera.NoReg);
                            item.AshnPreOpera.NoReg = item.AshnPreOpera.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AshnPreOpera.SectionID = sectionid;
                            item.AshnPreOpera.NRM = item.AshnPreOpera.NRM;
                            var model = eEMR.AsuhanKeperawatanPreOperatif.FirstOrDefault(x => x.SectionID == item.AshnPreOpera.SectionID && x.NoReg == item.AshnPreOpera.NoReg);

                            if (model == null)
                            {
                                model = new AsuhanKeperawatanPreOperatif();
                                var o = IConverter.Cast<AsuhanKeperawatanPreOperatif>(item.AshnPreOpera);
                                eEMR.AsuhanKeperawatanPreOperatif.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsuhanKeperawatanPreOperatif>(item.AshnPreOpera);
                                eEMR.AsuhanKeperawatanPreOperatif.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFAsuhanKeperawatanPeriOperatif(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsuhanKeperawatanPreOperatif.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_AsuhanKeperawatanPreOperatif", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_AsuhanKeperawatanPreOperatif;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}