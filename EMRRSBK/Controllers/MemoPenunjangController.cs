﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data.Entity.Validation;

namespace EMRRSBK.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class MemoPenunjangController : Controller
    {
        #region ==== CREATE
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new OrderPenunjangViewModel();
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NoReg = noreg;
            ViewBag.Section = Request.Cookies["SectionIDPelayanan"].Value;
            using (var e = new EMR_Entities())
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == noreg);
                    if (m == null) return HttpNotFound();

                    ViewBag.DokterID = m.DokterID;
                    ViewBag.NamaDokter = m.NamaDOkter;
                    if (Request.Cookies["SectionIDPelayanan"].Value == "SEC002")
                    {
                        var diagnosa = e.AsesmenPengkajianTerintegrasiGawatDarurat.FirstOrDefault(x => x.NoReg == noreg);
                        if (diagnosa != null) {
                            ViewBag.DiagnosaIndikasi = diagnosa.DiagnosaMedis;
                        }
                    }
                    else
                    {
                        var diagnosacppt = e.KunjunganPoliklinik.OrderBy(x => x.Tanggal).FirstOrDefault(x => x.NoReg == noreg);
                        if (diagnosacppt != null)
                        {
                            ViewBag.DiagnosaIndikasi = diagnosacppt.Diagnosa;
                        }
                    }
                        
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new OrderPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                if (item.SectionID == null) throw new Exception("Ruangan tidak boleh kosong");
                                if (item.DokterID == null) throw new Exception("Dokter tidak boleh kosong");

                                var user = User.Identity.GetUserId();
                                var nobukti = "";

                                item.JamSampling = item.JamSampling_Tgl.Value.Date + item.JamSampling_Jam.Value.TimeOfDay;

                                nobukti = s.trOrder_insert_New(
                                    DateTime.Now,
                                    item.NoReg,
                                    "",
                                    item.Hasil_Cito == null ? false : true,
                                    false,
                                    "",
                                    item.DiagnosaIndikasi,
                                    item.DokterID,
                                    "",
                                    "",
                                    "",
                                    null,
                                    item.JamSampling,
                                    null,
                                    "",
                                    "",
                                    item.PemeriksaanTambahan,
                                    false,
                                    user,
                                    item.SectionID,
                                    item.SectionAsalID,
                                    false
                                ).FirstOrDefault();

                                if (item.OrderManual != null)
                                {
                                    var i = 1;
                                    foreach (var x in item.OrderManual)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetailManual>(item.OrderManual);
                                        m.NoBukti = nobukti;
                                        m.NoUrut = i;
                                        m.JasaID = x.JasaID;
                                        m.Qty = x.Qty;
                                        m.Tarif = x.Tarif;
                                        m.DokterID = x.DokterID;
                                        m.Keterangan = x.Keterangan;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetailManual.Add(m);

                                        i++;
                                    }
                                }

                                if (item.Jasa != null)
                                {
                                    foreach (var x in item.Jasa)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetail>(item.Jasa);
                                        m.NoBukti = nobukti;
                                        m.UrutJenis = x.UrutJenis;
                                        m.NamaJenis = x.NamaJenis;
                                        m.UrutKelompok = x.UrutKelompok;
                                        m.ID_Kelompok = x.ID_Kelompok;
                                        m.NamaKelompok = x.NamaKelompok;
                                        m.UrutJasa = x.UrutJasa;
                                        m.NamaTindakan = x.NamaTindakan;
                                        m.JasaID = x.JasaID;
                                        m.Dipilih = true;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetail.Add(m);
                                    }
                                }

                                var r = s.SaveChanges();

                                dbContextTransaction.Commit();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"OrderPenunjang Create {item.NoReg} {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });

                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }

                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== EDIT
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            var model = new OrderPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                model = IConverter.Cast<OrderPenunjangViewModel>(m);

                var d = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == nobukti).ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_OrderManual>(x));
                foreach (var dd in d)
                {
                    var jj = s.SIMmListJasa.FirstOrDefault(x => x.JasaID == dd.JasaID);
                    if (jj != null)
                    {
                        dd.NamaJasa = jj.JasaName;
                    }
                    dd.Tarif_View = IConverter.ToMoney(decimal.Parse(dd.Tarif.ToString()));

                    var tt = s.mDokter.FirstOrDefault(x => x.DokterID == dd.DokterID);
                    if (tt != null)
                    {
                        dd.NamaDokter = tt.NamaDOkter;
                    }
                }
                model.OrderManual = d;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                if (dokter != null)
                {
                    model.NamaDokter = dokter.NamaDOkter;
                }

                model.DokterID = m.DokterID;
                model.NoReg = m.Noreg;
                model.SectionID = m.SectionID;
                model.JamSampling_Tgl = m.JamSampling.Value.Date;
                model.JamSampling_Jam = m.JamSampling;


            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new OrderPenunjangViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region === VALIDASI
                                if (item.SectionID == null) throw new Exception("Ruangan tidak boleh kosong");
                                //if (item.OrderManual == null && item.Jasa == null)
                                //{
                                //    throw new Exception("Salah satu detail jasa harus diisi");
                                //}
                                var Order = s.trOrderPenunjang.Where(x => x.NoBukti == item.NoBukti).FirstOrDefault();
                                if (Order == null)
                                {
                                    throw new Exception("Order Penunjang tidak ditemukan");
                                }
                                #endregion

                                #region === DELETE DETAIL
                                var trOrderPenunjangDetailManual = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == Order.NoBukti);
                                if (trOrderPenunjangDetailManual != null)
                                {
                                    s.trOrderPenunjangDetailManual.RemoveRange(trOrderPenunjangDetailManual);
                                }

                                var trOrderPenunjangDetail = s.trOrderPenunjangDetail.Where(x => x.NoBukti == Order.NoBukti);
                                if (trOrderPenunjangDetail != null)
                                {
                                    s.trOrderPenunjangDetail.RemoveRange(trOrderPenunjangDetail);
                                }
                                #endregion

                                var user = User.Identity.GetUserId();
                                var nobukti = Order.NoBukti;

                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.DiagnosaIndikasi = item.DiagnosaIndikasi;
                                Order.DokterID = item.DokterID;
                                Order.PemeriksaanTambahan = item.PemeriksaanTambahan;
                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.Hasil_Cito = item.Hasil_Cito;
                                Order.JamSampling = item.JamSampling_Tgl.Value.Date + item.JamSampling_Jam.Value.TimeOfDay;
                                Order.UserId = user;

                                if (item.OrderManual != null)
                                {
                                    var i = 1;
                                    foreach (var x in item.OrderManual)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetailManual>(item.OrderManual);
                                        m.NoBukti = nobukti;
                                        m.NoUrut = i;
                                        m.JasaID = x.JasaID;
                                        m.Qty = x.Qty;
                                        m.Tarif = x.Tarif;
                                        m.DokterID = x.DokterID;
                                        m.Keterangan = x.Keterangan;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetailManual.Add(m);

                                        i++;
                                    }
                                }

                                if (item.Jasa != null)
                                {
                                    foreach (var x in item.Jasa)
                                    {
                                        var m = IConverter.Cast<trOrderPenunjangDetail>(item.Jasa);
                                        m.NoBukti = nobukti;
                                        m.UrutJenis = x.UrutJenis;
                                        m.NamaJenis = x.NamaJenis;
                                        m.UrutKelompok = x.UrutKelompok;
                                        m.ID_Kelompok = x.ID_Kelompok;
                                        m.NamaKelompok = x.NamaKelompok;
                                        m.UrutJasa = x.UrutJasa;
                                        m.NamaTindakan = x.NamaTindakan;
                                        m.JasaID = x.JasaID;
                                        m.Dipilih = true;
                                        m.Realisasi = false;
                                        s.trOrderPenunjangDetail.Add(m);
                                    }
                                }

                                var r = s.SaveChanges();

                                dbContextTransaction.Commit();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"OrderPenunjang Create {item.NoReg} {item.SectionID}"
                                };
                                UserActivity.InsertUserActivity(userActivity);

                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });

                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new ArgumentNullException("connection", "Masih ada transaksi, silahkan simpan ulang");
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                                throw new Exception(ex.Message);
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message + " - " + ex.InnerException);
                            }

                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail_Get(string nobukti)
        {
            var model = new OrderPenunjangViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                model = IConverter.Cast<OrderPenunjangViewModel>(m);

                var d = s.trOrderPenunjangDetailManual.Where(x => x.NoBukti == nobukti).ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_OrderManual>(x));
                foreach (var dd in d)
                {
                    var jj = s.SIMmListJasa.FirstOrDefault(x => x.JasaID == dd.JasaID);
                    if (jj != null)
                    {
                        dd.NamaJasa = jj.JasaName;
                    }
                    dd.Tarif_View = IConverter.ToMoney(decimal.Parse(dd.Tarif.ToString()));

                    var tt = s.mDokter.FirstOrDefault(x => x.DokterID == dd.DokterID);
                    if (tt != null)
                    {
                        dd.NamaDokter = tt.NamaDOkter;
                    }
                }
                model.OrderManual = d;

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                if (dokter != null)
                {
                    model.NamaDokter = dokter.NamaDOkter;
                }

                model.DokterID = m.DokterID;
                model.NoReg = m.Noreg;
                model.SectionID = m.SectionID;

            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
        #endregion

        #region ==== GET DETAIL
        [HttpGet]
        public string get_JenisKelompokJasa(string section, string nobukti = null)
        {
            try
            {
                object Jenis = new Vw_mOrderPenunjang_Jenis();
                object Kelompok = new Vw_mOrderPenunjang_Kelompok();
                object Jasa = new Vw_mOrderPenunjang_DetailJasa();
                object JasaSave = new trOrderPenunjangDetail();
                using (var s = new SIM_Entities())
                {
                    Jenis = s.Vw_mOrderPenunjang_Jenis.Where(x => x.SectionID == section).ToList();
                    Kelompok = s.Vw_mOrderPenunjang_Kelompok.ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_Kelompok>(x));
                    Jasa = s.Vw_mOrderPenunjang_DetailJasa.ToList().ConvertAll(x => IConverter.Cast<OrderPenunjang_Jasa>(x));
                    if (nobukti != null)
                    {
                        JasaSave = s.trOrderPenunjangDetail.Where(x => x.NoBukti == nobukti).ToList();
                    }
                }
                if (nobukti == null)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Jenis = Jenis,
                        Kelompok = Kelompok,
                        Jasa = Jasa,
                        Message = "-"
                    });
                }
                else
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Jenis = Jenis,
                        Kelompok = Kelompok,
                        Jasa = Jasa,
                        JasaSave = JasaSave,
                        Message = "-"
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== D E L E T E

        [HttpPost]
        public string Delete(string nobukti)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.trOrderPenunjang.FirstOrDefault(x => x.NoBukti == nobukti);
                    if (m == null) throw new Exception("Data Tidak ditemukan");

                    s.trOrder_insert_batal(nobukti);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Order Penunjang delete {nobukti}"
                    };
                    UserActivity.InsertUserActivity(userActivity);

                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== L O O K U P  J A S A
        [HttpPost]
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_LookupTarif_Result> proses = s.Pelayanan_LookupTarif(filter[10] == "True" ? "" : Request.Cookies["SectionIDPelayanan"].Value);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaName)}.Contains(@0)", filter[1]);
                    //if (!string.IsNullOrEmpty(filter[2]))
                    //    proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.Kategori)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Pelayanan_LookupTarif_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region === G E T  TA R I F 
        [HttpPost]
        public string GetTarif(string jasa, string dokter, string section, string noreg, int? kategorioperasi, int? cito)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var p = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var c = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    var h = s.GetTarifBiaya_Global(jasa, dokter, cito, (kategorioperasi == null ? 1 : kategorioperasi), section, noreg, "1").FirstOrDefault();
                    var m = new
                    {
                        jasa = jasa,
                        dokter = dokter,
                        cito = cito,
                        kategorioperasi = kategorioperasi,
                        section = section,
                        noreg = noreg
                    };
                    if (h == null) throw new Exception($"GetTarifBiaya_Global tidak ditemukan " + m.ToString());
                    var d = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, p.JenisKerjasamaID, "1", c.UnitBisnisID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d,
                        Tarif = h.Harga ?? 0
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var noreg = filter[0];
                    var section = Request.Cookies["SectionIDPelayanan"].Value;
                    IQueryable<Pelayanan_GetMemoPenunjang> proses = s.Pelayanan_GetMemoPenunjang.Where(x => x.NoReg == noreg && x.SectionID == section && x.Batal == false);
                    proses = proses.Where($"{nameof(SIMtrMemoPenunjang.NoReg)}=@0", filter[0]);
                    proses = proses.Where($"{nameof(SIMtrMemoPenunjang.SectionID)}=@0", section);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<MemoPenunjangViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Jam_View = x.Jam.ToString("HH") + ":" + x.Jam.ToString("mm");
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}