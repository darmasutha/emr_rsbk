﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class LaporanOperasiController : Controller
    {
        // GET: LaporanOperasi
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.LaporanOperasi.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.LprnOprs = IConverter.Cast<LaporanOperasiViewModel>(item);
                            var operatr = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Operator);
                            if (operatr != null)
                            {
                                model.LprnOprs.OperatorNama = operatr.NamaDOkter;
                            }

                            var asstn1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Asisten1);
                            if (asstn1 != null)
                            {
                                model.LprnOprs.Asisten1Nama = asstn1.NamaDOkter;
                            }

                            var asstn2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Asisten2);
                            if (asstn2 != null)
                            {
                                model.LprnOprs.Asisten2Nama = asstn2.NamaDOkter;
                            }

                            var prwtinstrm = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PerawatInstrumen);
                            if (prwtinstrm != null)
                            {
                                model.LprnOprs.PerawatInstrumenNama = prwtinstrm.NamaDOkter;
                            }

                            var anestesi = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Anestesi);
                            if (anestesi != null)
                            {
                                model.LprnOprs.AnestesiNama = anestesi.NamaDOkter;
                            }

                            var penataanes = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.PenataAnestesi);
                            if (penataanes != null)
                            {
                                model.LprnOprs.PenataAnestesiNama = penataanes.NamaDOkter;
                            }

                            var onloop = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.OnLoop);
                            if (onloop != null)
                            {
                                model.LprnOprs.OnLoopNama = onloop.NamaDOkter;
                            }
                            model.LprnOprs.NamaPasien = m.NamaPasien;
                            model.LprnOprs.NoReg = noreg;
                            model.LprnOprs.NRM = model.NRM;
                            model.LprnOprs.SectionID = sectionid;
                            model.LprnOprs.JenisKelamin = m.JenisKelamin;
                            model.LprnOprs.Report = 1;
                            model.LprnOprs.Umur = m.UmurThn;
                        }
                        else
                        {
                            item = new LaporanOperasi();
                            model.LprnOprs = IConverter.Cast<LaporanOperasiViewModel>(item);
                            model.LprnOprs.NamaPasien = m.NamaPasien;
                            model.LprnOprs.NoReg = noreg;
                            model.LprnOprs.NRM = model.NRM;
                            model.LprnOprs.SectionID = sectionid;
                            model.LprnOprs.Umur = m.UmurThn;
                            model.LprnOprs.JenisKelamin = m.JenisKelamin;
                            model.LprnOprs.Tanggal = DateTime.Today;
                            model.LprnOprs.Jam = DateTime.Now;
                            model.LprnOprs.Report = 0;
                        }

                        var List = eEMR.InstruksiPascaOperasi_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (List != null)
                        {

                            model.LprnOprs.Intruksi_List = new ListDetail<IntruksiPascaOperasiModelDetail>();

                            foreach (var x1 in List)
                            {
                                var y = IConverter.Cast<IntruksiPascaOperasiModelDetail>(x1);
                                model.LprnOprs.Intruksi_List.Add(false, y);
                            }

                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string LaporanOperasi_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.LprnOprs.NoReg = item.LprnOprs.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.LprnOprs.SectionID = sectionid;
                            item.LprnOprs.NRM = item.LprnOprs.NRM;
                            var model = eEMR.LaporanOperasi.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.LprnOprs.NoReg);

                            if (model == null)
                            {
                                model = new LaporanOperasi();
                                var o = IConverter.Cast<LaporanOperasi>(item.LprnOprs);
                                eEMR.LaporanOperasi.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<LaporanOperasi>(item.LprnOprs);
                                eEMR.LaporanOperasi.AddOrUpdate(model);
                            }

                            #region ===== DETAIL

                            if (item.LprnOprs.Intruksi_List == null) item.LprnOprs.Intruksi_List = new ListDetail<IntruksiPascaOperasiModelDetail>();
                            item.LprnOprs.Intruksi_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.LprnOprs.Intruksi_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.LprnOprs.NoReg;
                                x.Model.NRM = item.LprnOprs.NRM;
                                x.Model.Tanggal = DateTime.Today;
                                x.Model.Jam = DateTime.Now;

                            }
                            var new_List = item.LprnOprs.Intruksi_List;
                            var real_List = eEMR.InstruksiPascaOperasi_Detail.Where(x => x.SectionID == item.LprnOprs.SectionID && x.NoReg == item.LprnOprs.NoReg).ToList();
                            foreach (var x in real_List)
                            {
                                var z = new_List.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.InstruksiPascaOperasi_Detail.Remove(x);
                                }

                            }
                            foreach (var x in new_List)
                            {

                                var _m = real_List.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {

                                    _m = new InstruksiPascaOperasi_Detail();
                                    eEMR.InstruksiPascaOperasi_Detail.Add(IConverter.Cast<InstruksiPascaOperasi_Detail>(x.Model));
                                }

                                else
                                {

                                    _m = IConverter.Cast<InstruksiPascaOperasi_Detail>(x.Model);
                                    eEMR.InstruksiPascaOperasi_Detail.AddOrUpdate(_m);


                                }

                            }

                            #endregion


                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFLaporanOperasi(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_LaporanOperasi.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_LaporanOperasi", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_LaporanOperasi;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_InstruksiPascaOperasi_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_InstruksiPascaOperasi_Detail;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}