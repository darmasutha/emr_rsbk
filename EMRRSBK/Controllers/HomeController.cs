﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMRRSBK.Models;

namespace EMRRSBK.Controllers
{
    [Authorize(Roles = "Pelayanan")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName("Setting")]
        public ActionResult Setting_Get()
        {
            var model = new SettingViewModel();
            model.SerialNumber = Request.Cookies["SerialNumber"] == null ? "" : Request.Cookies["SerialNumber"].Value;
            var sett = Request.Cookies["PelayananRegistrasiPasienList"];
            if (sett == null)
                model.ListPasien = "Card";
            else
            {
                model.ListPasien = Request.Cookies["PelayananRegistrasiPasienList"].Value ?? "Card";
            }
            return View(model);
        }

        [HttpPost]
        [ActionName("Setting")]
        public string Setting_Post()
        {
            try
            {
                var item = new SettingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    Response.Cookies["PelayananRegistrasiPasienList"].Value = item.ListPasien;
                    Response.Cookies["PelayananRegistrasiPasienList"].Expires = DateTime.Today.AddYears(10);
                    Response.Cookies["SerialNumber"].Value = item.SerialNumber;
                    Response.Cookies["SerialNumber"].Expires = DateTime.Now.AddYears(100);
                    ResultSS result = new ResultSS(1);
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}