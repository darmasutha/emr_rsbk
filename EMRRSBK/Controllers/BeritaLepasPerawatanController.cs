﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace EMRRSBK.Controllers
{
    public class BeritaLepasPerawatanController : Controller
    {
        // GET: BeritaLepasPerawatan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.BeritaLepasPerawatan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.BeritaLpsPerawatan = IConverter.Cast<BeritaLepasPerawatanViewModel>(item);

                            model.BeritaLpsPerawatan.Nama = m.NamaPasien;
                            model.BeritaLpsPerawatan.Umur = m.UmurThn;
                            model.BeritaLpsPerawatan.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.BeritaLpsPerawatan.Alamat = m.Alamat;
                            model.BeritaLpsPerawatan.Pekrjaan = m.Pekerjaan;
                            model.BeritaLpsPerawatan.Report = 1;

                        }
                        else
                        {
                            item = new BeritaLepasPerawatan();
                            model.BeritaLpsPerawatan = IConverter.Cast<BeritaLepasPerawatanViewModel>(item);
                            model.BeritaLpsPerawatan.NoReg = m.NoReg;
                            model.BeritaLpsPerawatan.NRM = m.NRM;
                            model.BeritaLpsPerawatan.Tanggal = DateTime.Today;
                            model.BeritaLpsPerawatan.Jam = DateTime.Now;
                            model.BeritaLpsPerawatan.Nama = m.NamaPasien;
                            model.BeritaLpsPerawatan.Umur = m.UmurThn;
                            model.BeritaLpsPerawatan.JenisKelamin = (m.JenisKelamin == "M" ? "Laki-Laki" : "Perempuan");
                            model.BeritaLpsPerawatan.Pekrjaan = m.Pekerjaan;
                            model.BeritaLpsPerawatan.Alamat = m.Alamat;
                            model.BeritaLpsPerawatan.Report = 0;
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);

        }

        [HttpPost]
        [ActionName("Create")]
        public string BeritaLepasPerawatan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.BeritaLpsPerawatan.NoReg);
                            item.BeritaLpsPerawatan.NoReg = item.BeritaLpsPerawatan.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.BeritaLpsPerawatan.SectionID = sectionid;
                            item.BeritaLpsPerawatan.NRM = item.BeritaLpsPerawatan.NRM;
                            var model = eEMR.BeritaLepasPerawatan.FirstOrDefault(x => x.SectionID == item.BeritaLpsPerawatan.SectionID && x.NoReg == item.BeritaLpsPerawatan.NoReg);

                            if (model == null)
                            {
                                model = new BeritaLepasPerawatan();
                                var o = IConverter.Cast<BeritaLepasPerawatan>(item.BeritaLpsPerawatan);
                                eEMR.BeritaLepasPerawatan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<BeritaLepasPerawatan>(item.BeritaLpsPerawatan);
                                eEMR.BeritaLepasPerawatan.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}
