﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class AsuhanKeperawatanIntraOperatifController : Controller
    {
        // GET: AsuhanKeperawatanIntraOperatif
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionnama = Request.Cookies["SectionNamaPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.AsuhanKeperawatanIntraOperatif.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.AshnIntraOpera = IConverter.Cast<AsuhanKeperawatanIntraOperatifViewModel>(item);

                            model.AshnIntraOpera.NamaPasien = m.NamaPasien;
                            model.AshnIntraOpera.JenisKelamin = m.JenisKelamin;
                            model.AshnIntraOpera.Umur = m.UmurThn;
                            model.AshnIntraOpera.Report = 1;
                        }
                        else
                        {
                            item = new AsuhanKeperawatanIntraOperatif();
                            model.AshnIntraOpera = IConverter.Cast<AsuhanKeperawatanIntraOperatifViewModel>(item);
                            model.AshnIntraOpera.NoReg = noreg;
                            model.AshnIntraOpera.NamaPasien = m.NamaPasien;
                            model.AshnIntraOpera.JenisKelamin = m.JenisKelamin;
                            model.AshnIntraOpera.Umur = m.UmurThn;
                            model.AshnIntraOpera.NRM = m.NRM;
                            model.AshnIntraOpera.SectionID = sectionid;
                            model.AshnIntraOpera.Tanggal = DateTime.Today;
                            model.AshnIntraOpera.Jam = DateTime.Now;
                            model.AshnIntraOpera.JamDignosa = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaGangguanRasa = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaHambatanMobilitas = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaHipotermi = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaKomplikasi = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaPolaNafas = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaRisiko = DateTime.Now;
                            model.AshnIntraOpera.JamDignosaRisikoKecelakaan = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanAlurposisi = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanAlurposisi1 = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanAlurposisi2 = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanAlurposisi3 = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanBersihSecret = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanKaji = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanMempertahankanSuhu = DateTime.Now;
                            model.AshnIntraOpera.JamPerencanaanTingkatKeamanan = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam1 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam2 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam3 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam4 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam5 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam6 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam7 = DateTime.Now;
                            model.AshnIntraOpera.Evaluasi_Jam8 = DateTime.Now;
                            model.AshnIntraOpera.Report = 0;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string AsuhanKeperawatanIntraOperatif_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == item.AshnIntraOpera.NoReg);
                            item.AshnIntraOpera.NoReg = item.AshnIntraOpera.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.AshnIntraOpera.SectionID = sectionid;
                            item.AshnIntraOpera.NRM = item.AshnIntraOpera.NRM;
                            var model = eEMR.AsuhanKeperawatanIntraOperatif.FirstOrDefault(x => x.SectionID == item.AshnIntraOpera.SectionID && x.NoReg == item.AshnIntraOpera.NoReg);

                            if (model == null)
                            {
                                model = new AsuhanKeperawatanIntraOperatif();
                                var o = IConverter.Cast<AsuhanKeperawatanIntraOperatif>(item.AshnIntraOpera);
                                eEMR.AsuhanKeperawatanIntraOperatif.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<AsuhanKeperawatanIntraOperatif>(item.AshnIntraOpera);
                                eEMR.AsuhanKeperawatanIntraOperatif.AddOrUpdate(model);
                            }



                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFAsuhanKeperawatanIntraOperatif(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_AsuhanKeperawatanIntraOperatif.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_AsuhanKeperawatanIntraOperatif", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_AsuhanKeperawatanIntraOperatif;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}