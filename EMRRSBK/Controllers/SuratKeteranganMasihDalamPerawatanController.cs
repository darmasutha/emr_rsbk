﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class SuratKeteranganMasihDalamPerawatanController : Controller
    {
        // GET: SuratKeteranganMasihDalamPerawatan
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var itempoli = eEMR.KunjunganPoliklinik.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        var item = eEMR.SuratKeteranganMasihDalamPerawatan.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.SrtKetMasihDlmPerawatan = IConverter.Cast<SuratKeteranganMasihDalamPerawatanViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == itempoli.Paraf);
                            if (dokter != null) 
                            {
                                model.SrtKetMasihDlmPerawatan.KodeDokterNama = dokter.NamaDOkter;
                                model.SrtKetMasihDlmPerawatan.KodeDokter = dokter.DokterID;
                            }
                            model.SrtKetMasihDlmPerawatan.Tanggal = DateTime.Now;
                            model.SrtKetMasihDlmPerawatan.NamaPasien = m.NamaPasien;
                            model.SrtKetMasihDlmPerawatan.UmurPasien = m.UmurThn;
                            model.SrtKetMasihDlmPerawatan.JenisKelaminPasien = m.JenisKelamin;
                            model.SrtKetMasihDlmPerawatan.Diagnosa = itempoli.Diagnosa;
                            model.SrtKetMasihDlmPerawatan.Tindakan = itempoli.Intruksi;
                        }
                        else
                        {
                            item = new SuratKeteranganMasihDalamPerawatan();
                            model.SrtKetMasihDlmPerawatan = IConverter.Cast<SuratKeteranganMasihDalamPerawatanViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == itempoli.Paraf);
                            if (dokter != null)
                            {
                                model.SrtKetMasihDlmPerawatan.KodeDokterNama = dokter.NamaDOkter;
                                model.SrtKetMasihDlmPerawatan.KodeDokter = dokter.DokterID;
                            }
                            model.SrtKetMasihDlmPerawatan.Tanggal = DateTime.Now;
                            model.SrtKetMasihDlmPerawatan.NamaPasien = m.NamaPasien;
                            model.SrtKetMasihDlmPerawatan.NamaPasien = m.NamaPasien;
                            model.SrtKetMasihDlmPerawatan.UmurPasien = m.UmurThn;
                            model.SrtKetMasihDlmPerawatan.JenisKelaminPasien = m.JenisKelamin;
                            model.SrtKetMasihDlmPerawatan.Diagnosa = itempoli.Diagnosa;
                            model.SrtKetMasihDlmPerawatan.Tindakan = itempoli.Intruksi;
                            model.SrtKetMasihDlmPerawatan.Jabatan = "Dokter Rumah Sakit Bhayangkara Denpasar";
                        }
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string SuratKeteranganSrtKetMasihDlmPerawatan_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.SrtKetMasihDlmPerawatan.NoReg = noreg;
                            item.SrtKetMasihDlmPerawatan.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            item.SrtKetMasihDlmPerawatan.NRM = m.NRM;
                            item.SrtKetMasihDlmPerawatan.Tanggal = DateTime.Now;
                            var model = eEMR.SuratKeteranganMasihDalamPerawatan.FirstOrDefault(x => x.SectionID == item.SrtKetMasihDlmPerawatan.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new SuratKeteranganMasihDalamPerawatan();
                                var o = IConverter.Cast<SuratKeteranganMasihDalamPerawatan>(item.SrtKetMasihDlmPerawatan);
                                eEMR.SuratKeteranganMasihDalamPerawatan.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<SuratKeteranganMasihDalamPerawatan>(item.SrtKetMasihDlmPerawatan);
                                eEMR.SuratKeteranganMasihDalamPerawatan.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFSuratKeteranganSrtKetMasihDlmPerawatan(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_SuratKeteranganMasihDalamPerawatan.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"Rpt_SuratKeteranganMasihDalamPerawatan", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_SuratKeteranganMasihDalamPerawatan;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}