﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class PersetujuanDirawatICUController : Controller
    {
        // GET: PersetujuanDirawatICU
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    model.NoReg = noreg;
                    model.SectionID = sectionid;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.PersetujuanDirawatDiICU.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.PDirawatICU = IConverter.Cast<PersetujuanDirawatICUViewModel>(item);
                            var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Dokter);
                            if (dokter != null)
                            {
                                model.PDirawatICU.DokterNama = dokter.NamaDOkter;
                            }

                            var saksi1 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Saksi1);
                            if (saksi1 != null)
                            {
                                model.PDirawatICU.Saksi1Nama = saksi1.NamaDOkter;
                            }

                            var saksi2 = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.Saksi2);
                            if (saksi2 != null)
                            {
                                model.PDirawatICU.Saksi2Nama = saksi2.NamaDOkter;
                            }
                            model.PDirawatICU.NamaPasien = m.NamaPasien;
                            model.PDirawatICU.TglLahir = m.TglLahir;
                            model.PDirawatICU.JenisKelamin = m.JenisKelamin;
                            model.PDirawatICU.AlamatPasien = m.Alamat;
                        }
                        else
                        {
                            item = new PersetujuanDirawatDiICU();
                            model.PDirawatICU = IConverter.Cast<PersetujuanDirawatICUViewModel>(item);
                            model.PDirawatICU.NamaPasien = m.NamaPasien;
                            model.PDirawatICU.TglLahir = m.TglLahir;
                            model.PDirawatICU.JenisKelamin = m.JenisKelamin;
                            model.PDirawatICU.AlamatPasien = m.Alamat;
                            model.PDirawatICU.Tanggal = DateTime.Today;

                        }

                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string PersetujuanDirawatICU_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.PDirawatICU.NoReg = noreg;
                            item.PDirawatICU.SectionID =  Request.Cookies["SectionIDPelayanan"].Value;
                            item.PDirawatICU.NRM = m.NRM;
                            var model = eEMR.PersetujuanDirawatDiICU.FirstOrDefault(x => x.SectionID == item.PDirawatICU.SectionID && x.NoReg == item.NoReg);

                            if (model == null)
                            {
                                model = new PersetujuanDirawatDiICU();
                                var o = IConverter.Cast<PersetujuanDirawatDiICU>(item.PDirawatICU);
                                eEMR.PersetujuanDirawatDiICU.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<PersetujuanDirawatDiICU>(item.PDirawatICU);
                                eEMR.PersetujuanDirawatDiICU.AddOrUpdate(model);
                            }

                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string noreg)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["SectionNamaPelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"PersetujuanDirawatDi_ICU.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                //cmd.Add(new SqlCommand("SP_VW_Registrasi", conn));
                //cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                //cmd[i].CommandType = CommandType.StoredProcedure;
                //da.Add(new SqlDataAdapter(cmd[i]));
                //ds.Add(new DataSet());
                //da[i].Fill(ds[i]);
                //rd.Database.Tables["SP_VW_Registrasi;1"].SetDataSource(ds[i].Tables[0]);

                //i++;
                cmd.Add(new SqlCommand($"PersetujuanDirawatDi_ICU", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"PersetujuanDirawatDi_ICU;1"].SetDataSource(ds[i].Tables[0]);

            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}