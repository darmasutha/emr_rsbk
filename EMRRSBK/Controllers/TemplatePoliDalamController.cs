﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class TemplatePoliDalamController : Controller
    {
        // GET: TemplatePoliDalam
        [HttpPost]
        public string Template(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    IQueryable<Tem_IlmuPenyakitDalam> proses = s.Tem_IlmuPenyakitDalam.Where(x => x.ID == 2 || x.ID == 4);
                    //if (!string.IsNullOrEmpty(filter[0]))
                    //    proses = proses.Where($"{nameof(Tem_IlmuPenyakitDalam.ID)}.Contains(@0)", filter[0]);
                    //if (!string.IsNullOrEmpty(filter[1]))
                    //    proses = proses.Where($"{nameof(Tem_IlmuPenyakitDalam.Nama_Template)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    //var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}").ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<AssesmentIlmunPenyakitDalamViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }


        [HttpGet]
        public string GetTemplat(string id)
        {
            AssesmentIlmunPenyakitDalamViewModel item;
            try
            {
                int idtemplet = int.Parse(id);
                using (var s = new EMR_Entities())
                {
                    var t = s.Tem_IlmuPenyakitDalam.Where(x => x.ID == idtemplet).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmunPenyakitDalamViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliUmum(string id, string nama)
        {
            AssesmentIPoliumumViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.AsesmenPoliUmum.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIPoliumumViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatLembarGigi(string id, string nama)
        {
            LembarPoliGigiViewModel item;
            try
            {
                    using (var s = new EMR_Entities())
                    {
                        var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                        var tamplet = s.trTemplate.Where(xx => xx.DokumenID == nama && xx.NamaTemplate == id).FirstOrDefault();
                        var t = s.LembarPoliGigi.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                        item = IConverter.Cast<LembarPoliGigiViewModel>(t);
                    }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatHiperbarik1(string id, string nama)
        {
            AssesmentIKeperawatanHiperbarikViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.PengkajianAwalMedisDanKeperawatanHiperbarik1_1.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIKeperawatanHiperbarikViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatHiperbarik2(string id, string nama)
        {
            AssesmentIKeperawatanHiperbarik2ViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.DokumenID == id).FirstOrDefault();
                    var t = s.PengkajianAwalMedisDanKeperawatanHiperbarik1_2.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIKeperawatanHiperbarik2ViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatBedahTrauma(string id, string nama)
        {
            AssesmentIlmuPenyakitBedahTraumaViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitBedahTrauma.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitBedahTraumaViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatBedahNonTrauma(string id, string nama)
        {
            AssesmentIlmuPenyakitBedahNonTraumaViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitBedahNonTrauma.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitBedahNonTraumaViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatTHT(string id, string nama)
        {
            AssesmentIlmuPenyakitTHTViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitTHT.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitTHTViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatJiwa(string id, string nama)
        {
            AssesmentIlmuPenyakitJiwaViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitJiwa.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitJiwaViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliMata(string id, string nama)
        {
            AssesmentIlmuPenyakitMataViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitMata.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitMataViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliKulitKelamin(string id, string nama)
        {
            AssesmentIlmuKulitDanKelaminViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitKulitDanKelamin.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuKulitDanKelaminViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliKandungan(string id, string nama)
        {
            PengkajianAwalKebidananKandunganViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.PengkajianAwalKebidananKandungan.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<PengkajianAwalKebidananKandunganViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliAnak(string id, string nama)
        {
            AssesmentIlmuPenyakitAnakViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitAnak.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitAnakViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliSaraf(string id, string nama)
        {
            AssesmentIlmuPenyakitSarafViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.DokumenID == id).FirstOrDefault();
                    var t = s.IlmuPenyakitSaraf.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitSarafViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatIGDDokter(string id, string nama)
        {
            AssesmentPengkajianTerintetegrasiGawatDaruratViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.AsesmenPengkajianTerintegrasiGawatDarurat.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentPengkajianTerintetegrasiGawatDaruratViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatIGDPengkajianMedis(string id, string nama)
        {
            PengkajianMedisViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.AsesmenPengkajianTerintegrasiGawatDarurat.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<PengkajianMedisViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        
        [HttpGet]
        public string GetTemplatCatatanTerintegrasiRJ(string id, string nama)
        {
            CatatanEdukasiTerintegrasiRJViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.CatatanEdukasiTerintegrasiRJ.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<CatatanEdukasiTerintegrasiRJViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetCPPTTemplet(string id, string nama)
        {
            AssesmentIlmuKulitDanKelaminViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitKulitDanKelamin.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuKulitDanKelaminViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliDalam(string id, string nama)
        {
            AssesmentIlmunPenyakitDalamViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.IlmuPenyakitDalam.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmunPenyakitDalamViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPoliJantung(string id, string nama)
        {
            AssesmentIlmuPenyakitJantungViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama).FirstOrDefault();
                    var t = s.IlmuPenyakitJantung.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentIlmuPenyakitJantungViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatCatatanFisio(string id, string nama)
        {
            CatatanFisiotherapiViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.CatatanFisiotherapi.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<CatatanFisiotherapiViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatLembarFisio(string id, string nama)
        {
            LembarFormulirPoliFisiotherapiViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.LembarFormulirPoliFisiotherapi.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<LembarFormulirPoliFisiotherapiViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatKesimpulanHasil(string id, string nama)
        {
            KesimpulanHasilPemeriksaanKesehatanViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.KesimpulanHasilPemeriksaanKesehatan.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<KesimpulanHasilPemeriksaanKesehatanViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatLaporanPemeriksaan(string id, string nama)
        {
            LaporanPemeriksaanKesehatanMCUViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.LaporanPemeriksaanKesehatan.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<LaporanPemeriksaanKesehatanMCUViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatPengkajianKprwtn(string id, string nama)
        {
            PengkajianKeperawatanViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.PengkajianKeperawatan.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<PengkajianKeperawatanViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatAssAwalMedis(string id, string nama)
        {
            AssesmentAwalMedisNewViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.AsesmenAwalMedis.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentAwalMedisNewViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetTemplatAssAwalKeperawatan(string id, string nama)
        {
            AssesmentAwalPerawatNewViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == nama && xx.SectionID == sectionid).FirstOrDefault();
                    var t = s.AsesmenAwalKeperawatan.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<AssesmentAwalPerawatNewViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListTamplet(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter, string dokumenid, string dokter)
        {
            try
            {
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    IQueryable<trTemplate> proses = s.trTemplate.Where(x => x.SectionID == sectionid && x.DokumenID == dokumenid);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(trTemplate.NamaTemplate)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<trTempletViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}