﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EMRRSBK.Controllers
{
    public class RencanaPemulanganPasienController : Controller
    {
        // GET: RencanaPemulanganPasien
        public ActionResult Index(string noreg, string sectionid)
        {
            var model = new RegistrasiPasienViewModel();

            try
            {
                using (var eSIM = new SIM_Entities())
                {
                    var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                    model.NRM = m.NRM;
                    sectionid = Request.Cookies["SectionIDPelayanan"].Value;

                    using (var eEMR = new EMR_Entities())
                    {
                        var item = eEMR.RencanaPemulanganPasien.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == sectionid);
                        if (item != null)
                        {
                            model.RcnaPmlngn = IConverter.Cast<RencanaPemulanganPasienViewModel>(item);

                            var perawat = eSIM.mDokter.FirstOrDefault(x => x.DokterID == item.KodePerawat);
                            if (perawat != null)
                            {
                                model.RcnaPmlngn.KodePerawatNama = perawat.NamaDOkter;
                            }
                            model.RcnaPmlngn.NoReg = noreg;
                            model.RcnaPmlngn.NRM = model.NRM;
                            model.RcnaPmlngn.SectionID = sectionid;
                            model.RcnaPmlngn.Report = 1;
                        }
                        else
                        {
                            item = new RencanaPemulanganPasien();
                            model.RcnaPmlngn = IConverter.Cast<RencanaPemulanganPasienViewModel>(item);
                            model.RcnaPmlngn.NoReg = noreg;
                            model.RcnaPmlngn.NRM = model.NRM;
                            model.RcnaPmlngn.SectionID = sectionid;
                            model.RcnaPmlngn.Tanggal = DateTime.Today;
                            model.RcnaPmlngn.Jam = DateTime.Now;
                            model.RcnaPmlngn.TanggalMRS = DateTime.Today;
                            model.RcnaPmlngn.JamMRS = DateTime.Now;
                            model.RcnaPmlngn.TanggalPerencanaan = DateTime.Today;
                            model.RcnaPmlngn.JamPerencanaan = DateTime.Now;
                            model.RcnaPmlngn.EstimasiTanggalPulang = DateTime.Today;
                        }

                        #region ===Detail
                        var ListObat = eEMR.RencanaPemulanganPasien_Detail.Where(x => x.SectionID == sectionid && x.NoReg == noreg).ToList();
                        if (ListObat != null)
                        {

                            model.RcnaPmlngn.RcnaPmlngn_List = new ListDetail<RencanaPemulanganPasienModelDetail>();

                            foreach (var x in ListObat)
                            {
                                var y = IConverter.Cast<RencanaPemulanganPasienModelDetail>(x);
                                var dokter = eSIM.mDokter.FirstOrDefault(z => z.DokterID == y.KodePetugas);
                                y.KodePetugasNama = dokter == null ? "" : dokter.NamaDOkter;
                                model.RcnaPmlngn.RcnaPmlngn_List.Add(false, y);
                            }

                        }
                        #endregion
                    }

                }
            }
            catch (SqlException ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            catch (Exception ex) { ViewBag.Status = "danger"; ViewBag.Message = ex.Message; }
            if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["Status"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string RencanaPemulanganPasien_Post(string noreg, string sectionid)
        {
            try
            {
                var item = new RegistrasiPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {

                    ResultSS result;
                    using (var eEMR = new EMR_Entities())
                    {
                        using (var eSIM = new SIM_Entities())
                        {
                            var m = eSIM.VW_DataPasienGet.FirstOrDefault(x => x.NoReg == noreg);
                            item.RcnaPmlngn.NoReg = item.RcnaPmlngn.NoReg;
                            sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                            item.RcnaPmlngn.SectionID = sectionid;
                            item.RcnaPmlngn.NRM = item.RcnaPmlngn.NRM;
                            var model = eEMR.RencanaPemulanganPasien.FirstOrDefault(x => x.SectionID == sectionid && x.NoReg == item.RcnaPmlngn.NoReg);

                            if (model == null)
                            {
                                model = new RencanaPemulanganPasien();
                                var o = IConverter.Cast<RencanaPemulanganPasien>(item.RcnaPmlngn);
                                eEMR.RencanaPemulanganPasien.Add(o);
                            }
                            else
                            {
                                model = IConverter.Cast<RencanaPemulanganPasien>(item.RcnaPmlngn);
                                eEMR.RencanaPemulanganPasien.AddOrUpdate(model);
                            }

                            #region ===== Detail Obat

                            if (item.RcnaPmlngn.RcnaPmlngn_List == null) item.RcnaPmlngn.RcnaPmlngn_List = new ListDetail<RencanaPemulanganPasienModelDetail>();
                            item.RcnaPmlngn.RcnaPmlngn_List.RemoveAll(x => x.Remove);
                            foreach (var x in item.RcnaPmlngn.RcnaPmlngn_List)
                            {
                                x.Model.SectionID = sectionid;
                                x.Model.NoReg = item.RcnaPmlngn.NoReg;
                                x.Model.NRM = item.RcnaPmlngn.NRM;
                                //x.Model.Tanggal = DateTime.Today;
                                //x.Model.Jam = DateTime.Now;

                            }
                            var new_ListObat = item.RcnaPmlngn.RcnaPmlngn_List;
                            var real_ListObat = eEMR.RencanaPemulanganPasien_Detail.Where(x => x.SectionID == item.RcnaPmlngn.SectionID && x.NoReg == item.RcnaPmlngn.NoReg).ToList();
                            foreach (var x in real_ListObat)
                            {
                                var z = new_ListObat.FirstOrDefault(y => y.Model.No == x.No);
                                if (z == null)
                                {
                                    eEMR.RencanaPemulanganPasien_Detail.Remove(x);
                                }
                            }
                            foreach (var x in new_ListObat)
                            {
                                var _m = real_ListObat.FirstOrDefault(y => y.No == x.Model.No);

                                if (_m == null)
                                {
                                    _m = new RencanaPemulanganPasien_Detail();
                                    eEMR.RencanaPemulanganPasien_Detail.Add(IConverter.Cast<RencanaPemulanganPasien_Detail>(x.Model));
                                }
                                else
                                {
                                    _m = IConverter.Cast<RencanaPemulanganPasien_Detail>(x.Model);
                                    eEMR.RencanaPemulanganPasien_Detail.AddOrUpdate(_m);
                                }

                            }

                            #endregion
                            
                            result = new ResultSS(eEMR.SaveChanges());
                            return JsonHelper.JsonMsgCreate(result);
                        }

                    }
                }
                if (!ModelState.IsValid)
                {

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                        }
                    }

                }
                return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDFRencanaPemulanganPasien(string noreg, string sectionid)
        {
            var rd = new ReportDocument();
            var tipepelayanan = Request.Cookies["TipePelayanan"].Value;
            rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), $"Rpt_RencanaPemulanganPasien.rpt"));
            var service = new SqlCon_EMR();
            using (var conn = new SqlConnection(service.ConString))
            {
                var cmd = new List<SqlCommand>();
                var da = new List<SqlDataAdapter>();
                var ds = new List<DataSet>();

                var i = 0;
                cmd.Add(new SqlCommand("Rpt_RencanaPemulanganPasien", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables["Rpt_RencanaPemulanganPasien;1"].SetDataSource(ds[i].Tables[0]);

                i++;
                cmd.Add(new SqlCommand($"Rpt_RencanaPemulanganPasien_Detail", conn));
                cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                cmd[i].Parameters.Add(new SqlParameter("@SectionID", sectionid));
                cmd[i].CommandType = CommandType.StoredProcedure;
                da.Add(new SqlDataAdapter(cmd[i]));
                ds.Add(new DataSet());
                da[i].Fill(ds[i]);
                rd.Database.Tables[$"Rpt_RencanaPemulanganPasien_Detail;1"].SetDataSource(ds[i].Tables[0]);
            };
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            rd.Close();
            return File(stream, "application/pdf");
        }
    }
}