﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;

namespace EMRRSBK.Controllers
{
    public class VerifAssesmentPengkajianController : Controller
    {
        // GET: VerifAssesmentPengkajian

        #region === ASSEMENT PENGKAJIAN
        
        [HttpGet]
        public string CheckBerhasilAssPengkajian(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.AsesmenPengkajianTerintegrasiGawatDarurat.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTangan != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                        if (m.TandaTanganPetugas != null)
                        {
                            result.Petugas = "1";
                        }
                        else
                        {
                            result.Petugas = "0";
                        }
                        if (m.TandaTanganDokterPengkaji != null)
                        {
                            result.DokterPengkaji = "1";
                        }
                        else
                        {
                            result.DokterPengkaji = "0";
                        }
                    }
                }
                else
                {
                    result.Pasien = "0";
                    result.Petugas = "0";
                    result.DokterPengkaji = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        [HttpGet]
        public string get_keyassdiag(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                fdata = f.finger_data;
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifAssesmentPengkajian/checkverifassdiag";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keyassdiagdktpengkaji(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifAssesmentPengkajian/checkverifassdiagdktpengkaji";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifassdiag(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.AsesmenPengkajianTerintegrasiGawatDarurat.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTanganPetugas = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifassdiagdktpengkaji(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.AsesmenPengkajianTerintegrasiGawatDarurat.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTanganDokterPengkaji = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion

        #region === ASSEMENT PENGKAJIAN SKINING
        
        [HttpGet]
        public string CheckBerhasilSkining(string noreg, string section, string id)
        {
            var result = new ReturnFingerViewModel();
            using (var s = new EMR_Entities())
            {
                var finger = s.mFinger.Where(x => x.userid == id).FirstOrDefault();
                if (finger != null)
                {
                    var m = s.AsesmenPengkajianMedisSkrining.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                    if (m != null)
                    {
                        if (m.TandaTangan != null)
                        {
                            result.Pasien = "1";
                        }
                        else
                        {
                            result.Pasien = "0";
                        }
                        if (m.TandaTanganDokter != null)
                        {
                            result.Dokter = "1";
                        }
                        else
                        {
                            result.Dokter = "0";
                        }
                        if (m.TandaTanganPerawat != null)
                        {
                            result.Perawat = "1";
                        }
                        else
                        {
                            result.Perawat = "0";
                        }
                        if (m.TandaTanganBidan != null)
                        {
                            result.Bidan = "1";
                        }
                        else
                        {
                            result.Bidan = "0";
                        }
                    }
                }
                else
                {
                    result.Pasien = "0";
                    result.Dokter = "0";
                    result.Perawat = "0";
                    result.Bidan = "0";
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result
            });
        }

        [HttpGet]
        public string get_keyassdiagakining(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifAssesmentPengkajian/checkverifassdiagskinging";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keyassdiagskiningperawat(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifAssesmentPengkajian/checkverifassdiagskiningperawat";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        [HttpGet]
        public string get_keyassdiagskiningbidan(string userid, string noreg, string section)
        {
            var ac = "";
            var sn = "";
            var fdata = "";
            var time = ConfigurationManager.AppSettings["timeoutShow"];
            using (var s = new EMR_Entities())
            {
                var m = s.mDevice.FirstOrDefault();
                var f = s.mFinger.FirstOrDefault(x => x.userid == userid);
                ac = m.ac;
                sn = m.sn;
                //if (f == null)
                //{
                //    fdata = "";
                //}
                //else
                //{
                fdata = f.finger_data;
                //}
            }
            var urlpost = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifAssesmentPengkajian/checkverifassdiagskiningbidan";
            return $"{userid};{fdata};SecurityKey;{time};{urlpost};{ac + sn};{noreg + "/" + section};";
        }

        public string checkverifassdiagskinging(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.AsesmenPengkajianMedisSkrining.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTanganDokter = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifassdiagskiningperawat(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.AsesmenPengkajianMedisSkrining.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTanganPerawat = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }

        public string checkverifassdiagskiningbidan(string VerPas)
        {
            var data = VerPas.Split(';');
            var user_id = data[0];
            var vStamp = data[1];
            var time = data[2];
            var sn = data[3];
            var param = data[4].Split('/');
            var noreg = param[0];
            var section = param[1];
            //var url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/VerifikasiFinger/sent";

            using (var s = new EMR_Entities())
            {
                using (var eSIM = new SIM_Entities())
                {
                    if (section == "SEC002")
                    {
                        var dokter = eSIM.mDokter.FirstOrDefault(x => x.DokterID == user_id);
                        if (dokter != null)
                        {
                            user_id = dokter.DokterID;
                        }

                        var m = s.mDevice.FirstOrDefault(x => x.sn == sn);
                        var f = s.mFinger.Where(x => x.userid == user_id);
                        foreach (var i in f.ToList())
                        {
                            var salt = StaticModel.CreateMD5(sn + i.finger_data + m.vc + time + user_id + m.vkey);

                            if (salt.ToUpper() == vStamp.ToUpper())
                            {
                                #region === Verif Persetujuan Umum 

                                var dataspersetujuanumum = s.AsesmenPengkajianMedisSkrining.FirstOrDefault(x => x.NoReg == noreg && x.SectionID == section);
                                if (dataspersetujuanumum != null)
                                {
                                    if (dokter != null)
                                    {
                                        dataspersetujuanumum.TandaTanganBidan = i.Gambar1;
                                    }
                                    else
                                    {
                                        dataspersetujuanumum.TandaTangan = i.Gambar1;
                                    }
                                    s.SaveChanges();
                                }
                                #endregion
                                return "empty";
                            };
                        }
                    }

                }

            }
            return "Gagal";
        }
        #endregion
    }
}