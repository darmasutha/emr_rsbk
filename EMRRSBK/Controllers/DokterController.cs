﻿using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class DokterController : Controller
    {
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mDokter> proses = s.mDokter.Where(x => x.DokterIdEMR == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        //[HttpPost]
        //public string ListUmum(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<mDokter> proses = s.mDokter.Where(x => x.DokterIdEMR == true && x.KodeKategoriVendor == "V-002" && x.DokterID != "XX");
        //            if (!string.IsNullOrEmpty(filter[0]))
        //                proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
        //            if (!string.IsNullOrEmpty(filter[1]))
        //                proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
        //        }
        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        //[HttpPost]
        //public string ListSpesialis(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<mDokter> proses = s.mDokter.Where(x => x.DokterIdEMR == true && x.KodeKategoriVendor == "V-009" && x.DokterID != "XX");
        //            if (!string.IsNullOrEmpty(filter[0]))
        //                proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
        //            if (!string.IsNullOrEmpty(filter[1]))
        //                proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
        //        }
        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        //[HttpPost]
        //public string ListRuangan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{

        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.TipePelayanan == "RI" && x.StatusAktif == true);
        //            proses = proses.Where($"SectionID.Contains(@0)", filter[0]);
        //            proses = proses.Where($"SectionName.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => new StandardViewModel()
        //            {
        //                Kode = x.SectionID,
        //                Nama = x.SectionName
        //            });
        //            result.Data = m;
        //        }

        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }


        //}

        //[HttpPost]
        //public string ListRuanganPemindahan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{

        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => (x.TipePelayanan == "RI" || x.TipePelayanan == "RJ" || x.TipePelayanan == "Penunjang2") && x.StatusAktif == true);
        //            proses = proses.Where($"SectionID.Contains(@0)", filter[0]);
        //            proses = proses.Where($"SectionName.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => new StandardViewModel()
        //            {
        //                Kode = x.SectionID,
        //                Nama = x.SectionName
        //            });
        //            result.Data = m;
        //        }

        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }


        //}

        //[HttpPost]
        //public string ListNoKamar(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter, string ruangan)
        //{

        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<mKamar> proses = s.mKamar.Where(x => x.SalID == ruangan);
        //            proses = proses.Where($"SalID.Contains(@0)", filter[0]);
        //            proses = proses.Where($"NoKamar.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => new StandardViewModel()
        //            {
        //                Kode = x.NoKamar,
        //                Nama = x.NoKamar
        //            });
        //            result.Data = m;
        //        }

        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }


        //}

        //[HttpPost]
        //public string ListNoKamardanNama(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter, string ruangan)
        //{

        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<mKamar> proses = s.mKamar.Where(x => x.SalID == ruangan);
        //            proses = proses.Where($"SalID.Contains(@0)", filter[0]);
        //            proses = proses.Where($"NoKamar.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => new StandardViewModel()
        //            {
        //                Kode = x.NoKamar,
        //                Nama = x.NoKamar
        //            });
        //            result.Data = m;
        //        }

        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }


        //}

        //[HttpPost]
        //public string ListPoli(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.TipePelayanan == "RJ" && (x.PoliKlinik == "SPESIALIS" || x.PoliKlinik == "UMUM"));
        //            proses = proses.Where($"SectionID.Contains(@0)", filter[0]);
        //            proses = proses.Where($"SectionName.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => new StandardViewModel()
        //            {
        //                Kode = x.SectionID,
        //                Nama = x.SectionName
        //            });
        //            result.Data = m;
        //        }

        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        ////[HttpPost]
        ////public string ListPerawat(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        ////{
        ////    try
        ////    {
        ////        ResultSS result;
        ////        using (var s = new SIM_Entities())
        ////        {
        ////            IQueryable<Vw_Perawat> proses = s.Vw_Perawat.Where(x => /*x.NamaProfesi != null &&*/ x.DokterIdEMR == true);
        ////            if (!string.IsNullOrEmpty(filter[0]))
        ////                proses = proses.Where($"{nameof(Vw_Perawat.PerawatID)}.Contains(@0)", filter[0]);
        ////            if (!string.IsNullOrEmpty(filter[1]))
        ////                proses = proses.Where($"{nameof(Vw_Perawat.NamaPerawat)}.Contains(@0)", filter[1]);
        ////            var totalcount = proses.Count();
        ////            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        ////                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        ////            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        ////            result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PerawatViewModel>(x));
        ////        }
        ////        return JsonConvert.SerializeObject(new TableList(result));
        ////    }
        ////    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        ////    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        ////}

        //[HttpPost]
        //public string ListTerapi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<mBarang> proses = s.mBarang.Where(x => x.Aktif == true);
        //            if (!string.IsNullOrEmpty(filter[0]))
        //                proses = proses.Where($"{nameof(mBarang.Kode_Barang)}.Contains(@0)", filter[0]);
        //            if (!string.IsNullOrEmpty(filter[1]))
        //                proses = proses.Where($"{nameof(mBarang.Nama_Barang)}.Contains(@0)", filter[1]);
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            result.Data = models.ToList().ConvertAll(x => IConverter.Cast<BarangViewModel>(x));
        //        }
        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        //[HttpPost]
        public string ListObat(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mBarang> proses = s.mBarang.Where(x => x.Aktif == true && x.Kelompok == "OBAT");
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mBarang.Kode_Barang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mBarang.Nama_Barang)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<BarangViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        
        [HttpPost]
        public string ListSupplier(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier.Where(x => x.DokterIdEMR == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mSupplier.Kode_Supplier)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mSupplier.Nama_Supplier)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListPasien(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_DataPasienReg> proses = s.VW_DataPasienReg.Where(x => x.Tanggal == DateTime.Today);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(VW_DataPasienReg.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(VW_DataPasienReg.NamaPasien)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiPasienViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        
    }
}