﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMRRSBK.Entities;
using EMRRSBK.Models;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace EMRRSBK.Controllers
{
    public class RenpraRJController : Controller
    {
        // GET: RenpraRJ
        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet(string noreg, string nrm, string copy = "")
        {
            var model = new Renpra_RJViewModel();
            using (var s = new EMR_Entities())
            {
                if (copy != "")
                {
                    var dokumen = s.Renpra_RJ.FirstOrDefault(x => x.NRM == nrm && x.NoReg == noreg);
                    model = IConverter.Cast<Renpra_RJViewModel>(dokumen);
                    model._METHOD = "CREATE";
                    model.NoReg = noreg;
                    model.NRM = nrm;
                }
                else
                {
                    model._METHOD = "CREATE";
                    model.NoReg = noreg;
                    model.NRM = nrm;
                    model.Tanggal = DateTime.Today;
                    model.Jam = DateTime.Now;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpGet]
        [ActionName("Update")]
        public ActionResult Update(string id = "")
        {
            var model = new Renpra_RJViewModel();
            using (var sim = new SIM_Entities())
            {
                using (var s = new EMR_Entities())
                {
                    var dokumen = s.Renpra_RJ.FirstOrDefault(x => x.NRM == id);
                    if (dokumen != null)
                    {
                        model = IConverter.Cast<Renpra_RJViewModel>(dokumen);
                        model._METHOD = "UPDATE";
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Petugas);
                        if (dokter != null)
                        {
                            model.PetugasNama = dokter.NamaDOkter;
                        }
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string CreatePost()
        {
            try
            {
                var item = new Renpra_RJViewModel();
                TryUpdateModel(item);
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var model = s.Renpra_RJ.FirstOrDefault(x => x.NRM == item.NRM);

                    var activity = "";
                    if (item._METHOD == "CREATE")
                    {
                        var o = IConverter.Cast<Renpra_RJ>(item);
                        o.NRM = item.NRM;
                        o.SectionID = item.SectionID;
                        o.NoReg = item.NoReg;
                        s.Renpra_RJ.Add(o);

                        activity = "Create Assesmen Renpra_RJ";

                        if (item.saveTemplate == true)
                        {
                            var temp = new trTemplate();
                            temp.NoReg = item.NoReg;
                            temp.NamaTemplate = item.templateName;
                            temp.DokterID = item.Petugas;
                            temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            temp.DokumenID = "Renpra_RJ";
                            s.trTemplate.Add(temp);
                        }
                    }
                    else
                    {
                        model = IConverter.Cast<Renpra_RJ>(item);
                        s.Renpra_RJ.AddOrUpdate(model);

                        activity = "Update Assesmen Renpra_RJ";

                        if (item.saveTemplate == true)
                        {
                            var check_temp = s.trTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                            if (check_temp == null)
                            {
                                var temp = new trTemplate();
                                temp.NoReg = item.NoReg;
                                temp.NamaTemplate = item.templateName;
                                temp.DokterID = item.Petugas;
                                temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                temp.DokumenID = "Renpra_RJ";
                                s.trTemplate.Add(temp);
                            }
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost, ValidateInput(false)]
        [ActionName("SecondCreate")]
        public string SecondCreate(Renpra_RJViewModel item, string img)
        {
            try
            {
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    var model = s.Renpra_RJ.FirstOrDefault(x => x.NRM == item.NRM && x.NoReg == item.NoReg);
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var sectionname = Request.Cookies["SectionNamaPelayanan"].Value;
                    var activity = "";
                    if (item._METHOD == "CREATE")
                    {
                        var o = IConverter.Cast<Renpra_RJ>(item);
                        o.NRM = item.NRM;
                        o.SectionID = sectionid;
                        o.NoReg = item.NoReg;
                        s.Renpra_RJ.Add(o);

                        activity = "Create Assesmen Renpra RJ";

                        if (img == "true")
                        {
                            var temp = new trTemplate();
                            temp.NoReg = item.NoReg;
                            temp.NamaTemplate = item.templateName;
                            temp.DokterID = item.Petugas;
                            temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                            temp.DokumenID = "Renpra_RJ";
                            s.trTemplate.Add(temp);
                        }
                    }
                    else if (item._METHOD == "UPDATE")
                    {
                        model = IConverter.Cast<Renpra_RJ>(item);
                        model.SectionID = sectionid;
                        s.Renpra_RJ.AddOrUpdate(model);

                        activity = "Update Assesmen Renpra RJ";

                        if (img == "true")
                        {
                            var check_temp = s.trTemplate.FirstOrDefault(x => x.NamaTemplate == item.templateName);
                            if (check_temp == null)
                            {
                                var temp = new trTemplate();
                                temp.NoReg = item.NoReg;
                                temp.NamaTemplate = item.templateName;
                                temp.DokterID = item.Petugas;
                                temp.SectionID = Request.Cookies["SectionIDPelayanan"].Value;
                                temp.DokumenID = "Renpra_RJ";
                                s.trTemplate.Add(temp);
                            }
                        }
                    }

                    s.SaveChanges();

                    result = new ResultSS(1);

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $" {activity} {item.NoReg}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgCreate(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string get_profesi(string dokter)
        {
            try
            {
                using (var s = new SIM_Entities())
                {
                    var dt = s.mDokter.FirstOrDefault(x => x.DokterID == dokter);
                    if (dt != null)
                    {
                        //var prf = s.SIMmProfesi.FirstOrDefault(x => x.KodeProfesi == dt.KodeProfesi);
                        //if (prf != null)
                        //{
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = dt.NamaDOkter,
                            Message = ""

                        });
                        //}
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = false,
                    Data = "",
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string copyRenpra_RJ(int no, string id, string noreg)
        {
            try
            {
                var model = new Renpra_RJViewModel();
                using (var s = new EMR_Entities())
                {
                    var dokumen = s.Renpra_RJ.FirstOrDefault(x => x.NRM == id && x.NoReg == noreg && x.No == no);
                    model = IConverter.Cast<Renpra_RJViewModel>(dokumen);
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string editRenpra_RJ(int no, string id, string noreg)
        {
            try
            {
                var model = new Renpra_RJViewModel();
                using (var sim = new SIM_Entities())
                {
                    using (var s = new EMR_Entities())
                    {
                        var dokumen = s.Renpra_RJ.FirstOrDefault(x => x.NRM == id && x.NoReg == noreg && x.No == no);
                        model = IConverter.Cast<Renpra_RJViewModel>(dokumen);
                        model.Tanggal_View = model.Tanggal.Value.ToString("yyyy-MM-dd");
                        model.Jam_View = model.Jam.Value.ToString("HH:mm");
                        var dokter = sim.mDokter.FirstOrDefault(x => x.DokterID == model.Petugas);
                        if (dokter != null)
                        {
                            model.PetugasNama = dokter.NamaDOkter;
                        }
                    }
                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = model,
                    Message = ""

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string getListObat(string noreg)
        {
            try
            {
                var section_id = Request.Cookies["EMRSectionIDPelayanan"].Value;
                var list_obat = "";
                using (var s = new EMR_Entities())
                {
                    //var m_list = s.SIMtrResep.Where(x => x.NoRegistrasi == noreg && x.SectionID == section_id).ToList();

                    //foreach (var y in m_list)
                    //{
                    //    var x_list = s.SIMtrResepDetail.Where(xx => xx.NoResep == y.NoResep).ToList();

                    //    foreach (var x in x_list)
                    //    {
                    //        var b = s.mBarang.Where(ss => ss.Barang_ID == x.Barang_ID).FirstOrDefault();

                    //        list_obat += b.Nama_Barang + " \t ( " + x.Qty + " ) \t" + x.Dosis + " \n";
                    //    }
                    //}

                }

                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = list_obat,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public ActionResult ExportPDF(string nrm)
        {
            try
            {
                string ServerPath = "~/CrystalReports/";
                string reportname = $"Rpt_Renpra_RJ";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), $"{reportname}.rpt"));
                var service = new SqlCon_EMR();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand(reportname, conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NRM", nrm));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"{reportname};1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        [HttpPost]
        public string ListTamplet(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter, string dokumenid)
        {
            try
            {
                var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                ResultSS result;
                using (var s = new EMR_Entities())
                {
                    IQueryable<trTemplate> proses = s.trTemplate.Where(x => x.SectionID == sectionid && x.DokumenID == dokumenid);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(trTemplate.NamaTemplate)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<trTempletViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string LoadingTampletRenpra_RJ(string namatemp, string dokumenid)
        {
            Renpra_RJViewModel item;
            try
            {
                using (var s = new EMR_Entities())
                {
                    var sectionid = Request.Cookies["SectionIDPelayanan"].Value;
                    var tamplet = s.trTemplate.Where(xx => xx.NamaTemplate == namatemp).FirstOrDefault();
                    var t = s.Renpra_RJ.Where(x => x.NoReg == tamplet.NoReg && x.SectionID == sectionid).FirstOrDefault();
                    item = IConverter.Cast<Renpra_RJViewModel>(t);
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = item,
                    Message = "Success.."

                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}