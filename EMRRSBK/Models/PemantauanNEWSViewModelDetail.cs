﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PemantauanNEWSViewModelDetail
    {
        public string NoReg { get; set; } 
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Respirasi { get; set; }
        public string Merintih { get; set; }
        public string Warna { get; set; }
        public string Nadi { get; set; }
        public string Neuro { get; set; }
        public string Tenperatur { get; set; }
        public string Skor { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string RentangSkor { get; set; }
        public Nullable<int> NomorReport { get; set; }
        public Nullable<int> NomorReport_Group { get; set; }
        public string Username { get; set; }
        public int SudahRegDkt { get; set; }
        public string TandaTanganDokter { get; set; }
        public string ViewBag { get; set; }
    }
}