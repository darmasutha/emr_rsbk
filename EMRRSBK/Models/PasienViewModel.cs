﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PasienViewModel
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public DateTime TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string TanggalLahir { get; set; }
    }
}