﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ImplementasiViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string TindakanKeperawatan { get; set; }
        public string Evaluasi { get; set; }
        public string NamaParaf { get; set; }
        public string NamaParafNnama { get; set; }
        public string TTDDokter { get; set; }
        public int SudahRegDkt { get; set; }
        public int Viewbag { get; set; }
    }
}