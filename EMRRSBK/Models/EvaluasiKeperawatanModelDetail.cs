﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EvaluasiKeperawatanModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalEvaluasi { get; set; }
        public string NoDx { get; set; }
        public string TtPpjp { get; set; }
        public string NamaPpjp { get; set; }
        public string NamaPpjpNama { get; set; }
        public string Evaluasi_S { get; set; }
        public string Evaluasi_O { get; set; }
        public string Evaluasi_A { get; set; }
        public string Evaluasi_P { get; set; }
        public string P_Ket { get; set; }
        public string Username { get; set; }
    }
}
