﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class IdentitasPasienViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string PasienKeluarga { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTangan { get; set; }



        public string NRM { get; set; }
        public string Nama { get; set; }
        public string Tempat { get; set; }
        public string TanggalLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string Agama { get; set; }
        public string Alamat { get; set; }
        public string Kabupaten { get; set; }
        public string Provensi { get; set; }
        public string NoTelp { get; set; }
        public string Kewarganegaraan { get; set; }
        public string Pendidikan { get; set; }
        public string Pekerjaan { get; set; }
        public string PangkatKesatuan { get; set; }
        public string StatusKawin { get; set; }
        public string CaraBayar { get; set; }
        public string CaraBayarNomor { get; set; }
        public string HubunganpenanggungJawab { get; set; }
        public string NamaPenganggungJawab { get; set; }
        public string TempatPenganggungJawab { get; set; }
        public string TanggalLahirPenganggungJawab { get; set; }
        public string AlamatLahirPenganggungJawab { get; set; }
        public string KabupatenLahirPenganggungJawab { get; set; }
        public string ProvensiLahirPenganggungJawab { get; set; }
        public string NoTelpLahirPenganggungJawab { get; set; }
    }
}