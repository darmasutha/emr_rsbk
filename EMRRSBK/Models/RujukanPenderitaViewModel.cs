﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RujukanPenderitaViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Yth { get; set; }
        public string YthNama { get; set; }
        public string Di { get; set; }
        public string Pekerjaan { get; set; }
        public string Diagnosa_Kerja { get; set; }
        public string Riwayat_Pekerjaan { get; set; }
        public string Perawatan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPengirim { get; set; }
        public string NamaPengirim { get; set; }
        public string Username { get; set; }
        public string NamaPenderita { get; set; }
        public string UmurPenderita { get; set; }
        public string JKPenderita { get; set; }
        public string AlamatPenderita { get; set; }
        public string PekerjaanPenderita { get; set; }
    }
}