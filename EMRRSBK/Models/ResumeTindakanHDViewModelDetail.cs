﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ResumeTindakanHDViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl { get; set; }
        public string BBKering { get; set; }
        public string BBPra { get; set; }
        public string BBPost { get; set; }
        public string TensiPra { get; set; }
        public string TensiPost { get; set; }
        public string Nadi { get; set; }
        public string Lama { get; set; }
        public string Heparinisasi { get; set; }
        public string RataQB { get; set; }
        public string UFGoal { get; set; }
        public string UFRemoved { get; set; }
        public string Transfusi { get; set; }
        public string ObatIntraHD { get; set; }
        public string ObatIntraHDNama { get; set; }
        public string MRS_RJ { get; set; }
        public string Tindakan { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Username { get; set; }
    }
}