﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SIMtrRiwayatAlergiViewModelDetail
    {
        public int No { get; set; }
        public string NRM { get; set; }
        public string DeskripsiAlergi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglInput { get; set; }
        public int BarangID { get; set; }
        public string BarangNama { get; set; }
        public string KodeBarang { get; set; }


    }
}