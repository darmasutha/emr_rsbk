﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratPersetujuanPemeriksaanPenyidikanViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string Nama { get; set; }
        public string JK { get; set; }
        public string Umur { get; set; }
        public string Pekerjaan { get; set; }
        public string Alamat { get; set; }
        public string HubunganKeluarga { get; set; }
        public string Saksi1 { get; set; }
        public string Saksi2 { get; set; }
        public string MemberiPernyataan { get; set; }
        public string MemberiPernyataanNama { get; set; }
        public string NamaBertandaTangan { get; set; }
        public string JKBertandaTangan { get; set; }
        public string UmurBertandaTangan { get; set; }
        public string AlamatBertandaTangan { get; set; }
        public string PekerjaanBertandaTangan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        public bool MendapatPenjelasan1 { get; set; }
        public bool MendapatPenjelasan2 { get; set; }
        public bool MendapatPenjelasan3 { get; set; }
        public bool MendapatPenjelasan4 { get; set; }
        public int SudahRegPetugas { get; set; }
        public string TandaTanganPetugas { get; set; }
    }
}