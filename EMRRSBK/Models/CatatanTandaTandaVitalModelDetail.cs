﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanTandaTandaVitalModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<double> Nadi { get; set; }
        public Nullable<double> Suhu { get; set; }
        public string Username { get; set; }
        public string TekananDarah { get; set; }
        public string Pernafasan { get; set; }
        public string SkalaNyeri { get; set; }
        public string SkorJatuh { get; set; }
    }
}
