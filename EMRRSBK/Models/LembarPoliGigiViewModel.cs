﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LembarPoliGigiViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalUpdateTerakhir { get; set; }
        public string GolonganDarah { get; set; }
        public string TekananDarah { get; set; }
        public string TekananDarah_Ket { get; set; }
        public string PenyakitJantung { get; set; }
        public string PenyakitJantung_Ket { get; set; }
        public string Diabetes { get; set; }
        public string Diabetes_Ket { get; set; }
        public string Hemofili { get; set; }
        public string Hemofili_Ket { get; set; }
        public string Hepatitis { get; set; }
        public string Hepatitis_Ket { get; set; }
        public string PenyakitLainnya { get; set; }
        public string PenyakitLainnya_Ket { get; set; }
        public string Alergi { get; set; }
        public string Alergi_Ket { get; set; }
        public string Occlussi { get; set; }
        public string TorusPalatinus { get; set; }
        public string TorusMandibula { get; set; }
        public string Palatum { get; set; }
        public string Supernumerary { get; set; }
        public string Supernumerary_Ket { get; set; }
        public string Diastema { get; set; }
        public string Diastema_Ket { get; set; }
        public string GigiAnomali { get; set; }
        public string GigiAnomali_Ket { get; set; }
        public string LainLain { get; set; }
        public string Ket_11 { get; set; }
        public string Ket_12 { get; set; }
        public string Ket_13 { get; set; }
        public string Ket_14 { get; set; }
        public string Ket_15 { get; set; }
        public string Ket_16 { get; set; }
        public string Ket_17 { get; set; }
        public string Ket_18 { get; set; }
        public string Ket_21 { get; set; }
        public string Ket_22 { get; set; }
        public string Ket_23 { get; set; }
        public string Ket_24 { get; set; }
        public string Ket_25 { get; set; }
        public string Ket_26 { get; set; }
        public string Ket_27 { get; set; }
        public string Ket_28 { get; set; }
        public string Ket_48 { get; set; }
        public string Ket_47 { get; set; }
        public string Ket_46 { get; set; }
        public string Ket_45 { get; set; }
        public string Ket_44 { get; set; }
        public string Ket_43 { get; set; }
        public string Ket_42 { get; set; }
        public string Ket_41 { get; set; }
        public string Ket_38 { get; set; }
        public string Ket_37 { get; set; }
        public string Ket_36 { get; set; }
        public string Ket_35 { get; set; }
        public string Ket_34 { get; set; }
        public string Ket_33 { get; set; }
        public string Ket_32 { get; set; }
        public string Ket_31 { get; set; }
        public string Ket_51 { get; set; }
        public string Ket_52 { get; set; }
        public string Ket_53 { get; set; }
        public string Ket_54 { get; set; }
        public string Ket_55 { get; set; }
        public string Ket_61 { get; set; }
        public string Ket_62 { get; set; }
        public string Ket_63 { get; set; }
        public string Ket_64 { get; set; }
        public string Ket_65 { get; set; }
        public string Ket_85 { get; set; }
        public string Ket_84 { get; set; }
        public string Ket_83 { get; set; }
        public string Ket_82 { get; set; }
        public string Ket_81 { get; set; }
        public string Ket_71 { get; set; }
        public string Ket_72 { get; set; }
        public string Ket_73 { get; set; }
        public string Ket_74 { get; set; }
        public string Ket_75 { get; set; }
        public string Ket_D { get; set; }
        public string Ket_M { get; set; }
        public string Ket_F { get; set; }
        public string DiperiksaOleh { get; set; }
        public string DiperiksaOlehNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPemeriksaan { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string GambarGigi { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTangan { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string Template { get; set; }
        public string DokumenID { get; set; }
    }
}