﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class BeritaMasukPerawatanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }


        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Kepada { get; set; }

        public string Nama { get; set; }
        public int? Umur { get; set; }
        public string JenisKelamin { get; set; }        
        public string Pekrjaan { get; set; }

        public string Alamat { get; set; }
        public string Diberitahukan { get; set; }
        public string TelahMasuk_Hari { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TelahMasuk_Tanggal { get; set; }
        public string KodeDokter { get; set; }
        public string Username { get; set; }

        public int Report { get; set; }

    }
}