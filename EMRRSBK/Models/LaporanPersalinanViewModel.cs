﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LaporanPersalinanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string KeadaanIbu { get; set; }
        public string KeadaanUmum { get; set; }
        public string Nadi { get; set; }
        public string TekananDarah { get; set; }
        public string Suhu { get; set; }
        public string Hb { get; set; }
        public string Uterus { get; set; }
        public string PendarahanKalaIII { get; set; }
        public string PendarahanKalaIV { get; set; }
        public string BetukUkuran { get; set; }
        public string TaliPusat { get; set; }
        public string KuliKetuban { get; set; }
        public string Anak_JenisKelamin { get; set; }
        public string Anak_BeratBadan { get; set; }
        public string Anak_LingkarDada { get; set; }
        public string Anak_Lahir { get; set; }
        public string Anak_PanjangBadan { get; set; }
        public string Anak_LingkarKepala { get; set; }
        public string KelaiananKongential { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BayiKeadaanJelek { get; set; }
        public string SebabKelahiranMati { get; set; }
        public string Denyut_1mnt { get; set; }
        public string Denyut_5mnt { get; set; }
        public string Denyut_10mnt { get; set; }
        public string Pernafasan_1mnt { get; set; }
        public string Pernafasan_5mnt { get; set; }
        public string Pernafasan_10mnt { get; set; }
        public string Tonus_1mnt { get; set; }
        public string Tonus_5mnt { get; set; }
        public string Tonus_10mnt { get; set; }
        public string Peka_1mnt { get; set; }
        public string Peka_5mnt { get; set; }
        public string Peka_10mnt { get; set; }
        public string Warana_1mnt { get; set; }
        public string Warana_5mnt { get; set; }
        public string Warana_10mnt { get; set; }
        public string Total_1mnt { get; set; }
        public string Total_5mnt { get; set; }
        public string Total_10mnt { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BayiLahirTgl { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> BayiLahirJam { get; set; }
        public string Kehamilan { get; set; }
        public string JenisPartus { get; set; }
        public string HB_Ibu { get; set; }
        public string Leko { get; set; }
        public string Golongan { get; set; }
        public string DitolongOleh { get; set; }
        public string DitolongOlehNama { get; set; }
        public string Ureum { get; set; }
        public string Komplikasi { get; set; }
        public string GulaDarah { get; set; }
        public string BayiLahir { get; set; }
        public string JeniskelaminBayi { get; set; }
        public string BeratBayi { get; set; }
        public string PanjangBayi { get; set; }
        public string DirawatDi { get; set; }
        public string Urin { get; set; }
        public string KeluhanSekarang { get; set; }
        public string Username { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Pangkat { get; set; }
        public string NRP { get; set; }
        public string Kesatuan { get; set; }
        public int? Umur { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
    }
}