﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ObservasiKomprehensifViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public int Nomor { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Nadi { get; set; }
        public string Tensi { get; set; }
        public Nullable<int> Suhu { get; set; }
        public string Resperasi { get; set; }
        public string SkalaNyeri { get; set; }
        public Nullable<int> GCS_E { get; set; }
        public Nullable<int> GCS_V { get; set; }
        public Nullable<int> GCS_M { get; set; }
        public Nullable<int> GCS_Total { get; set; }
        public string RPupil { get; set; }
        public string KaKi { get; set; }
        public string NamaTandaTangan { get; set; }
        public string NamaTandaTanganNama { get; set; }
        public string Username { get; set; }
        public int SudahRegDkt { get; set; }
        public string Viewbag { get; set; }
        public string TTDDokter { get; set; }
    }
}