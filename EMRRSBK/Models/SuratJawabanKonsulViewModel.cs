﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratJawabanKonsulViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string TS_Dr_Konsul { get; set; }
        public string Nama_Pasien_Konsul { get; set; }
        public string TS_Dr_JawabanKonsul { get; set; }
        public string TS_Dr_JawabanKonsulNama { get; set; }
        public string Diagnosa_JawabanKonsul { get; set; }
        public string Tindakan_JawabanKonsul { get; set; }
        public int Report { get; set; }
    }
}