﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RegistrasiViewModel
    {
        public string NoReg { get; set; }
        public short NoAntri { get; set; }
        public int Nomor { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public Nullable<int> CustomerKerjasamaID { get; set; }
        public string CustomerKerjasamaIDNama { get; set; }
        public string NoKartu { get; set; }
        [Required]
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Nama_Supplier { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public System.DateTime TglReg { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string TglReg_View { get; set; }
        public string Tanggal_View { get; set; }
        public string Jam_View { get; set; }
        public System.DateTime JamReg { get; set; }
        public string SectionID { get; set; }
        public string Alamat { get; set; }
        public bool SudahInputEMR { get; set; }
        public string Phone { get; set; }
        [Required]
        public string DokterID { get; set; }
        public string DokterIDNama { get; set; }
        public string DokterRawatID { get; set; }
        public bool PasienBaru { get; set; }
        public bool PasienKTP { get; set; }
        public bool PenanggungIsPasien { get; set; }
        public string PenanggungNRM { get; set; }
        [Required]
        public string PenanggungNama { get; set; }
        [Required]
        public string PenanggungAlamat { get; set; }
        [Required]
        public string PenanggungHubungan { get; set; }
        [Required]
        public string PenanggungTelp { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public bool Batal { get; set; }
        public string StatusBayar { get; set; }
        public string SudahPeriksa { get; set; }
        public string CPPT_OGambar { get; set; }
        public string Template { get; set; }
    }
}