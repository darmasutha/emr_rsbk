﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class HasilBacaPenunjangDetailViewModel
    {
        public string NoBukti { get; set; }
        public short NoUrut { get; set; }
        public string PathDokumen { get; set; }
        public string FileName { get; set; }
    }
}