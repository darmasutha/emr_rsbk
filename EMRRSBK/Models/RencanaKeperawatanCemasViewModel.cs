﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanCemasViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_DiagnosaKeperawatan { get; set; }
        public bool PenyakitKritis { get; set; }
        public bool TakutTerhadapKematian { get; set; }
        public bool ProsedurInvasif { get; set; }
        public bool PemasanganVentilator { get; set; }
        public bool Cbox_KetCemasLainnya { get; set; }
        public string KetCemasLainnya { get; set; }
        public bool Insomnia { get; set; }
        public bool Gelisah { get; set; }
        public bool Ketakutan { get; set; }
        public bool Sedih { get; set; }
        public bool TTV_Abnormal { get; set; }
        public bool NafasTidakSinkron { get; set; }
        public string RR { get; set; }
        public string N { get; set; }
        public string SetelahDilakukanTindakan { get; set; }
        public string KolaborasiLanjutan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string NamaTerang { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}