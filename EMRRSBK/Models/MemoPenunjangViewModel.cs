﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class MemoPenunjangViewModel
    {
        public int? Nomor { get; set; }
        public string SectionID { get; set; }
        public string NoBuktiMemo { get; set; }
        public string NoReg { get; set; }
        public string SectionTujuanID { get; set; }
        public string SectionTujuanNama { get; set; }
        public string DokterID { get; set; }
        public string DokterNama { get; set; }
        public string Memo { get; set; }
        public string Diagnosa { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public bool SudahPeriksa { get; set; }


        //public string NoBuktiMemo { get; set; }
        //public string NoBuktiHeader { get; set; }
        //public string DokterID { get; set; }
        //public string Tanggal_View { get; set; }
        //public string SectionTujuanID { get; set; }
        //public short UserID { get; set; }
        //public bool SudahPeriksa { get; set; }
        //public string NoBillPenunjang { get; set; }
        //public Nullable<System.DateTime> TanggalPeriksa { get; set; }
        //public Nullable<System.DateTime> JamPeriksa { get; set; }
        //public string NoReg { get; set; }
        //public string Kamar { get; set; }
        //public string NoBed { get; set; }
        //public Nullable<int> NoRegPasien { get; set; }
        //public Nullable<int> JenisKerjasamaID { get; set; }
        //public string KelasID { get; set; }
        //public Nullable<int> UmurThn { get; set; }
        //public Nullable<int> UmurBln { get; set; }
        //public Nullable<int> UmurHr { get; set; }
        //public string Diagnosa { get; set; }
        //public string nobuktiperiksa { get; set; }
    }
}