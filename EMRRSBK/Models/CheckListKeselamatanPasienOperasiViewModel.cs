﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CheckListKeselamatanPasienOperasiViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SignIn { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_SignIn { get; set; }
        public string IdentitasPasien { get; set; }
        public string PersetujuanTindakan { get; set; }
        public string AreaOperasi { get; set; }
        public string RiwayatAlergi { get; set; }
        public string PersiapanAlat { get; set; }
        public string DokumenPemeriksaan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_TimeOut { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_TimeOut { get; set; }
        public bool MemastikanBahwa { get; set; }
        public bool MemastikanDanBaca { get; set; }
        public string ApakahProfilaksis { get; set; }
        public bool ApakahTindakanBeresiko { get; set; }
        public string ApakahTindakanBeresiko_Ket { get; set; }
        public bool BerapaLamaTindakan { get; set; }
        public string BerapaLamaTindakan_Ket { get; set; }
        public bool ApakahSudahAntisipasi { get; set; }
        public string ApakahSudahAntisipasi_Ket { get; set; }
        public bool ApakahSudahDipastikan { get; set; }
        public string ApakahSudahDipastikan_Ket { get; set; }
        public bool ApakahDadaMasalah { get; set; }
        public string ApakahDadaMasalah_Ket { get; set; }
        public string ApakahHasilRadiologi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_SignOut { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam_SignOut { get; set; }
        public bool NamaTindakan { get; set; }
        public string NamaTindakanKet { get; set; }
        public bool KelengkapanAlat { get; set; }
        public bool PelabelanSpecimen { get; set; }
        public bool ApakahAdaMasalah { get; set; }
        public bool KomplikasiDariTindakanChck { get; set; }
        public bool RencanaKontrolChck { get; set; }
        public string ApakahAdaMasalah_Ket { get; set; }
        public string RencanaKontrol { get; set; }
        public string KomplikasiDariTindakan { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public int SudahRegDokter { get; set; }
        public int SudahRegPerawat { get; set; }
        public string TandaTanganPerawat { get; set; }
        public string TandaTanganDokter { get; set; }
    }
}