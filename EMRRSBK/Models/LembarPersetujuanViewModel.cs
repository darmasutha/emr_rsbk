﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LembarPersetujuanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string NamaDokterPsikolog { get; set; }
        public string NamaDokterPsikologNama { get; set; }
        public string STR { get; set; }
        public string SIPPK { get; set; }
        public string NamaDokterPasien { get; set; }
        public string NamaDokterPasienNama { get; set; }
        public string UserName { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Ktp { get; set; }
        public string Alamat { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDokter { get; set; }
    }
}