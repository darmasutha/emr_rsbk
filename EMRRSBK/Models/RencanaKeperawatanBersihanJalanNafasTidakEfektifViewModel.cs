﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanBersihanJalanNafasTidakEfektifViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public bool DisfungsiNeuromusuler { get; set; }
        public bool Alergi { get; set; }
        public bool Spasme { get; set; }
        public bool SekresiTertahan { get; set; }
        public bool BendaAsing { get; set; }
        public bool Intubasi { get; set; }
        public bool Ventilasi { get; set; }
        public bool SulitBernafas { get; set; }
        public bool MengeluhBatuk { get; set; }
        public bool BatukBerdahak { get; set; }
        public bool Dispnea { get; set; }
        public bool PerubahanFrekuensi { get; set; }
        public string RR { get; set; }
        public bool Ronchi { get; set; }
        public bool Sianosis { get; set; }
        public bool Gelisah { get; set; }
        public bool Lainnya { get; set; }
        public string Lainnya_Ket { get; set; }
        public string PerawatanSelama { get; set; }
        public string Mandiri_Lain { get; set; }
        public string Kolaborasi_Lain { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}