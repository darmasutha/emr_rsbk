﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LaporanVisumETRepertumViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NamaLaki { get; set; }
        public string JKLaki { get; set; }
        public string UmurLaki { get; set; }
        public string PekerjaanLaki { get; set; }
        public string AgamaLaki { get; set; }
        public string KewarganegaraanLaki { get; set; }
        public string AlamatLaki { get; set; }
        public string LokasiLukaLaki { get; set; }
        public string NamaPerempuan { get; set; }
        public string JKPerempuan { get; set; }
        public string UmurPerempuan { get; set; }
        public string PekerjaanPerempuan { get; set; }
        public string AgamaPerempuan { get; set; }
        public string KewarganegaraanPerempuan { get; set; }
        public string AlamatPerempuan { get; set; }
        public string LokasiLukaPerempuan { get; set; }
        public string Visum1 { get; set; }
        public string Visum2 { get; set; }
    }
}