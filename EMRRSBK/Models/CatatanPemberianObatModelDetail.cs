﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanPemberianObatModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Obat { get; set; }
        public string ObatNama { get; set; }
        public string Dosis { get; set; }
        public string CaraPemakaian { get; set; }
        public bool BenarPasien { get; set; }
        public bool BenarObat { get; set; }
        public bool BenarDosis { get; set; }
        public bool BenarWaktuPemakian { get; set; }
        public bool BenarCaraPemakaian { get; set; }
        public string Keterangan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Username { get; set; }
    }
}
