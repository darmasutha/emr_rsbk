﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class FormPerhitunganIntraoperatifViewModel
    {
        public ListDetail<FormPerhitunganIntraoperatifModelDetail> PrhtnganIntra_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public int? Umur { get; set; }
        public string RM { get; set; }
        public string BenarPerhitungan { get; set; }
        public string Instrumen { get; set; }
        public string InstrumenNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public int SudahRegInstrumen { get; set; }
        public string TandaTanganInstrumen { get; set; }
    }
}