﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PeresapanHemodialisisViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Diagnosis { get; set; }
        public bool RiwayatHD_Ya { get; set; }
        public bool RiwayatHD_Tidak { get; set; }
        public bool Ya_Reguler { get; set; }
        public bool Ya_AntarWaktu { get; set; }
        public string HDTerakhir { get; set; }
        public bool Ya_TravelingDialisis { get; set; }
        public string JenisTindakan { get; set; }
        public string LamaHD { get; set; }
        public string AliranDarah { get; set; }
        public string Ultrafiltrasi { get; set; }
        public string LuasMembran { get; set; }
        public string LuasMembran_Ket { get; set; }
        public string Anticoagulan { get; set; }
        public bool AksesVaskuler_AVShunt { get; set; }
        public bool AksesVaskuler_DLument { get; set; }
        public bool AksesVaskuler_VFemoralis { get; set; }
        public string InstruksiKhusus { get; set; }
        public string DrKonsultan { get; set; }
        public string DokterMeminta { get; set; }

        public string NamaPasien { get; set;}
        public string JenisKelamin { get; set; }
        public int? Umur { get; set;}
        public int Report { get; set;}
        
        public string Ruangan { get; set;}

    }
}