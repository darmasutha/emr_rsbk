﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class HistoryPasienViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string NRM { get; set; }
        public int Nomor { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string Report { get; set; }
        public string Form { get; set; } 


    }
}