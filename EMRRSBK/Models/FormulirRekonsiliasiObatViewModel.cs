﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class FormulirRekonsiliasiObatViewModel
    {
        public ListDetail<FormulirRekonsiliasiObat_AlergiObatModelDetail> Alergi_List { get; set; }
        public ListDetail<FormulirRekonsiliasiObat_ObatDigunakanModelDetail> Obat_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string MemilikiAlergi { get; set; }
        public string MemilikiAlergi_Ket { get; set; }
        public string KodePengisiForm { get; set; }
        public int Report { get; set; }
        public string Username { get; set; }
    }
}