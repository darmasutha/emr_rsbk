﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanNyeriViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_DiagnosaKeperawatan { get; set; }
        public bool DiagnosaKeperawatan_FisikLukaDiKulit { get; set; }
        public bool DiagnosaKeperawatan_Psikologis { get; set; }
        public bool DiagnosaKeperawatan_Biologis { get; set; }
        public bool Ds_NyeriPadaLukaDiKulit { get; set; }
        public bool Ds_NyeriPadaLukaOperasi { get; set; }
        public bool Ds_PusingSakitKepala { get; set; }
        public bool Ds_NyeriPadaLukaKanker { get; set; }
        public bool Ds_NyeriPadaTulangDanSendi { get; set; }
        public bool Ds_NyeriPadaSkala { get; set; }
        public string Ds_KetLainnya { get; set; }
        public string Ds_KetSkala { get; set; }
        public bool Do_WajahMeringisMenyeringai { get; set; }
        public bool Do_NyeriPadaSkala { get; set; }
        public bool Do_PucatAkibatMenahanNyeri { get; set; }
        public bool Do_TingkahLakuBerhati { get; set; }
        public bool Do_PerubahanPadaTandaVital { get; set; }
        public string Do_KetSkala { get; set; }
        public string Tujuan_SetelahDiberikanTindakanKeperawatan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string NamaTerang { get; set; }
        public bool Ds_CbxKetLainnya { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}