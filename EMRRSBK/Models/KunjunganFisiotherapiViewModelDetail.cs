﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class KunjunganFisiotherapiViewModelDetail
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string UserName { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_tindakan { get; set; }
        public string Tindakan { get; set; }
        public int No { get; set; }
    }
}