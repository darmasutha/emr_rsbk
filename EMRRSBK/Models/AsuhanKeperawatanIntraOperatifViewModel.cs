﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanKeperawatanIntraOperatifViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Subyektif_Pasien { get; set; }
        public bool Subyektif_MerasaHaus { get; set; }
        public bool Subyektif_Pusing { get; set; }
        public bool Subyektif_Mulut { get; set; }
        public bool Subyektif_PasienDingin { get; set; }
        public bool Subyektif_PasienPusing { get; set; }
        public bool Subyektif_Telinga { get; set; }
        public bool Subyektif_PasienMual { get; set; }
        public bool Subyektif_PasienKesemutan { get; set; }
        public bool Subyektif_PasienKaki { get; set; }
        public bool Obyektif_TampakGelisah { get; set; }
        public bool Obyektif_KeringatDingin { get; set; }
        public bool Obyektif_BibirKering { get; set; }
        public bool Obyektif_TD { get; set; }
        public string Obyektif_TD_Ket { get; set; }
        public string Obyektif_N_Ket { get; set; }
        public bool Obyektif_R { get; set; }
        public string Obyektif_R_Ket { get; set; }
        public bool Obyektif_NadiKecil { get; set; }
        public bool Obyektif_Tachikardi { get; set; }
        public bool Obyektif_Aproe { get; set; }
        public bool Obyektif_Penurunan { get; set; }
        public bool Obyektif_NafasPendek { get; set; }
        public bool Obyektif_Mengigil { get; set; }
        public bool Obyektif_Menangis { get; set; }
        public bool Obyektif_AlatBantu { get; set; }
        public bool Obyektif_Peningkatan { get; set; }
        public bool Obyektif_Orthopnoe { get; set; }
        public bool Obyektif_Perubahan { get; set; }
        public bool Obyektif_PasienMuntah { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosa { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaHipotermi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaRisiko { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaPolaNafas { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaGangguanRasa { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaKomplikasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaRisikoKecelakaan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamDignosaHambatanMobilitas { get; set; }
        public bool Diagnosa_RisikoGangguan { get; set; }
        public bool Diagnosa_Hipertensi { get; set; }
        public bool Diagnosa_RisikoAspirasi { get; set; }
        public string Diagnosa_RisikoAspirasi_DS { get; set; }
        public bool Diagnosa_PolaNafas { get; set; }
        public string Diagnosa_PolaNafas_DS { get; set; }
        public bool Diagnosa_Gangguan { get; set; }
        public bool Diagnosa_Komplikasi { get; set; }
        public bool Diagnosa_RisikoKecelakaan { get; set; }
        public string Diagnosa_RisikoKecelakaan_DS { get; set; }
        public bool Diagnosa_HambatanMobilitas { get; set; }
        public bool Tujuan_AskepKeseimbangan { get; set; }
        public bool Tujuan_AskepPasien { get; set; }
        public bool Tujuan_AskepTidakAkan { get; set; }
        public bool Tujuan_TindakanAnestesi { get; set; }
        public bool Tujuan_AskepMual { get; set; }
        public bool Tujuan_PompaJantung { get; set; }
        public bool Tujuan_PasienAman { get; set; }
        public bool Tujuan_TindakanPerawatan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanKaji { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanMempertahankanSuhu { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanAlurposisi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanBersihSecret { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanAlurposisi1 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanAlurposisi2 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanTingkatKeamanan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamPerencanaanAlurposisi3 { get; set; }
        public bool Perencanaan_Kaji { get; set; }
        public bool Perencanaan_Kolaborasi { get; set; }
        public bool Perencanaan_MonitorMasukan { get; set; }
        public bool Perencanaan_MonitorHemodinamik { get; set; }
        public bool Perencanaan_MonitorPerdarahan { get; set; }
        public bool Perencanaan_SuhuTubuh { get; set; }
        public bool Perencanaan_PantauTandaVital { get; set; }
        public bool Perencanaan_BeriPenghangat { get; set; }
        public bool Perencanaan_AturPosisi { get; set; }
        public bool Perencanaan_PantauTanda { get; set; }
        public bool Perencanaan_PantauTingkat { get; set; }
        public bool Perencanaan_PantauStatus { get; set; }
        public bool Perencanaan_BersihkanJalan { get; set; }
        public bool Perencanaan_KolaborasiDokter { get; set; }
        public bool Perencanaan_ETT { get; set; }
        public bool Perencanaan_JagaJalanNafas { get; set; }
        public bool Perencanaan_PasangOksigen { get; set; }
        public bool Perencanaan_SuplaiOksigen { get; set; }
        public bool Perencanaan_MonitorOksigen { get; set; }
        public bool Perencanaan_MonitorRitme { get; set; }
        public bool Perencanaan_MonitorPola { get; set; }
        public bool Perencanaan_MonitorTanda { get; set; }
        public bool Perencanaan_AturPosisiPasien { get; set; }
        public bool Perencanaan_Meningkatkan { get; set; }
        public bool Perencanaan_PantauVital { get; set; }
        public bool Perencanaan_PantauGejala { get; set; }
        public bool Perencanaan_PantauJumlah { get; set; }
        public bool Perencanaan_PantauTurgor { get; set; }
        public bool Perencanaan_PantauMasukkan { get; set; }
        public bool Perencanaan_KolaborasiDokter1 { get; set; }
        public bool Perencanaan_AturPosisiPasien1 { get; set; }
        public bool Perencanaan_KajiToleransi { get; set; }
        public bool Perencanaan_Palpitasi { get; set; }
        public bool Perencanaan_KajiTekanan { get; set; }
        public bool Perencanaan_BeriOksigen { get; set; }
        public bool Perencanaan_EvaluasiRespon { get; set; }
        public bool Perencanaan_KolaborasiDokter2 { get; set; }
        public bool Perencanaan_Tingkatkan { get; set; }
        public bool Perencanaan_JagaPosisi { get; set; }
        public bool Perencanaan_UbahTempat { get; set; }
        public bool Perencanaan_CegahResiko { get; set; }
        public bool Perencanaan_PasangPengaman { get; set; }
        public bool Perencanaan_PantauObat { get; set; }
        public bool Perencanaan_AturPosisiPasien2 { get; set; }
        public bool Perencanaan_BantuPergerakan { get; set; }
        public bool Perencanaan_LakukanPenilaian { get; set; }
        public bool Perencanaan_PertahankanPosisi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam1 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam2 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam3 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam4 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam5 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam6 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam7 { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Evaluasi_Jam8 { get; set; }
        public string Evaluasi1_CairanMasuk { get; set; }
        public string Evaluasi1_CairanKeluar { get; set; }
        public string Evaluasi1_TD { get; set; }
        public string Evaluasi1_R { get; set; }
        public string Evaluasi1_N { get; set; }
        public string Evaluasi1_A { get; set; }
        public string Evaluasi1_P { get; set; }
        public string Evaluasi2_SuhuNormal { get; set; }
        public string Evaluasi2_R { get; set; }
        public string Evaluasi2_N { get; set; }
        public string Evaluasi2_Kesadaran { get; set; }
        public string Evaluasi2_A { get; set; }
        public string Evaluasi2_P { get; set; }
        public string Evaluasi3_S { get; set; }
        public string Evaluasi3_A { get; set; }
        public string Evaluasi3_P { get; set; }
        public string Evaluasi4_S { get; set; }
        public string Evaluasi4_TD { get; set; }
        public string Evaluasi4_N { get; set; }
        public string Evaluasi4_R { get; set; }
        public string Evaluasi4_A { get; set; }
        public string Evaluasi4_P { get; set; }
        public string Evaluasi5_TD { get; set; }
        public string Evaluasi5_N { get; set; }
        public string Evaluasi5_R { get; set; }
        public string Evaluasi5_A { get; set; }
        public string Evaluasi5_P { get; set; }
        public string Evaluasi6_A { get; set; }
        public string Evaluasi6_P { get; set; }
        public string Evaluasi7_S { get; set; }
        public string Evaluasi7_A { get; set; }
        public string Evaluasi7_P { get; set; }
        public string Evaluasi8_A { get; set; }
        public string Evaluasi8_P { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public int? Umur { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}