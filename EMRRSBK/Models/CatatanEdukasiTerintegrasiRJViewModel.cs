﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanEdukasiTerintegrasiRJViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Agama { get; set; } 
        public string Pendidikan { get; set; }
        public string NamaPanggilan { get; set; }
        public string NilaiDianut { get; set; }
        public string KesulitanKomunikasi { get; set; }
        public string KesulitanKomunikasi_Ket { get; set; }
        public bool Bahasa_Indonesia { get; set; }
        public bool Bahasa_Inggris { get; set; }
        public bool Bahasa_Lainnya { get; set; }
        public string Bahasa_Lainnya_Ket { get; set; }
        public string Penerjemah { get; set; }
        public string Penerjemah_Ket { get; set; }
        public bool TidakAdaHambatan { get; set; }
        public bool GangguanPenglihatan { get; set; }
        public bool GangguanProsesPikir { get; set; }
        public bool MotivasiBelajar { get; set; }
        public bool GangguanPendengaran { get; set; }
        public bool HambatanBahasa { get; set; }
        public bool BatasanJasmani { get; set; }
        public bool KemampuanMembaca { get; set; }
        public bool GangguanEmosional { get; set; }
        public bool HambatanLainnya { get; set; }
        public string HambatanLainnya_Ket { get; set; }
        public string KesediaanPasien { get; set; }
        public bool KondisiMedis { get; set; }
        public bool RencanaPerawatan { get; set; }
        public bool PerawatanLuka { get; set; }
        public bool PerawatanLanjutan { get; set; }
        public bool PenggunaanAlatMedis { get; set; }
        public bool ManajemenNyeri { get; set; }
        public bool Diet { get; set; }
        public bool PenggunaanObat { get; set; }
        public bool InteraksiObat { get; set; }
        public bool TeknikRehabilitasi { get; set; }
        public bool PengisianInformed { get; set; }
        public bool TeknikCuciTangan { get; set; }
        public bool EdukasiLainnya1 { get; set; }
        public string EdukasiLainnya1_Ket { get; set; }
        public bool EdukasiLainnya2 { get; set; }
        public string EdukasiLainnya2_Ket { get; set; }
        public bool EdukasiLainnya3 { get; set; }
        public string EdukasiLainnya3_Ket { get; set; }
        public string BidangDisiplin1 { get; set; }
        public string BidangDisiplin2 { get; set; }
        public string BidangDisiplin3 { get; set; }
        public string DokterSpesialis_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> DokterSpesialis_Tanggal { get; set; }
        public string DokterSpesialis_Metode { get; set; }
        public string DokterSpesialis_MetodeMenit { get; set; }
        public string ManajemenNyeri_MetodeMenit { get; set; }
        public string Nutrisi_MetodeMenit { get; set; }
        public string Rohaniawan_MetodeMenit { get; set; }
        public string Farmasi_MetodeMenit { get; set; }
        public string Rehabilitasi_MetodeMenit { get; set; }
        public bool DokterSpesialis_TidakRespon { get; set; }
        public bool DokterSpesialis_TidakPaham { get; set; }
        public bool DokterSpesialis_Paham { get; set; }
        public bool DokterSpesialis_Dibantu { get; set; }
        public bool DokterSpesialis_TanpaDibantu { get; set; }
        public bool DokterSpesialis_Mampu { get; set; }
        public bool DokterSpesialis_Lainnya { get; set; }
        public string DokterSpesialis_Lainnya_Ket { get; set; }
        public string DokterSpesialis_NamaEdukator { get; set; }
        public string DokterSpesialis_NamaEdukatorNama { get; set; }
        public string DokterSpesialis_Pasien { get; set; }
        public string DokterSpesialis_PasienNama { get; set; }
        public string Nutrisi_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Nutrisi_Tanggal { get; set; }
        public string Nutrisi_Metode { get; set; }
        public bool Nutrisi_TidakRespon { get; set; }
        public bool Nutrisi_TidakPaham { get; set; }
        public bool Nutrisi_DapatMenjelaskan { get; set; }
        public bool Nutrisi_Mampu { get; set; }
        public bool Nutrisi_Lainnya { get; set; }
        public string Nutrisi_Lainnya_Ket { get; set; }
        public string Nutrisi_Edukator { get; set; }
        public string Nutrisi_EdukatorNama { get; set; }
        public string Nutrisi_Pasien { get; set; }
        public string ManajemenNyeri_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> ManajemenNyeri_Tanggal { get; set; }
        public string ManajemenNyeri_Metode { get; set; }
        public bool ManajemenNyeri_TidakRespon { get; set; }
        public bool ManajemenNyeri_TidakPaham { get; set; }
        public bool ManajemenNyeri_DapatMenjelaskan { get; set; }
        public bool ManajemenNyeri_Mampu { get; set; }
        public bool ManajemenNyeri_Lainnya { get; set; }
        public string ManajemenNyeri_Lainnya_Ket { get; set; }
        public string ManajemenNyeri_Edukator { get; set; }
        public string ManajemenNyeri_EdukatorNama { get; set; }
        public string ManajemenNyeri_Pasien { get; set; }
        public string Rohaniawan_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Rohaniawan_Tanggal { get; set; }
        public string Rohaniawan_Metode { get; set; }
        public bool Rohaniawan_Mampu { get; set; }
        public string Rohaniawan_Edukator { get; set; }
        public string Rohaniawan_EdukatorNama { get; set; }
        public string Rohaniawan_Pasien { get; set; }
        public string Farmasi_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Farmasi_Tanggal { get; set; }
        public string Farmasi_Metode { get; set; }
        public bool Farmasi_TidakRespon { get; set; }
        public bool Farmasi_TidakPaham { get; set; }
        public bool Farmasi_Paham { get; set; }
        public bool Farmasi_Dibantu { get; set; }
        public bool Farmasi_TanpaDibantu { get; set; }
        public bool Farmasi_Mampu { get; set; }
        public bool Farmasi_Lainnya { get; set; }
        public string Farmasi_Lainnya_Ket { get; set; }
        public string Farmasi_Edukator { get; set; }
        public string Farmasi_EdukatorNama { get; set; }
        public string Farmasi_Pasien { get; set; }
        public string PerawatA_Ket1 { get; set; }
        public string PerawatA_Ket2 { get; set; }
        public string Perawat_Lain_Ket { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Perawat_Tanggal { get; set; }
        public string Perawat_Metode { get; set; }
        public string Perawat_MetodeMenit { get; set; }
        public bool Perawat_TidakRespon { get; set; }
        public bool Perawat_TidakPaham { get; set; }
        public bool Perawat_Paham { get; set; }
        public bool Perawat_Dibantu { get; set; }
        public bool Perawat_TanpaDibantu { get; set; }
        public bool Perawat_Mampu { get; set; }
        public bool Perawat_Lainnya { get; set; }
        public string Perawat_Lainnya_Ket { get; set; }
        public string Perawat_Edukator { get; set; }
        public string Perawat_EdukatorNama { get; set; }
        public string Perawat_Pasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Rehabilitasi_Tanggal { get; set; }
        public string Rehabilitasi_Metode { get; set; }
        public bool Rehabilitasi_TidakRespon { get; set; }
        public bool Rehabilitasi_TidakPaham { get; set; }
        public bool Rehabilitasi_Paham { get; set; }
        public bool Rehabilitasi_Dibantu { get; set; }
        public bool Rehabilitasi_TanpaDibantu { get; set; }
        public bool Rehabilitasi_Mampu { get; set; }
        public bool Rehabilitasi_Lain { get; set; }
        public string Rehabilitasi_Lain_Ket { get; set; }
        public string Rehabilitasi_Edukator { get; set; }
        public string Rehabilitasi_EdukatorNama { get; set; }
        public string Rehabilitasi_Pasien { get; set; }
        public string Username { get; set; }
        public string JenisKerjasama { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string PenanggungNama { get; set; }
        public int SudahRegDokterSpesialis { get; set; }
        public int SudahRegPasienSpesialis { get; set; }
        public int SudahRegManajemenNyeri { get; set; }
        public int SudahRegPasienManajemenNyeri { get; set; }
        public int SudahRegNutrisi { get; set; }
        public int SudahRegPasienNutrisi { get; set; }
        public int SudahRegRohaniawan { get; set; }
        public int SudahRegPasienRohaniawan { get; set; }
        public int SudahRegFarmasi { get; set; }
        public int SudahRegPasienFarmasi { get; set; }
        public int SudahRegRehabilitasi { get; set; }
        public int SudahRegPasienRehabilitasi { get; set; }
        public int SudahRegPerawat { get; set; }
        public int SudahRegPasienPerawat { get; set; }
        public string TandaTanganDokterSpesialis_NamaEdukator { get; set; }
        public string TandaTanganManajemenNyeri_Edukator { get; set; }
        public string TandaTanganNutrisi_Edukator { get; set; }
        public string TandaTanganRohaniawan_Edukator { get; set; }
        public string TandaTanganFarmasi_Edukator { get; set; }
        public string TandaTanganPerawat_Edukator { get; set; }
        public string TandaTanganRehabilitasi_Edukator { get; set; }
        public string TandaTanganDokterSpesialis_Pasien { get; set; }
        public string TandaTanganNutrisi_Pasien { get; set; }
        public string TandaTanganManajemenNyeri_Pasien { get; set; }
        public string TandaTanganRohaniawan_Pasien { get; set; }
        public string TandaTanganFarmasi_Pasien { get; set; }
        public string TandaTanganPerawat_Pasien { get; set; }
        public string TandaTanganRehabilitasi_Pasien { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
    }
}
