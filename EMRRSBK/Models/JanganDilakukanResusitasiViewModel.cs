﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class JanganDilakukanResusitasiViewModel
    {
        public string NoReg { get; set; }

        public string SectionID { get; set; }

        public string NRM { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> Tanggal { get; set; }

        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string JKBertandaTangan { get; set; }

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> TglLahir { get; set; }

        public string No_Telp { get; set; }

        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public int SudahRegDPJP { get; set; }
        public int SudahRegPasien { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTangan { get; set; }
    }
}