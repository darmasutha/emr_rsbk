﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DokumenViewModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public int No { get; set; }
    }
}