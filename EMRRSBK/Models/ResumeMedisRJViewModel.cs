﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ResumeMedisRJViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglKeluar { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakit { get; set; }
        public string Alergi { get; set; }
        public string Kesadaran { get; set; }
        public string TD { get; set; }
        public string Suhu { get; set; }
        public string Nadi { get; set; }
        public string Pernafasan { get; set; }
        public string SpO2 { get; set; }
        public string PemeriksaanFisik { get; set; }
        public string PemeriksaanPenunjang { get; set; }
        public string Diagnosa { get; set; }
        public string Pengobatan { get; set; }
        public string Saran { get; set; }
        public string DokterID { get; set; }
        public string DokterIDNama { get; set; }


        public string NRM { get; set; }
        public string Poliklinik { get; set; }
        public string Kewarganegaraan { get; set; }
        public string Alamat { get; set; }
    }
}