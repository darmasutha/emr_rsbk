﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanRisikoKekuranganVolumeCairanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_DiagnosaKeperawatan { get; set; }
        public bool DiagnosaKeperawatan_KehilanganVolumeCairan { get; set; }
        public bool DiagnosaKeperawatan_KegagalanMekanisme { get; set; }
        public bool Ds_MengeluhHaus { get; set; }
        public bool Ds_Cbx_KetLainnya { get; set; }
        public string Ds_KetLainnya { get; set; }
        public bool Do_PenurunanTurgorKulit { get; set; }
        public bool Do_MembranMukosa { get; set; }
        public bool Do_Cbx_SNTD { get; set; }
        public bool Do_UbunCekung { get; set; }
        public bool Do_MataCekung { get; set; }
        public bool Do_MukosaBibirKering { get; set; }
        public bool Do_Cbx_DiareMuntah { get; set; }
        public bool Do_PengisianVena { get; set; }
        public bool Do_PerubahanStatusMental { get; set; }
        public bool Do_Cbx_KonsentrasiUrine { get; set; }
        public bool Do_Cbx_PenurunanBB { get; set; }
        public bool Do_Cbx_UrineOutput { get; set; }
        public bool Do_KelemahanFisik { get; set; }
        public bool Do_Cbx_Hematocrit { get; set; }
        public bool Do_Cbx_CVPmmHg { get; set; }
        public string Do_KetS { get; set; }
        public string Do_KetN { get; set; }
        public string Do_KetTD { get; set; }
        public string Do_KetDiare { get; set; }
        public string Do_KetMuntah { get; set; }
        public string Do_KetBJUrine { get; set; }
        public string Do_KetPenurunanBB { get; set; }
        public string Do_KetUrineOutput { get; set; }
        public string Do_KetHematocrit { get; set; }
        public string Do_KetCVPmmHg { get; set; }
        public string Do_PerubahanStatusMental_E { get; set; }
        public string Do_PerubahanStatusMental_V { get; set; }
        public string Do_PerubahanStatusMental_M { get; set; }
        public string Tujuan_SetelahDilakukanTindakanKeperawatan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> RencanaTindakan_Jam_MonitorStatusHidrasi { get; set; }
        public string Paraf { get; set; }
        public string NamaTerang { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}