﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class EMRPenggajianAwalViewModel
    {
        public int No { get; set; }
        public string SectionID { get; set; }
        public string KodeUrl { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
}