﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class StandardViewModel
    {
        public string Kode { get;  set; }
        public int KodeInt { get;  set; }
        public string Nama { get;  set; }
    }
}