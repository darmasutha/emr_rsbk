﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DeviceViewModel
    {
        public string device_name { get; set; }
        public string sn { get; set; }
        public string vc { get; set; }
        public string ac { get; set; }
        public string vkey { get; set; }
    }
}