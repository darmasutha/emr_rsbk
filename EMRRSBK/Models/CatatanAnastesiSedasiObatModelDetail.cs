﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanAnastesiSedasiObatModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string Obat { get; set; }
        public string ObatNama { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu { get; set; }
        public string Jumlah { get; set; }
        public string Air { get; set; }
        public string Gas { get; set; }
    }
}