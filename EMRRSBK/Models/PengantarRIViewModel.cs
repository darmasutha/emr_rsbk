﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PengantarRIViewModel 
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; } 
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string No { get; set; }
        public string Dari { get; set; }
        public bool AlasanDirawat_Diagnostik { get; set; }
        public bool AlasanDirawat_Kuratif { get; set; }
        public bool AlasanDirawat_Paliatif { get; set; }
        public bool AlasanDirawat_Rehabilitatif { get; set; }
        public string Diagnosa { get; set; }
        public string RencanaTindakan { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglTindakan { get; set; }
        public bool Persiapan_Puasa { get; set; }
        public bool Persiapan_Diet { get; set; }
        public bool Persiapan_Alat { get; set; }
        public bool Persiapan_Tidakperlu { get; set; }
        public bool Persiapan_Lain { get; set; }
        public string Persiapan_Lain_Ket { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglMRS { get; set; }
        public string MasukRawat { get; set; }
        public bool Bangsal { get; set; }
        public bool IntensiveCare { get; set; }
        public bool Isolasi { get; set; }
        public string NoBpjs { get; set; }
        public string KeteranganLainnya { get; set; }
        public string Ruang { get; set; }
        public string Pasien { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int SudahRegPasien { get; set; }
        public int SudahRegPegawai { get; set; }
        public string TTDpetugas { get; set; }
        public string TTDPasien { get; set; }
    }
}