﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class HasilBacaRadiologiViewModel
    {
        public string RegNo { get; set; }
        public string NoBukti { get; set; }
        public string NRM { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string RAD_AksesNumber { get; set; }
        public string StatusRad { get; set; }
        public Nullable<int> Nomor { get; set; }
        public string DokterPengirim { get; set; }

        public List<HasilBacaRadiologiDetailViewModel> Detail_List { get; set; }
    }

    public class HasilBacaRadiologiDetailViewModel
    {
        public string RegNo { get; set; }
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NamaDOkter { get; set; }
        public string RAD_AksesNumber { get; set; }
        public string StatusRad { get; set; }
        public Nullable<int> Nomor { get; set; }
        public Nullable<System.DateTime> TglInput { get; set; }
        public string TglInput_View { get; set; }
        public string DokterPengirim { get; set; }
        public string Klinis { get; set; }
        public string Judul { get; set; }
        public string JawabanBody { get; set; }
        public string Kesan { get; set; }
    }
}