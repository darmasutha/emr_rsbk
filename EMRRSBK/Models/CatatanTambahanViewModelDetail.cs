﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanTambahanViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglUraian { get; set; }
        public string Uraian { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Nilai { get; set; }
        public string ParafVerifikator { get; set; }
        public string ParafVerifikatorNama { get; set; }
        public string Username { get; set; }
    }
}