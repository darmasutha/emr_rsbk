﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class TimPengendalianInfeksiNosokomialViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Pekerjaan { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalMRS { get; set; }
        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKRS { get; set; }
        public string Diagnosis { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalOperasi { get; set; }
        public string Operator { get; set; }
        public string OperatorNama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPemasanganKateter { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPenggantianKateter { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPelepasanKateter { get; set; }
        public string YangMelakukanKateter { get; set; }
        public string JenisKateter { get; set; }
        public string KateterSilicon { get; set; }
        public string ReaksiTimbul1_Bengkak_1 { get; set; }
        public string ReaksiTimbul1_Bengkak_2 { get; set; }
        public string ReaksiTimbul1_Bengkak_3 { get; set; }
        public string ReaksiTimbul1_Bengkak_4 { get; set; }
        public string ReaksiTimbul1_Bengkak_5 { get; set; }
        public string ReaksiTimbul1_Bengkak_6 { get; set; }
        public string ReaksiTimbul1_Bengkak_7 { get; set; }
        public string ReaksiTimbul1_Bengkak_8 { get; set; }
        public string ReaksiTimbul1_Merah_1 { get; set; }
        public string ReaksiTimbul1_Merah_2 { get; set; }
        public string ReaksiTimbul1_Merah_3 { get; set; }
        public string ReaksiTimbul1_Merah_4 { get; set; }
        public string ReaksiTimbul1_Merah_5 { get; set; }
        public string ReaksiTimbul1_Merah_6 { get; set; }
        public string ReaksiTimbul1_Merah_7 { get; set; }
        public string ReaksiTimbul1_Merah_8 { get; set; }
        public string ReaksiTimbul1_Suhu30C_1 { get; set; }
        public string ReaksiTimbul1_Suhu30C_2 { get; set; }
        public string ReaksiTimbul1_Suhu30C_3 { get; set; }
        public string ReaksiTimbul1_Suhu30C_4 { get; set; }
        public string ReaksiTimbul1_Suhu30C_5 { get; set; }
        public string ReaksiTimbul1_Suhu30C_6 { get; set; }
        public string ReaksiTimbul1_Suhu30C_7 { get; set; }
        public string ReaksiTimbul1_Suhu30C_8 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_1 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_2 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_3 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_4 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_5 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_6 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_7 { get; set; }
        public string ReaksiTimbul1_SedimenUrine_8 { get; set; }
        public string ReaksiTimbul1_Lelosit_1 { get; set; }
        public string ReaksiTimbul1_Lelosit_2 { get; set; }
        public string ReaksiTimbul1_Lelosit_3 { get; set; }
        public string ReaksiTimbul1_Lelosit_4 { get; set; }
        public string ReaksiTimbul1_Lelosit_5 { get; set; }
        public string ReaksiTimbul1_Lelosit_6 { get; set; }
        public string ReaksiTimbul1_Lelosit_7 { get; set; }
        public string ReaksiTimbul1_Lelosit_8 { get; set; }
        public string ReaksiTimbul1_Eritrosit_1 { get; set; }
        public string ReaksiTimbul1_Eritrosit_2 { get; set; }
        public string ReaksiTimbul1_Eritrosit_3 { get; set; }
        public string ReaksiTimbul1_Eritrosit_4 { get; set; }
        public string ReaksiTimbul1_Eritrosit_5 { get; set; }
        public string ReaksiTimbul1_Eritrosit_6 { get; set; }
        public string ReaksiTimbul1_Eritrosit_7 { get; set; }
        public string ReaksiTimbul1_Eritrosit_8 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKulturUrine { get; set; }
        public string KulturUrine_Negatif { get; set; }
        public string KulturUrine_Positif { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPemasanganInfus { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPenggantianInfus { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalPelepasanInfus { get; set; }
        public string LokasiInfus { get; set; }
        public string YangMelakukanInfus { get; set; }
        public string NaCL { get; set; }
        public string RL { get; set; }
        public string PemberianCairanLainLain { get; set; }
        public string RD { get; set; }
        public string D5Persen { get; set; }
        public string D10Persen { get; set; }
        public string Nutrisi { get; set; }
        public string PemberianAB { get; set; }
        public string Kemoterapi { get; set; }
        public string ObatLain { get; set; }
        public string TransfusiWholeBlood { get; set; }
        public string PRC { get; set; }
        public string FFP { get; set; }
        public string Trombosit { get; set; }
        public string TransfusiWholeBlood_LainLain { get; set; }
        public string ReaksiTimbul2_Bengkak_1 { get; set; }
        public string ReaksiTimbul2_Bengkak_2 { get; set; }
        public string ReaksiTimbul2_Bengkak_3 { get; set; }
        public string ReaksiTimbul2_Bengkak_4 { get; set; }
        public string ReaksiTimbul2_Bengkak_5 { get; set; }
        public string ReaksiTimbul2_Bengkak_6 { get; set; }
        public string ReaksiTimbul2_Merah_1 { get; set; }
        public string ReaksiTimbul2_Merah_2 { get; set; }
        public string ReaksiTimbul2_Merah_3 { get; set; }
        public string ReaksiTimbul2_Merah_4 { get; set; }
        public string ReaksiTimbul2_Merah_5 { get; set; }
        public string ReaksiTimbul2_Merah_6 { get; set; }
        public string ReaksiTimbul2_Suhu38C_1 { get; set; }
        public string ReaksiTimbul2_Suhu38C_2 { get; set; }
        public string ReaksiTimbul2_Suhu38C_3 { get; set; }
        public string ReaksiTimbul2_Suhu38C_4 { get; set; }
        public string ReaksiTimbul2_Suhu38C_5 { get; set; }
        public string ReaksiTimbul2_Suhu38C_6 { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalKulturDarah { get; set; }
        public string KulturDarahNegatif { get; set; }
        public string KulturDarahPositif { get; set; }
        public string TempatPembedahanDilakukan { get; set; }
        public string GiliranRondaOperasi { get; set; }
        public string YangMelakukanOperasi_DokterAhli { get; set; }
        public string YangMelakukanOperasi_DokterAhliNama { get; set; }
        public string YangMelakukanOperasi_Asisten_1 { get; set; }
        public string YangMelakukanOperasi_Asisten_1Nama { get; set; }
        public string YangMelakukanOperasi_Asisten_2 { get; set; }
        public string YangMelakukanOperasi_Asisten_2Nama { get; set; }
        public string GolonganPembedahan_Bersih { get; set; }
        public string GolonganPembedahan_BersihTerkontaminasi { get; set; }
        public string GolonganPembedahan_Infoksi { get; set; }
        public string GolonganPembedahan_Kotor { get; set; }
        public string LamaPembedahan { get; set; }
        public string Jam_LamaPembedahan { get; set; }
        public string Menit_LamaPembedahan { get; set; }
        public string NamaTindakanPembedahan { get; set; }
        public string Catatan_KeadaanMempengaruhiLukaOperasi { get; set; }
        public string Catatan_DiabetesMellitus { get; set; }
        public string Catatan_Obesitas { get; set; }
        public string Catatan_GangguanHati { get; set; }
        public string Catatan_GangguanGinjal { get; set; }
        public string Catatan_GangguanSystemKekebalan { get; set; }
        public bool CATATAN_Harike_1 { get; set; }
        public bool CATATAN_Harike_2 { get; set; }
        public bool CATATAN_Harike_3 { get; set; }
        public bool CATATAN_Harike_4 { get; set; }
        public bool CATATAN_Harike_5 { get; set; }
        public bool CATATAN_Harike_6 { get; set; }
        public bool CATATAN_Harike_7 { get; set; }
        public bool CATATAN_Harike_8 { get; set; }
        public bool CATATAN_Harike_9 { get; set; }
        public bool CATATAN_Harike_10 { get; set; }
        public bool CATATAN_Harike_11 { get; set; }
        public bool CATATAN_Harike_12 { get; set; }
        public bool CATATAN_Harike_13 { get; set; }
        public bool CATATAN_Harike_14 { get; set; }
        public string KeadaanLukaOperasiHariKe3_Kering { get; set; }
        public string KeadaanLukaOperasiHariKe3_KeluarPusKulturNegatif { get; set; }
        public string KeadaanLukaOperasiHariKe3_KeluarPusKulturpositif { get; set; }
        public string KeadaanLukaOperasiHariKe3_EksudatDengan { get; set; }
        public string KeadaanLukaOperasiHariKe3_JenisKuman { get; set; }
        public string KeadaanLukaOperasiHariKe3_KulturPositif { get; set; }
        public string KeadaanLukaOperasiHariKe3_KulturNegatif { get; set; }
        public string KeadaanLukaOperasiHariKe5_Kering { get; set; }
        public string KeadaanLukaOperasiHariKe5_KeluarPusKulturNegatif { get; set; }
        public string KeadaanLukaOperasiHariKe5_KeluarPusKulturpositif { get; set; }
        public string KeadaanLukaOperasiHariKe5_EksudatDengan { get; set; }
        public string KeadaanLukaOperasiHariKe5_JenisKuman { get; set; }
        public string KeadaanLukaOperasiHariKe5_KulturPositif { get; set; }
        public string KeadaanLukaOperasiHariKe5_KulturNegatif { get; set; }
        public string KeadaanLukaOperasiHariKe7_Kering { get; set; }
        public string KeadaanLukaOperasiHariKe7_KeluarPusKulturNegatif { get; set; }
        public string KeadaanLukaOperasiHariKe7_KeluarPusKulturpositif { get; set; }
        public string KeadaanLukaOperasiHariKe7_EksudatDengan { get; set; }
        public string KeadaanLukaOperasiHariKe7_JenisKuman { get; set; }
        public string KeadaanLukaOperasiHariKe7_KulturPositif { get; set; }
        public string KeadaanLukaOperasiHariKe7_KulturNegatif { get; set; }
        public string YangMelakukan_Dokter { get; set; }
        public string YangMelakukan_DokterNama { get; set; }
        public string YangMelakukan_PinataAnastesi { get; set; }
        public string YangMelakukan_PinataAnastesiNama { get; set; }
        public string YangMelakukan_Perawat { get; set; }
        public string YangMelakukan_PerawatNama { get; set; }
        public string GejalaInfeksi_Batuk { get; set; }
        public string GejalaInfeksi_NyeriDada { get; set; }
        public string GejalaInfeksi_SputumPurulen { get; set; }
        public string PhotoThorax { get; set; }
        public string KuiturSputum_Positif { get; set; }
        public string KuiturSputum_Negatif { get; set; }
        public bool Dekubitus_Harike_1 { get; set; }
        public bool Dekubitus_Harike_2 { get; set; }
        public bool Dekubitus_Harike_3 { get; set; }
        public bool Dekubitus_Harike_4 { get; set; }
        public bool Dekubitus_Harike_5 { get; set; }
        public bool Dekubitus_Harike_6 { get; set; }
        public bool Dekubitus_Harike_7 { get; set; }
        public bool Dekubitus_Harike_8 { get; set; }
        public bool Dekubitus_Harike_9 { get; set; }
        public bool Dekubitus_Harike_10 { get; set; }
        public bool Dekubitus_Harike_11 { get; set; }
        public bool Dekubitus_Harike_12 { get; set; }
        public bool Dekubitus_Harike_13 { get; set; }
        public bool Dekubitus_Harike_14 { get; set; }
        public bool Dekubitus_Harike_15 { get; set; }
        public string PetugasPemeriksa { get; set; }
        public string PetugasPemeriksaNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string Ruangan { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}