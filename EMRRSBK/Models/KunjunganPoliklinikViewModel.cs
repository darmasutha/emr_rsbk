﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class KunjunganPoliklinikViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKunjunganPoliklinik_View { get; set; }
        public string Tempat { get; set; }
        public string Username { get; set; }
        public string SOAPSelect { get; set; }
        public string SOAP { get; set; }
        public string Diagnosa { get; set; }
        public string DiagnosaSelect { get; set; }
        public string IntruksiSelect { get; set; }
        public string Intruksi { get; set; }
        public string Terapi { get; set; }
        public string PrintEResepID { get; set; }
        public string PrintEResepFarmasi { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string Tanggal_View { get; set; }
        public string Jam_View { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDokter { get; set; }
        public string ViewBag { get; set; }
        public string _METHOD { get; set; }
        public string Edit { get; set; }
        public bool saveTemplate { get; set; }
        public string templateName { get; set; }
        public string DokumenID { get; set; }
    }
}
