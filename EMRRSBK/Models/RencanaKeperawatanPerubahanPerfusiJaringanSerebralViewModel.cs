﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanPerubahanPerfusiJaringanSerebralViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public bool Diagnosa_PerdarahanIntra { get; set; }
        public bool Diagnosa_Serebral { get; set; }
        public bool Diagnosa_OklusiOtak { get; set; }
        public bool Diagnosa_Vasopasme { get; set; }
        public bool Diagnosa_Edema { get; set; }
        public bool DS_Mengeluh { get; set; }
        public bool DS_Lainnya { get; set; }
        public string DS_Lainnya_Ket { get; set; }
        public bool DO_TIK { get; set; }
        public bool DO_Penurunan { get; set; }
        public bool DO_TampakGelisah { get; set; }
        public bool DO_TTV { get; set; }
        public string Tujuan_KeperawatanSelama { get; set; }
        public bool Mandiri_1 { get; set; }
        public bool Mandiri_2 { get; set; }
        public bool Mandiri_3 { get; set; }
        public bool Mandiri_4 { get; set; }
        public bool Mandiri_5 { get; set; }
        public bool Mandiri_6 { get; set; }
        public bool Mandiri_7 { get; set; }
        public bool Kolaborasi_1 { get; set; }
        public bool Kolaborasi_2 { get; set; }
        public string KodeDokter { get; set; }
        public string KodeDokterNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}