﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class trTempletViewModel
    {
        public string NoReg { get; set; }
        public string NamaTemplate { get; set; }
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public string DokumenID { get; set; }
    }
}