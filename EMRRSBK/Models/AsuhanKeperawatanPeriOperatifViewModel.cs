﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AsuhanKeperawatanPeriOperatifViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string UserName { get; set; }
        public string DiagnosaPreOp { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MulaiAnastesi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> SelesaiAnastesi { get; set; }
        public string TeknikAnastesi { get; set; }
        public string BB { get; set; }
        public bool DSubyektif_PasienBelumTahu { get; set; }
        public bool DSubyektif_MulutTerasaPahit { get; set; }
        public bool DSubyektif_Haus { get; set; }
        public bool DSubyektif_PuasaLama { get; set; }
        public bool DSubyektif_PasienMenyatakanNyeri { get; set; }
        public bool DObyektif_PasienTampakGelisah { get; set; }
        public bool DObyektif_BerkeringatDingin { get; set; }
        public string DObyektif_TD { get; set; }
        public string DObyektif_N { get; set; }
        public string DObyektif_R { get; set; }
        public bool DObyektif_ProduksiUrine { get; set; }
        public string DObyektif_KetProduksiUrine { get; set; }
        public bool DObyektif_BibirKering { get; set; }
        public bool DObyektif_KukuSianosis { get; set; }
        public bool DObyektif_Merintih { get; set; }
        public bool DObyektif_Menangis { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanResiko { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_DiagnosaKeperawatanNyeri { get; set; }
        public bool DiagnosaKeperawatan_CemasKurangPengetahuan { get; set; }
        public bool DiagnosaKeperawatan_RisikoDefisitvolumeCairan { get; set; }
        public bool DiagnosaKeperawatan_NyeriProsesPenyakitnya { get; set; }
        public bool TujuanKeperawatan_SetelahAskepCemas { get; set; }
        public bool TujuanKeperawatan_SetelahAskepKeseimbangan { get; set; }
        public bool TujuanKeperawatan_SetelahPerawatan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasiKajitingkat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_PerencanaanImplementasiKajiderajat { get; set; }
        public bool PerencanaanImplementasi_KajiTingkatKecemasan { get; set; }
        public bool PerencanaanImplementasi_OrientasikanDenganTim { get; set; }
        public bool PerencanaanImplementasi_JelaskanJenisProsedur { get; set; }
        public bool PerencanaanImplementasi_BeriDoronganPasien { get; set; }
        public bool PerencanaanImplementasi_DampingPasienUntuk { get; set; }
        public bool PerencanaanImplementasi_AjarkanTeknikRelaksasi { get; set; }
        public bool PerencanaanImplementasi_KolaborasiPemberianObat { get; set; }
        public bool PerencanaanImplementasi_KajiTingkatKekurangan { get; set; }
        public bool PerencanaanImplementasi_KolaborasiPemberianCairan { get; set; }
        public bool PerencanaanImplementasi_MonitorMasukanKeluaran { get; set; }
        public bool PerencanaanImplementasi_MonitorHaemodinamik { get; set; }
        public bool PerencanaanImplementasi_MonitorPendarahan { get; set; }
        public bool PerencanaanImplementasi_KajiDerajatLokasi { get; set; }
        public bool PerencanaanImplementasi_GunakanTeknikKomunikasi { get; set; }
        public bool PerencanaanImplementasi_KolaborasiDenganDokter { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_Evaluasi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiPasienMengatakan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JAM_EvaluasiPasienMelaporkan { get; set; }
        public string Evaluasi_PasienTampakTenang_TD { get; set; }
        public string Evaluasi_PasienTampakTenang_N { get; set; }
        public string Evaluasi_PasienTampakTenang_R { get; set; }
        public string Evaluasi_PasienTampakTenang_A { get; set; }
        public string Evaluasi_PasienTampakTenang_P { get; set; }
        public string Evaluasi_CairanMasuk { get; set; }
        public string Evaluasi_CairanKeluar { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_TD { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_N { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_R { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_A { get; set; }
        public string Evaluasi_KebutuhanCairanSeimbang_P { get; set; }
        public string Evaluasi_PasienMelaporkanNyeri_A { get; set; }
        public string Evaluasi_PasienMelaporkanNyeri_P { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }
        public int? Umur { get; set; }
        public string Alamat { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}