﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LaporanOperasiViewModel
    {
        public ListDetail<IntruksiPascaOperasiModelDetail> Intruksi_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Operator { get; set; }
        public string OperatorNama { get; set; }
        public string Asisten1 { get; set; }
        public string Asisten1Nama { get; set; }
        public string Asisten2 { get; set; }
        public string Asisten2Nama { get; set; }
        public string PerawatInstrumen { get; set; }
        public string PerawatInstrumenNama { get; set; }
        public string OnLoop { get; set; }
        public string OnLoopNama { get; set; }
        public string Anestesi { get; set; }
        public string AnestesiNama { get; set; }
        public string PenataAnestesi { get; set; }
        public string PenataAnestesiNama { get; set; }
        public string KamarOperasi { get; set; }
        public string DiagnosaPre { get; set; }
        public string DiagnosaPost { get; set; }
        public string TindakanOperasi { get; set; }
        public string JenisAnestesi { get; set; }
        public string Klasisfikasi { get; set; }
        public string JaringanYangDieksisi { get; set; }
        public string GolonganOperasi { get; set; }
        public string PemeriksaanPA { get; set; }
        public string JenisJaringan { get; set; }
        public string PemeriksaanCairan { get; set; }
        public string JenisPemeriksaan { get; set; }
        public string RuangRawat { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> MulaiAnestesi { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> OperasiDiMulai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> OperasiSelesai { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> LamaOperasi { get; set; }
        public string LaporanOperasi1 { get; set; }
        public string Komplikasi { get; set; }
        public string Perdarahan { get; set; }
        public string Username { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Pangkat { get; set; }
        public string NRP { get; set; }
        public string Kesatuan { get; set; }
        public int? Umur { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
    }
}