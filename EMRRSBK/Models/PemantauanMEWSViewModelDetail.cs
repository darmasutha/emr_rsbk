﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PemantauanMEWSViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Respirasi { get; set; }
        public string TD_Sistolik { get; set; }
        public string Nadi { get; set; }
        public string Temperatur { get; set; }
        public string Spo2 { get; set; }
        public string Kesadaran { get; set; }
        public string Urine { get; set; }
        public string TanpaKeteter { get; set; }
        public string TotalAEWS { get; set; }
        public string RentangSkor { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string RentangSkorMEWS { get; set; }
        public Nullable<int> NomorReport { get; set; }
        public Nullable<int> NomorReport_Group { get; set; }
        public string Username { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTangan { get; set; }
        public int SudahRegDokter { get; set; }
        public string Viewbag { get; set; }
    }
}