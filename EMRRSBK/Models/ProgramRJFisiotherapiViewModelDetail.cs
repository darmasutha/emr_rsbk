﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ProgramRJFisiotherapiViewModelDetail
    { 
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string UserName { get; set; }
        public string Program { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal_Program { get; set; }
        public string TTD_Pasien { get; set; }
        public string TTD_PasienNama { get; set; }
        public string TTD_Dokter { get; set; }
        public string TTD_DokterNama { get; set; }
        public string TTD_Terapis { get; set; }
        public string TTD_TerapisNama { get; set; }
        public int No { get; set; }
        public string TandaTanganTerapis { get; set; }
        public string TandaTanganDokter { get; set; }
        public string TandaTanganPasien { get; set; }
        public int SudahRegDokter { get; set; }
        public int SudahRegTerapis { get; set; }
        public int SudahRegPasien { get; set; }
        public string ViewBagDokter { get; set; }
        public string ViewBagTerapis { get; set; }
        public string ViewBagPasien { get; set; }
    }
}