﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class AssesmentIKeperawatanHiperbarik2ViewModel
    {
        public ListDetail<RencanaKerjaDokterHiperbarikViewModelDetail> RKDHiperbark_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string KondisiFisik { get; set; }
        public string KondisiMental { get; set; }
        public string Aktivitas { get; set; }
        public string Mobilisasi { get; set; }
        public string GangguanPerkemihan { get; set; }
        public string Nilai { get; set; }
        public string KeluhanUtama { get; set; }
        public string RiwayatPenyakit { get; set; }
        public string KeadaanUmum { get; set; }
        public bool Kesadaran_ComposMentis { get; set; }
        public bool Kesadaran_Apatis { get; set; }
        public bool Kesadaran_SoporaComa { get; set; }
        public bool Kesadaran_Coma { get; set; }
        public bool Kesadaran_SulitDinilai { get; set; }
        public string GCS_E { get; set; }
        public string GCS_M { get; set; }
        public string GCS_V { get; set; }
        public string TindakanResusitasi { get; set; }
        public string BeratBadan { get; set; }
        public string TinggiBadan { get; set; }
        public string Tensi { get; set; }
        public string Suhu { get; set; }
        public string Nadi { get; set; }
        public string Respirasi { get; set; }
        public string Saturasi { get; set; }
        public string THT_Telinga { get; set; }
        public string THT_Hidung { get; set; }
        public string THT_Tenggorokan { get; set; }
        public string Thorax_Jantung { get; set; }
        public string Thorax_Paru { get; set; }
        public string Extremitas { get; set; }
        public string Gambar1 { get; set; }
        public string StatusLokalis { get; set; }
        public string DataPenunjang { get; set; }
        public string Diagnosis { get; set; }
        public string Instruksi { get; set; }
        public string KondisiSaatPulang { get; set; }
        public string InstruksiPulang { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string DokterPengkaji { get; set; }
        public string DokterPengkajiNama { get; set; }
        public string DPJP { get; set; }
        public string DPJPNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public int SudahRegDPJP { get; set; }
        public int SudahRegPerawat { get; set; }
        public int SudahRegDokter { get; set; }
        public string TandaTanganDPJP { get; set; }
        public string TandaTanganPerawat { get; set; }
        public string TandaTanganDokter { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; } 
    }
}