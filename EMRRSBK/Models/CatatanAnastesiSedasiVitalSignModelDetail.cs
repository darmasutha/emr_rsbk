﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanAnastesiSedasiVitalSignModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }
        public string VitalSign { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Waktu { get; set; }
        public string R { get; set; }
        public string N { get; set; }
        public string TD { get; set; }
    }
}