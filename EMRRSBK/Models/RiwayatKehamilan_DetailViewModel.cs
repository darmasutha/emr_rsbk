﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RiwayatKehamilan_DetailViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public bool Abortus { get; set; }
        public bool Prematur { get; set; }
        public bool Aterm { get; set; }
        public string JenisPartus { get; set; }
        public bool Nakes { get; set; }
        public bool Non { get; set; }
        public bool JenisKelaminLaki { get; set; }
        public bool JenisKelaminPerempuan { get; set; }
        public string BBL { get; set; }
        public bool Normal { get; set; }
        public bool Cacat { get; set; }
        public bool Meninggal { get; set; }
        public string Keterangan { get; set; }
    }
}