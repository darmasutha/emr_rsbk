﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class DokumenRekamMedisDetailViewModel
    {
        public string NoBukti { get; set; }
        public int NoUrut { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Nullable<System.DateTime> TanggalDibuat { get; set; }
    }
}