﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanTranfusiDarahSelamaHDViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string KadarHB { get; set; }
        public string JenisDarah { get; set; }
        public string GolonganDarah { get; set; }
        public string JumlahTranfusi { get; set; }
        public string Keterangan { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string Username { get; set; }
    }
}