﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class FormulirDPJPViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string Username { get; set; }
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string NamaPasein { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Alamat { get; set; }
        public string NRM { get; set; }
        public string JenisKelamin { get; set; }
        public string DokterPenanggung { get; set; }
        public string DokterPenanggungNama { get; set; }
        public int Report { get; set; }
    }
}