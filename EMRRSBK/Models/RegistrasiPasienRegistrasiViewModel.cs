﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RegistrasiPasienRegistrasiViewModel
    {


        // --MODEL PERSETUJUAN UMUM REGISTRASI

        public RegPersetujuanUmumViewModel RegPersetujuanUmum { get; set; }
        public RegSuratPernyataanViewModel SuratPernyataan { get; set; }
        public RegSuratPernyataanKenaikanKelasViewModel SuratPernyataanKenaikanKelas { get; set; }
        public PersetujuanDirawatICUViewModel PDirawatICU { get; set; }
        public PengantarRIViewModel PengantarRawatInap { get; set; }
        public PersetujuanUmumNewViewModel PU { get; set; }

        // --MODEL CPPT
        public SOAPViewModel CPPT { get; set; }

        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string NRM { get; set; }
        public bool RawatInap { get; set; }
        public string Keterangan { get; set; }
        public string NamaPasien { get; set; }
        public string COmpanyID { get; set; }
        public string NoKartu { get; set; }
        public string JabatanDiPerusahaan { get; set; }
        public string JenisKelamin { get; set; }
        public string JenisKerjasama { get; set; }
        public string NamaKelas { get; set; }
        public string KelasID { get; set; }
        public string JenisPasien { get; set; }
        public string KdKelas { get; set; }
        public bool PasienKTP { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public string Nama_Customer { get; set; }
        public string Alamat { get; set; }
        public Nullable<int> UmurThn { get; set; }
        public Nullable<int> UmurBln { get; set; }
        public string Kode_Customer { get; set; }
        public int NoUrut { get; set; }
        public Nullable<short> Customer_ID { get; set; }
        public bool HD { get; set; }
        public bool DB { get; set; }
        public int JenisPembayaran { get; set; }
        public Nullable<byte> UmurHr { get; set; }
        public string KategoriPlafon { get; set; }
        public string NamaKasus { get; set; }
        public Nullable<int> JmlRIThnIni { get; set; }
        public Nullable<int> JmlRIOpnameIni { get; set; }
        public string StatusBayar { get; set; }
        public string StatusKeberadaan { get; set; }
        public Nullable<double> MarkUp { get; set; }
        public string JenisKunjungan { get; set; }
        public bool PasienBaru { get; set; }
        public bool PxKeluar_Pulang { get; set; }
        public bool PxKeluar_Dirujuk { get; set; }
        public bool PxKeluar_PlgPaksa { get; set; }
        public bool PxMeninggal { get; set; }
        public Nullable<bool> PasienAsuransi { get; set; }
        public string AsuransiID { get; set; }
        public string NoRegInduk { get; set; }
        public bool TermasukObat { get; set; }
        public Nullable<bool> Batal { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public string SectionID { get; set; }
        public Nullable<bool> VIP { get; set; }
        public string VIPKeterangan { get; set; }
        public string AssCompanyID_MA { get; set; }
        public Nullable<bool> Kerjasama { get; set; }
        public Nullable<bool> PasienBlackList { get; set; }
        public bool PasienVVIP { get; set; }
        public bool PasienLoyal { get; set; }
        public System.DateTime JamReg { get; set; }
        public string CaseNo { get; set; }
        public string ReffNo { get; set; }
        public Nullable<bool> Paket { get; set; }
        public string PaketJasaID { get; set; }
        public Nullable<bool> BayiSAkit { get; set; }
        public string Nama_Asuransi { get; set; }
        public string Nama_Asisting { get; set; }
        public Nullable<decimal> ExcessFee { get; set; }
        public string NoAsuransi { get; set; }
        public Nullable<bool> UnitLuar { get; set; }
        public Nullable<bool> UnitIntegrasi { get; set; }
        public Nullable<decimal> CustomerKerjasamaID { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public string Phone { get; set; }
        public string KdKelasAsal { get; set; }
        public bool TitipKelas { get; set; }
        public bool Pribadi { get; set; }
        public Nullable<bool> IKSMixed { get; set; }
        public Nullable<bool> Blok_Vit { get; set; }
        public Nullable<bool> Blok_Suplemen { get; set; }
        public string KdKelasPertanggungan { get; set; }
        public Nullable<int> KelasPertanggunganNomor { get; set; }
        public Nullable<bool> KelasPertanggungan_JKN { get; set; }
        public Nullable<bool> NONKELAS { get; set; }
        public Nullable<bool> JKN { get; set; }
        public string NoSEP { get; set; }
        public string KodeINACBG { get; set; }
        public Nullable<decimal> SELISIHTARIF_JKN { get; set; }
        public Nullable<decimal> TarifINA { get; set; }
        public string KdKelasPelayananSection { get; set; }
        public Nullable<int> KelasPelayananNomor { get; set; }
        public Nullable<bool> NonPBI { get; set; }
        public string DokterKirimID { get; set; }
        public bool Rujukan { get; set; }
        public string Symptom { get; set; }
        public string DokterRawatID { get; set; }
        public string PenanggungNama { get; set; }
        public string MemoMarketing { get; set; }
        public string PenanggungAlamat { get; set; }
        public string Pekerjaan_Reg { get; set; }
        public string Agama_Reg { get; set; }
        public string Nationality { get; set; }
        public string PenanggungHubungan { get; set; }
        public string PenanggungNoKTP { get; set; }
        public string PenanggungTelp { get; set; }
        public string Nama_Supplier { get; set; }
        public string NoANggotaE { get; set; }
        public string NoIdentitas { get; set; }
        public string PenanggungPekerjaan { get; set; }
        public string Email { get; set; }
        public string Kode_Kabupaten { get; set; }
        public string Nama_Kabupaten { get; set; }
        public Nullable<decimal> Deposit { get; set; }
        public Nullable<bool> NaikKelas { get; set; }
        public Nullable<bool> SKL { get; set; }
        public Nullable<bool> UGD { get; set; }
        public Nullable<bool> ODC { get; set; }
        public string Agama { get; set; }
        public string Pekerjaan { get; set; }
        public string AkanRIDokterID { get; set; }
        public Nullable<bool> AkanRI { get; set; }
        public string AkanRISectionID { get; set; }
        public Nullable<System.DateTime> TglRes { get; set; }
        public string NamaPasien_Reg { get; set; }
        public Nullable<bool> Purnawirawan { get; set; }
        public string TNI { get; set; }
        public Nullable<bool> BPJSDInas { get; set; }
        public Nullable<bool> BPJSUMUM { get; set; }
        public string Dinas { get; set; }
        public string NIP { get; set; }
        public string Pangkat { get; set; }
        public string Kesatuan { get; set; }
        public Nullable<bool> Lampiran { get; set; }
        public Nullable<bool> Inaktif { get; set; }
        public Nullable<System.DateTime> Tgl_Inaktif { get; set; }
        public string NoReservasi { get; set; }
        public Nullable<System.DateTime> TglKeluar { get; set; }
        public Nullable<bool> BB { get; set; }
        public Nullable<bool> TB { get; set; }
        public Nullable<bool> StatusGizi { get; set; }
        public Nullable<bool> Mahasiswa { get; set; }
        public Nullable<bool> Karyawan { get; set; }
        public Nullable<int> PaketLamaHari { get; set; }
        public Nullable<int> PaketObat { get; set; }
        public string UnitBisnisID { get; set; }
        public string KamarMasuk { get; set; }
        public string PaketBayiTabung { get; set; }
        public string TempatLahir { get; set; }
        public string SectionAsalName { get; set; }
        public string StatusCPPTHD { get; set; }
        public string AlasanBatal { get; set; }
        public string TglReg_View { get; set; }
        public string Tanggal_View { get; set; }
        public string Jam_View { get; set; }
        public string JamMasuk { get; set; }
        public string NoAntri { get; set; }
        public string Kamar { get; set; }
        public string Tanggal { get; set; }
        public string Jam { get; set; }
        public string NamaDOkter { get; set; }
        public string SectionName { get; set; }
        public string SudahPeriksa { get; set; }
        public string NoBill { get; set; }
        public string Nomor { get; set; }
    }
}