﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RencanaKeperawatanPolaNafasTidakEfektifViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_DiagnosaKeperawatan { get; set; }
        public bool DiagnosaKeperawatan_Hiperventilasi { get; set; }
        public bool DiagnosaKeperawatan_PenurunanEnergy { get; set; }
        public bool DiagnosaKeperawatan_Perusakan { get; set; }
        public bool DiagnosaKeperawatan_Nyeri { get; set; }
        public bool DiagnosaKeperawatan_Kecemasan { get; set; }
        public bool DiagnosaKeperawatan_DeformitasTulang { get; set; }
        public bool DiagnosaKeperawatan_KelainanDindingDada { get; set; }
        public bool DiagnosaKeperawatan_Obesitas { get; set; }
        public bool DiagnosaKeperawatan_PosisiTubuh { get; set; }
        public bool DiagnosaKeperawatan_KelelahanOtot { get; set; }
        public bool DiagnosaKeperawatan_DisfungsiNeuromuskuler { get; set; }
        public bool DiagnosaKeperawatan_PengesetanVentilator { get; set; }
        public bool DiagnosaKeperawatan_ObstruksiSelang { get; set; }
        public bool Ds_MengeluhSesak { get; set; }
        public bool Ds_SulitBernafas { get; set; }
        public bool Ds_Cbx_PerubahanIramaPemafasan { get; set; }
        public string Ds_Cbx_PerubahanIramaPemafasan_Ket { get; set; }
        public string Ds_Rr { get; set; }
        public bool Ds_Cbx_PerubahanIramaNadi { get; set; }
        public string Ds_Cbx_PerubahanIramaNadi_Ket { get; set; }
        public string Ds_Nadi { get; set; }
        public bool Ds_PenggunaanOtot { get; set; }
        public bool Ds_PenurunanVolume { get; set; }
        public bool Ds_Cbx_TerdengarSuaraAlarm { get; set; }
        public string Ds_TerdengarSuaraAlarm { get; set; }
        public string Tujuan_SetelahDilakukanTidakanKeperawatan { get; set; }
        public string Paraf { get; set; }
        public string NamaTerang { get; set; }
        public string NamaTerangNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
    }
}