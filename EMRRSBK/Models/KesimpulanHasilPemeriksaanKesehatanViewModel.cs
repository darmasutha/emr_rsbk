﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class KesimpulanHasilPemeriksaanKesehatanViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string PemeriksaanUntuk { get; set; }
        public string TinggiBadan { get; set; }
        public string BeratBadan { get; set; }
        public string Tensi { get; set; }
        public string Nadi { get; set; }
        public string Fisik { get; set; }
        public string Mata { get; set; }
        public string THT { get; set; }
        public string GIGI { get; set; }
        public string LAB { get; set; }
        public string Rontgen { get; set; }
        public string EKG { get; set; }
        public string JIWA { get; set; }
        public string Nilai { get; set; }
        public string NasehatMedis { get; set; }
        public string NIP { get; set; }
        public string Dokter { get; set; }
        public string DokterNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string TglLahir { get; set; }
        public string NilaiKuantitatif { get; set; }
        public string TandaTanganDokter { get; set; }
        public string GIGIKet { get; set; }

        public string DokumenID { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
    }
}
