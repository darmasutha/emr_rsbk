﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RMDomesticViolenceViewModel
    {

        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string NoKlien { get; set; }
        public string NamaKlien { get; set; }
        public string NamaKorban { get; set; }
        public int? Umur { get; set; }
        public int? UmurKorban { get; set; }
        public string JenisKelamin { get; set; }
        public string TempatLahir { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string NoKTP { get; set; }
        public string Agama { get; set; }
        public bool JenisKasus_Penganiayaan { get; set; }
        public bool JenisKasus_Perkosaan { get; set; }
        public bool JenisKasus_Pencabulan { get; set; }
        public bool JenisKasus_Sodomi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tgl_Pendaftaran { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> WaktuPemeriksaan { get; set; }
        public string TempatPemeriksaan { get; set; }
        public string NomorKeteranganPolisi { get; set; }
        public string NamaPolisi { get; set; }
        public string Kepolisian { get; set; }
        public string DokterPemeriksa { get; set; }
        public string DokterPemeriksaNama { get; set; }
        public string DokterKonsulen { get; set; }
        public string DokterKonsulenNama { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string NamaPekerjaSosial { get; set; }
        public string NamaPsikolog { get; set; }
        public string HubunganPasien { get; set; }
        public string HubunganPasien_Ket { get; set; }
        public string Orangtua_Nama1 { get; set; }
        public string Orangtua_NoKTP1 { get; set; }
        public string Orangtua_Nama2 { get; set; }
        public string Orangtua_NoKTP2 { get; set; }
        public string TTD_Nama1 { get; set; }
        public string TTD_Nama2 { get; set; }
        public string TTD_Pasien { get; set; }
        public string SukuBangsa { get; set; }
        public string Pendidikan { get; set; }
        public string Pekerjaan { get; set; }
        public decimal PenghasilanPerbulan { get; set; }
        public string StatusPernikahan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Kawin_Tahun { get; set; }
        public int Kawin_JumlahAnak { get; set; }
        public int UsiaAnak1 { get; set; }
        public int UsiaAnak2 { get; set; }
        public int UsiaAnak3 { get; set; }
        public int UsiaAnak4 { get; set; }
        public string SekolahAnak1 { get; set; }
        public string SekolahAnak2 { get; set; }
        public string SekolahAnak3 { get; set; }
        public string SekolahAnak4 { get; set; }
        public string AlamatRumah { get; set; }
        public string NoTelpRumah { get; set; }
        public string AlamatKantor { get; set; }
        public string NoTelpKantor { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> RiwayatKejadian_Tanggal { get; set; }
        public string RiwayatKejadian_TempatKejadian { get; set; }
        public bool Dipukul { get; set; }
        public bool Dicakar { get; set; }
        public bool Dibanting { get; set; }
        public bool Digigit { get; set; }
        public bool Didorong { get; set; }
        public bool Ditendang { get; set; }
        public bool Ditampar { get; set; }
        public bool Dijambak { get; set; }
        public bool Pisau { get; set; }
        public bool SenjataApi { get; set; }
        public bool LukadiBagian { get; set; }
        public string LukadiBagian_Ket { get; set; }
        public bool BendaTumpul { get; set; }
        public string BendaTumpul_Ket { get; set; }
        public bool Lainnya { get; set; }
        public string Lainnya_Ket { get; set; }
        public bool AncamanPsikologis { get; set; }
        public string AncamanPsikologis_Ket { get; set; }
        public bool AncamanBendaTajam { get; set; }
        public string AncamanBendaTajam_Ket { get; set; }
        public int BerapaKaliIntercourse { get; set; }
        public int BerapaKali_Pertama { get; set; }
        public int BerapaKali_Kedua { get; set; }
        public int BerapaKali_Ketiga { get; set; }
        public string PelecehanSeksual { get; set; }
        public string KejadianPertamaKali { get; set; }
        public int KejadianPertamaKali_Tidak { get; set; }
        public int JumlahKejadian { get; set; }
        public int Dalam1Bulan { get; set; }
        public int Dalam6Bulan { get; set; }
        public int Dalam1Tahun { get; set; }
        public bool PKTSebelum_Lainnya { get; set; }
        public string PKTSebelum_Lainnya_Ket { get; set; }
        public bool PKTSebelum_RS { get; set; }
        public string PKTSebelum_RS_Ket { get; set; }
        public bool PKT_Sendiri { get; set; }
        public string PKT_Sendiri_NamaPengantar { get; set; }
        public string PKT_Sendiri_Hubungan { get; set; }
        public bool PKT_Polisi { get; set; }
        public string PKT_Polisi_NamaPolisi { get; set; }
        public bool PKT_Lainnya { get; set; }
        public string PKT_Lainnya_Ket { get; set; }
        public bool Solusi_SembuhFisik { get; set; }
        public bool Solusi_SembuhMental { get; set; }
        public bool Solusi_Damai { get; set; }
        public bool Solusi_ProsesHukum { get; set; }
        public string KronologisKejadian { get; set; }
        public string LatarBelakang_Keluarga { get; set; }
        public string LatarBelakang_LingkunganSosial { get; set; }
        public string LatarBelakang_Ekonomi { get; set; }
        public string LatarBelakang_Genogram { get; set; }
        public string MasalahSosial { get; set; }
        public string SaranRekomendasi { get; set; }
        public string RiwayatPenyakit { get; set; }
        public string KeadaanUmum { get; set; }
        public string Perilaku { get; set; }
        public string TandaVital_TD { get; set; }
        public string TandaVital_N { get; set; }
        public string TandaVital_Suhu { get; set; }
        public string Kepala { get; set; }
        public string DindingDada { get; set; }
        public string ParuParu { get; set; }
        public string Kardiovaskuler { get; set; }
        public string Abdomen { get; set; }
        public string Punggung { get; set; }
        public string EkstremitasAtas { get; set; }
        public string EkstremitasBawah { get; set; }
        public string Penatalaksanaan { get; set; }
        public bool FollowUp_LSM { get; set; }
        public string FollowUp_LSM_Ket { get; set; }
        public string FollowUp_LSM_Pendamping { get; set; }
        public string Referensi { get; set; }
        public string Pelaku_Nama { get; set; }
        public int Pelaku_Umur { get; set; }
        public string Pelaku_Hubungan { get; set; }
        public string Pelaku_NoKTP { get; set; }
        public string Pelaku_Alamat { get; set; }
        public string Pelaku_LamaKenal { get; set; }
        public string Pelaku_SukuBangsa { get; set; }
        public string Pelaku_Agama { get; set; }
        public string Pelaku_Pekerjaan { get; set; }
        public decimal Pelaku_Penghasilan { get; set; }
        public string Pelaku_CiriCiri { get; set; }
        public string Pelaku_RiwayatPenyalahgunaanObat { get; set; }
        public string Pelaku_RiwayatPenggunaanAlkohol { get; set; }
        public string Pelaku_RiwayatTindakPidana { get; set; }
        public string Gambar1 { get; set; }
        public string Gambar2 { get; set; }
        public string Gambar3 { get; set; }
        public string Username { get; set; }
    }
}
