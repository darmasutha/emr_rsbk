﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratKeteranganMasihDalamPerawatanViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Nomor { get; set; }
        public string KodeDokter { get; set; }
        public string KodeDokterNama { get; set; }
        public string Jabatan { get; set; }
        public string Diagnosa { get; set; }
        public string Tindakan { get; set; }
        public string Username { get; set; }
        public string NamaPasien { get; set; }
        public int? UmurPasien { get; set; }
        public string JenisKelaminPasien { get; set; }
    }
}