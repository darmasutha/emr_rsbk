﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RegPersetujuanUmumViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string MembukaRahasia_1 { get; set; }
        public string MembukaRahasia_1_Hubungan { get; set; }
        public string MembukaRahasia_2 { get; set; }
        public string MembukaRahasia_2_Hubungan { get; set; }
        public string MembukaRahasia_3 { get; set; }
        public string MembukaRahasia_3_Hubungan { get; set; }
        public string MembukaRahasia_4 { get; set; }
        public string MembukaRahasia_4_Hubungan { get; set; }
        public string Username { get; set; }
        public string Perawat { get; set; }
        public string PerawatNama { get; set; }
        public string Pasien { get; set; }
        public string PasienPasien { get; set; }
        public string PasienNama { get; set; }
        public string TandaTanganPasien { get; set; }
        public string Memberi { get; set; }
        public string Kepada { get; set; }
        public string Pilihan { get; set; }
        public int SudahRegPegawai { get; set; }
        public int SudahRegPasien { get; set; }
        public string TandaTangan { get; set; }
        public string TandaTanganPetugas { get; set; }
    }
}