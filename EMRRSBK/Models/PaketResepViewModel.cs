﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PaketResepViewModel
    {
        public string KodePaket { get; set; }
        public string NamaPaket { get; set; }
    }
}