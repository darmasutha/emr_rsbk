﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class SuratKeteranganIstirahatViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Kepada_Yth { get; set; }
        public string Kepada_Di { get; set; }
        public string NoPol { get; set; }
        public string Memerlukan { get; set; }
        public string Selama { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TerhitungTgl { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> SampaiTgl { get; set; }
        public string KodeDokter { get; set; }
        public string KodeDokterNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public string NamaPasien { get; set; }
        public string Pangkat { get; set; }
        public string Kesatuan { get; set; }
        public int? UmurThn { get; set; }
        public string TandaTanganDokter { get; set; }
        public int SudahRegDokter { get; set; }
    }
}