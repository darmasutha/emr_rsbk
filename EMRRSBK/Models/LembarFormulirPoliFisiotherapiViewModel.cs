﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class LembarFormulirPoliFisiotherapiViewModel
    {
        public ListDetail<ProgramRJFisiotherapiViewModelDetail> Program_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string UserName { get; set; }
        public string Hub_Tertanggung { get; set; }
        public string Anamnesa { get; set; }
        public string PeriksaFisik_UjiFungsi { get; set; }
        public string Diag_Medis { get; set; }
        public string Diag_Fungsi { get; set; }
        public string Tata_Laksana { get; set; }
        public string Anjuran { get; set; }
        public string Evaluasi { get; set; }
        public string Suspek_Penyakit { get; set; }
        public string Ket_Suspek_Penyakit { get; set; }
        public string Diagnosis { get; set; }
        public string Permintaan_Terapi { get; set; }
        public string Tempat { get; set; }
        public string Nama_Dokter { get; set; }
        public string Nama_DokterNama { get; set; } 
        public string PeriksaPenunjang { get; set; }
        public int Report { get; set; }
        public int Nomor { get; set; }
        public string NamaPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Phone { get; set; }
        public string PenanggungNama { get; set; }
        public int SudahRegPasien { get; set; }
        public int SudahRegNama_Dokter { get; set; }
        public string TandaTangan { get; set; }
        public string TandaTanganNama_Dokter { get; set; }

        public string DokumenID { get; set; }
        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
    }
}