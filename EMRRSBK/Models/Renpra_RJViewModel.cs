﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class Renpra_RJViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public int No { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Diagnosa { get; set; }
        public string Tujuan { get; set; }
        public string Intervensi { get; set; }
        public string Implementasi { get; set; }
        public string Evaluasi { get; set; }
        public string Petugas { get; set; }
        public string PetugasNama { get; set; }
        public string TTDPetugas { get; set; }
        public string ViewBag { get; set; }
        public string _METHOD { get; set; }
        public string Edit { get; set; }
        public bool saveTemplate { get; set; }
        public string templateName { get; set; }
        public string Jam_View { get; set; }
        public string Tanggal_View { get; set; }

        public string Template { get; set; }
        public bool save_template { get; set; }
        public string nama_template { get; set; }
        public string DokumenID { get; set; }
    }
}
