﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanPelayananPasienViewModel
    {
        public ListDetail<RuangPerawatanModelDetail> RuangPrwt_List { get; set; }
        public ListDetail<PemeriksaanPenunjangRIModelDetail> PmrksaanPenunjang_List { get; set; }
        public ListDetail<TindakanKedokteranRIModelDetail> TindakanKeprwtnPemeriksa_List { get; set; }
        public ListDetail<CatatanTambahanViewModelDetail> CttTmbhn_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string NamaPasien { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string TandaTanganPerawatYgMemulangkan { get; set; }
        public string TandaTanganPerawatYgMemulangkanNama { get; set; }
        public string TandaTanganApotik { get; set; }
        public string TandaTanganApotikNama { get; set; }
        public string TandaTanganVerivikator { get; set; }
        public string TandaTanganVerivikatorNama { get; set; }
        public string TandaTanganKasir { get; set; }
        public string TandaTanganKasirNama { get; set; }
        public string Username { get; set; }
        public int Nomor { get; set; }
        public int Report { get; set; }
    }
}