﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class RuangPerawatanModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Ruangan_RuangPerawatan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglJamAwal_RuangPerawatan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglJamAkhir_RuangPerawatan { get; set; }
        public string ParafVerivikator_RuangPerawatan { get; set; }
        public string ParafVerivikator_RuangPerawatanNama { get; set; }
        public string Username { get; set; }
    }
}
