﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class CatatanPemindahanPasienAntarRSViewModelDetail
    {

        public ListDetail<RecomendasiViewModelDetail> Rekom_List { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> PemindahanPasien_Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> PemindahanPasien_Jam { get; set; }
        public string Username { get; set; }
        public string PemindahanPasien_DariRS { get; set; }
        public string PemindahanPasien_KeRS { get; set; }
        public string ContactPerson { get; set; }
        public string NoHp { get; set; }
        public string DokterMerawat { get; set; }
        public string DokterMerawatNama { get; set; }
        public string DiagnosaMedis { get; set; }
        public string ProcedurPembedahan { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> ProcedurPembedahan_Tanggal { get; set; }
        public string MasalahKeperawatan { get; set; }
        public string KondisiPasien { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergi_Ket { get; set; }
        public string RiwayatRekasi { get; set; }
        public string Keperawatan { get; set; }
        public string HasilIntervensiAbnormal { get; set; }
        public string Kewaspadaan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> ObservasiPukul { get; set; }
        public string TingkatKesadaran { get; set; }
        public string GCS_E { get; set; }
        public string GCS_V { get; set; }
        public string GCS_M { get; set; }
        public string PupilKanan { get; set; }
        public string PupilKiri { get; set; }
        public string TD { get; set; }
        public string Nadi { get; set; }
        public string Nadi_TeraturTidak { get; set; }
        public string RR { get; set; }
        public string Suhu { get; set; }
        public string SkalaNyeri { get; set; }
        public bool Diet_Oral { get; set; }
        public bool Diet_NGT { get; set; }
        public bool Diet_Batasan { get; set; }
        public string Diet_Batasan_Ket { get; set; }
        public bool Diet_Khusus { get; set; }
        public string Diet_Khusus_Ket { get; set; }
        public bool BAB_Normal { get; set; }
        public bool BAB_Heustomy { get; set; }
        public bool BAB_Inkontinensia { get; set; }
        public bool BAK_Normal { get; set; }
        public bool BAK_Inkontinensia { get; set; }
        public bool BAK_Kateter { get; set; }
        public string BAK_Kateter_Ket { get; set; }
        public string NoKateter { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglPemasangan { get; set; }
        public string AlatBantu { get; set; }
        public string AlatBantu_Ket { get; set; }
        public string Infus { get; set; }
        public string InfusLokasi { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> InfusTanggalPemasangan { get; set; }
        public string RencanaPemeriksaan { get; set; }
        public string RencanaTindakanLanjutan { get; set; }
        public string HasilLab { get; set; }
        public string HasilUSG { get; set; }
        public string HasilEKG { get; set; }
        public string FotoRontgen { get; set; }
        public string CTScan { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> JamKondisiPasien { get; set; }
        public string Alasan { get; set; }
        public string NamaDPJP { get; set; }
        public string NamaDPJPNama { get; set; }
        public bool VerifikasiDPJP { get; set; }
        public string NamaPerawat1 { get; set; }
        public string NamaPerawat1Nama { get; set; }
        public bool VerifikasiPerawat1 { get; set; }
        public string NamaPerawat2 { get; set; }
        public string NamaPerawat2Nama { get; set; }
        public bool VerifikasiPerawat2 { get; set; }
    }
}