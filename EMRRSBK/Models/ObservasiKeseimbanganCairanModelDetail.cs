﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class ObservasiKeseimbanganCairanModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Oral_Jenis { get; set; }
        public string Oral_Jumlah { get; set; }
        public string Enteral_Jenis { get; set; }
        public string Enteral_Jumlah { get; set; }
        public string Parenteral_Jenis { get; set; }
        public string Parenteral_Jumlah { get; set; }
        public string IntakePerShift { get; set; }
        public string Output_BAK { get; set; }
        public string Output_BAB { get; set; }
        public string Output_Muntah { get; set; }
        public string Output_NGT { get; set; }
        public string Output_Drain { get; set; }
        public string Output_LainLain { get; set; }
        public string OutputPerShift { get; set; }
        public string OutputTotal_Intake { get; set; }
        public string OutputTotal_Output { get; set; }
        public string Username { get; set; }
    }
}
