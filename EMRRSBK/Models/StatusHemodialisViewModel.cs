﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class StatusHemodialisViewModel
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Diagnosis { get; set; }
        public string MulaiHD { get; set; }
        public string CaraBayar { get; set; }
        public Nullable<System.DateTime> TanggalOperasi_1 { get; set; }
        public Nullable<System.DateTime> TanggalOperasi_2 { get; set; }
        public string Femoral { get; set; }
        public string RiwayatAlergi { get; set; }
        public string RiwayatAlergiAda { get; set; }
        public string TempatLahir { get; set; }
        public string Username { get; set; }

        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string NoTelp { get; set; }
        
   
        
        public int? Report { get; set; }
    }
}