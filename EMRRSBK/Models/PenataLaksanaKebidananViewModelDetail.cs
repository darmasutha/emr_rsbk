﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PenataLaksanaKebidananViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public string Username { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Profesi { get; set; }
        public string Penatalaksanaan { get; set; }
        public string Paraf { get; set; }
        public string ParafNama { get; set; }
        public string TTDDokter { get; set; }
        public int SudahRegDkt { get; set; }
        public string Viewbag { get; set; }
    }
}