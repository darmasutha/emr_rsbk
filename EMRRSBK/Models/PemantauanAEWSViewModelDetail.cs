﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PemantauanAEWSViewModelDetail 
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public int Nomor { get; set; }
        public int NomorReport { get; set; }
        public int NomorReport_Group { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string Respirasi { get; set; }
        public string Skala1 { get; set; }
        public string Skala2 { get; set; }
        public string Udara { get; set; }
        public string TD_Sistolik { get; set; } 
        public string Nadi { get; set; }
        public string Kesadaran { get; set; }
        public string Temperatur { get; set; }
        public string TotalAEWS { get; set; }
        public string RentangSkor { get; set; }
        public string Nama { get; set; }
        public string NamaNama { get; set; }
        public string Username { get; set; }
        public int Report { get; set; }
        public int SudahRegDkt { get; set; }
        public string Viewbag { get; set; }
        public string TTDDokter { get; set; }
    }
}