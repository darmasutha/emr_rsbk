﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PengkajianResikoJatuhDewasaViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string LA { get; set; }
        public string Usia { get; set; }
        public string Defisit { get; set; }
        public string Aktivitas { get; set; }
        public string RiwayatJatuh { get; set; }
        public string Kognisi { get; set; }
        public string PengobatanPengunaanAlat { get; set; }
        public string Mobilitas { get; set; }
        public string PolaBab { get; set; }
        public string Komorbiditas { get; set; }
        public string TotalSkor { get; set; }
        public string Keterangan { get; set; }
        public string NamaParaf { get; set; }
        public string NamaParafNama { get; set; }
        public string Username { get; set; }
    }
}