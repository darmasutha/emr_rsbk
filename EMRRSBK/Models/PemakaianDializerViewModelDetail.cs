﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMRRSBK.Models
{
    public class PemakaianDializerViewModelDetail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }

        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglPakai { get; set; }
        public string JenisDializer { get; set; }
        public string JenisDializerNama { get; set; }
        public string Baru { get; set; }
        public string Reuse { get; set; }
        public string VolumePriming { get; set; }
        public string Keterangan { get; set; }
        public string KodePetugas { get; set; }
        public string KodePetugasNama { get; set; }
        public string Username { get; set; }
    }
}