﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EMRRSBK.Startup))]
namespace EMRRSBK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
