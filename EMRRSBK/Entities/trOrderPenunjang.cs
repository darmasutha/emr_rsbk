//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class trOrderPenunjang
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Noreg { get; set; }
        public string NamaPasien { get; set; }
        public Nullable<bool> Hasil_Cito { get; set; }
        public Nullable<bool> Hasil_Rutin { get; set; }
        public string KodeICD { get; set; }
        public string DiagnosaIndikasi { get; set; }
        public string DokterID { get; set; }
        public string NamaVendor { get; set; }
        public string AlamatVendor { get; set; }
        public string TlpVendor { get; set; }
        public Nullable<System.DateTime> DiterimaTgl { get; set; }
        public Nullable<System.DateTime> JamSampling { get; set; }
        public Nullable<System.DateTime> JamBilling { get; set; }
        public string KodePetugas { get; set; }
        public string NamaPetugas { get; set; }
        public string PemeriksaanTambahan { get; set; }
        public Nullable<bool> Realisasi { get; set; }
        public string UserId { get; set; }
        public string SectionID { get; set; }
        public string SectionAsalID { get; set; }
        public bool Batal { get; set; }
        public Nullable<int> NomorDataREG { get; set; }
        public string KodeVendor { get; set; }
    }
}
