//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CatatanPemindahanPasienAntarRuangan
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string IndikasiPindah { get; set; }
        public Nullable<System.DateTime> Pemindahan_Tanggal { get; set; }
        public Nullable<System.DateTime> Pemindahan_Jam { get; set; }
        public string Pemindahan_DariRuang { get; set; }
        public string Pemindahan_KeRuang { get; set; }
        public string DokterRawat_1 { get; set; }
        public string DokterRawat_2 { get; set; }
        public string DokterRawat_3 { get; set; }
        public string DiagnosaMedis_1 { get; set; }
        public string DiagnosaMedis_2 { get; set; }
        public string DiagnosaMedis_3 { get; set; }
        public string PasienDijelaskan { get; set; }
        public string ProsedurPembedahan { get; set; }
        public Nullable<System.DateTime> ProsedurPembedahan_Tanggal { get; set; }
        public string MasalahKeperawatan { get; set; }
        public string RiawayatAlergi { get; set; }
        public string NamaObat { get; set; }
        public string RiwayatReaksi { get; set; }
        public string IntervensiMedik { get; set; }
        public string HasilInvestigasi { get; set; }
        public string Kewaspadaan { get; set; }
        public Nullable<System.DateTime> ObservasiTerakhir { get; set; }
        public string TingkatKesadaran { get; set; }
        public bool Depresi { get; set; }
        public bool Demensia { get; set; }
        public bool Confuse { get; set; }
        public string GCS_E { get; set; }
        public string GCS_V { get; set; }
        public string GCS_M { get; set; }
        public string Pupil_Kanan { get; set; }
        public string Pupil_Kiri { get; set; }
        public string TD { get; set; }
        public string N { get; set; }
        public string Teratur_TidakTeratur { get; set; }
        public string RR { get; set; }
        public string Suhu { get; set; }
        public string SkalaNyeri { get; set; }
        public bool Diet_Oral { get; set; }
        public bool Diet_NGT { get; set; }
        public bool Diet_BatasanCairan { get; set; }
        public string Diet_BatasanCairan_Ket { get; set; }
        public bool Diet_DietKhusus { get; set; }
        public string Diet_DietKhusus_Ket { get; set; }
        public bool BAB_Normal { get; set; }
        public bool BAB_Neustomy { get; set; }
        public bool BAB_Inkontinensia { get; set; }
        public bool BAK_Normal { get; set; }
        public bool BAK_Inkontinensia { get; set; }
        public bool BAK_Kateter { get; set; }
        public string BAK_Kateter_JenisKateter { get; set; }
        public string BAK_Kateter_NoKateter { get; set; }
        public Nullable<System.DateTime> TglPemasangan { get; set; }
        public bool Mobilisasi_Jalan { get; set; }
        public bool Mobilisasi_TirahBaring { get; set; }
        public bool Mobilisasi_Duduk { get; set; }
        public bool Transfer_Mandiri { get; set; }
        public bool Transfer_DibantuSebagian { get; set; }
        public bool Transfer_DibantuPenuh { get; set; }
        public bool AlatBantu_TanpaAlat { get; set; }
        public bool AlatBantu_GigiPalsu { get; set; }
        public bool AlatBantu_KacaMata { get; set; }
        public bool AlatBantu_AlatBantuDengar { get; set; }
        public bool AlatBantu_LainLain { get; set; }
        public string AlatBantu_LainLain_Ket { get; set; }
        public string Luka { get; set; }
        public string Luka_Kondisi { get; set; }
        public string Luka_Lokasi { get; set; }
        public string Luka_Ukuran { get; set; }
        public string InfusScore { get; set; }
        public Nullable<System.DateTime> InfusTanggal { get; set; }
        public bool Tindakan_ProtokolResiko { get; set; }
        public bool Tindakan_ProtokolRestrain { get; set; }
        public string PeralatanKhusus_1 { get; set; }
        public string LamaPenggunaan_1 { get; set; }
        public string PeralatanKhusus_2 { get; set; }
        public string LamaPenggunaan_2 { get; set; }
        public string PeralatanKhusus_3 { get; set; }
        public string LamaPenggunaan_3 { get; set; }
        public string HalIstimewa { get; set; }
        public string Konsultasi { get; set; }
        public string RencanaPemeriksaan { get; set; }
        public string Therapy { get; set; }
        public string PersiapanPulang { get; set; }
        public string RencanaTindakLanjut { get; set; }
        public string Obat { get; set; }
        public string MRI { get; set; }
        public string ECHO { get; set; }
        public string HasilLab { get; set; }
        public string MRA { get; set; }
        public string GigiPalsu { get; set; }
        public string FotoRontgen { get; set; }
        public string HasilUsg { get; set; }
        public string KacaMata { get; set; }
        public string CTScan { get; set; }
        public string HasilEKG { get; set; }
        public string AlatBantuDengar { get; set; }
        public string RekamMedisLama { get; set; }
        public string LainLain { get; set; }
        public Nullable<System.DateTime> JamSerahTerima { get; set; }
        public string Diserahkan_Perawat { get; set; }
        public string Diterima_Perawat { get; set; }
        public string Username { get; set; }
        public byte[] TandaTanganPerawatSerah { get; set; }
        public byte[] TandaTanganPerawatTerima { get; set; }
        public byte[] TandaTangan { get; set; }
    }
}
