//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RencanaPemulanganPasien
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string DiagnosaMedis { get; set; }
        public Nullable<System.DateTime> TanggalMRS { get; set; }
        public Nullable<System.DateTime> JamMRS { get; set; }
        public string AlasanMRS { get; set; }
        public Nullable<System.DateTime> TanggalPerencanaan { get; set; }
        public Nullable<System.DateTime> JamPerencanaan { get; set; }
        public Nullable<System.DateTime> EstimasiTanggalPulang { get; set; }
        public string KodePerawat { get; set; }
        public string PasienDanKeluarga { get; set; }
        public string Pekerjaan { get; set; }
        public string Keuangan { get; set; }
        public string AntisipasiMasalahPulang { get; set; }
        public string AntisipasiMasalahPulang_Ket { get; set; }
        public bool Bantuan_MinumObat { get; set; }
        public bool Bantuan_Mandi { get; set; }
        public bool Bantuan_Makan { get; set; }
        public bool Bantuan_Diet { get; set; }
        public bool Bantuan_MenyiapkanMakanan { get; set; }
        public bool Bantuan_Berpakaian { get; set; }
        public bool Bantuan_EdukasiKesehatan { get; set; }
        public bool Bantuan_Transportasi { get; set; }
        public string AdakahYangBantu { get; set; }
        public string AdakahYangBantu_Ket { get; set; }
        public string ApakahPasienHidup { get; set; }
        public string ApakahPasienHidup_Ket { get; set; }
        public string PasienMenggunakanAlatMedis { get; set; }
        public string PasienMenggunakanAlatMedis_Ket { get; set; }
        public string PasienPerluAlat { get; set; }
        public string PasienPerluAlat_Ket { get; set; }
        public string ApakahPerluBantuan { get; set; }
        public string ApakahPerluBantuan_Ket { get; set; }
        public string PasienBermasalah { get; set; }
        public string PasienBermasalah_Ket { get; set; }
        public string PasienMemilikiNyeri { get; set; }
        public string PasienMemilikiNyeri_Ket { get; set; }
        public string PasienPerluEdukasi { get; set; }
        public string PasienPerluEdukasi_Ket { get; set; }
        public string PasienPerluKeterampilan { get; set; }
        public string PasienPerluKeterampilan_Ket { get; set; }
        public string Username { get; set; }
    }
}
