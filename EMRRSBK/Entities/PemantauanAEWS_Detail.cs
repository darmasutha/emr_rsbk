//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PemantauanAEWS_Detail
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public int No { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string Respirasi { get; set; }
        public string Skala1 { get; set; }
        public string Skala2 { get; set; }
        public string Udara { get; set; }
        public string TD_Sistolik { get; set; }
        public string Nadi { get; set; }
        public string Kesadaran { get; set; }
        public string Temperatur { get; set; }
        public string TotalAEWS { get; set; }
        public string Nama { get; set; }
        public string RentangSkor { get; set; }
        public Nullable<int> NomorReport { get; set; }
        public Nullable<int> NomorReport_Group { get; set; }
        public string Username { get; set; }
        public byte[] TTDDokter { get; set; }
    }
}
