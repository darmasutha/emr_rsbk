//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class RujukanPenderita
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Yth { get; set; }
        public string Di { get; set; }
        public string Pekerjaan { get; set; }
        public string Diagnosa_Kerja { get; set; }
        public string Riwayat_Pekerjaan { get; set; }
        public string Perawatan { get; set; }
        public Nullable<System.DateTime> TanggalPengirim { get; set; }
        public string NamaPengirim { get; set; }
        public string Username { get; set; }
    }
}
