//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AsesmenResumeMedis
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Tanggal_MRS { get; set; }
        public Nullable<System.DateTime> Tanggal_KLR { get; set; }
        public string KeluhanUtama { get; set; }
        public string AlasanMRS { get; set; }
        public string RiwayatAlergi { get; set; }
        public string KesanUmum { get; set; }
        public string TandaVital_TD { get; set; }
        public string TandaVital_Nadi { get; set; }
        public string TandaVital_RR { get; set; }
        public string TandaVital_Suhu { get; set; }
        public string TandaVital_GCS_E { get; set; }
        public string TandaVital_GCS_V { get; set; }
        public string TandaVital_GCS_M { get; set; }
        public string Investigasi { get; set; }
        public string Diagnosisi_Utama { get; set; }
        public string Diagnosisi_Sekunder { get; set; }
        public string Diagnosisi_Komplikasi { get; set; }
        public string Tindakan { get; set; }
        public string Terapi_MRS { get; set; }
        public string Terapi_SaatIni { get; set; }
        public string PerkembanganPenyakit { get; set; }
        public string KondisiKeluar { get; set; }
        public string Prognosis_Vitality { get; set; }
        public string Prognosis_Functionally { get; set; }
        public string Prognosis_Recoverably { get; set; }
        public string PenyebabKematian { get; set; }
        public bool Rekomendasi_BolehPulang { get; set; }
        public bool Rekomendasi_PulangPaksa { get; set; }
        public bool Rekomendasi_Dirujuk { get; set; }
        public string Rekomendasi_Dirujuk_Ket { get; set; }
        public bool Dokter1 { get; set; }
        public string Dokter1_Ket { get; set; }
        public Nullable<System.DateTime> Dokter1_Kontrol { get; set; }
        public string Dokter1_Tempat { get; set; }
        public bool Dokter2 { get; set; }
        public string Dokter2_Ket { get; set; }
        public Nullable<System.DateTime> Dokter2_Kontrol { get; set; }
        public string Dokter2_Tempat { get; set; }
        public bool Obat { get; set; }
        public string Obat_Ket { get; set; }
        public string DokterRawat { get; set; }
        public bool ValidasiDokter { get; set; }
        public string Username { get; set; }
        public byte[] TandaTanganDokter { get; set; }
        public byte[] TandaTangan { get; set; }
    }
}
