//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SuratKeteranganDokter
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tgl { get; set; }
        public string NoPol { get; set; }
        public string Pangkat { get; set; }
        public string Kesatuan { get; set; }
        public string PadaWaktuDiperiksa { get; set; }
        public string PadaWaktuDiperiksa_Untuk { get; set; }
        public string TinggiBadan { get; set; }
        public string BeratBadan { get; set; }
        public string PemeriksaanSinarTembus { get; set; }
        public string PemeriksaanLaboratorium { get; set; }
        public string GolonganDarah { get; set; }
        public string ButaWarna { get; set; }
        public Nullable<System.DateTime> Tgl_SuratKeteranganDokter { get; set; }
        public string Dokter { get; set; }
        public string Username { get; set; }
        public byte[] TandaTanganDokter { get; set; }
        public byte[] TandaTangan { get; set; }
    }
}
