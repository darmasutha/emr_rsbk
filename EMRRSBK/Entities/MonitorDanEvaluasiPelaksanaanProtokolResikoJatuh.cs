//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMRRSBK.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitorDanEvaluasiPelaksanaanProtokolResikoJatuh
    {
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public int No { get; set; }
        public string NRM { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string StandarResikoRendah_1 { get; set; }
        public string StandarResikoRendah_2 { get; set; }
        public string StandarResikoRendah_3 { get; set; }
        public string StandarResikoRendah_4 { get; set; }
        public string StandarResikoRendah_5 { get; set; }
        public string StandarResikoRendah_6 { get; set; }
        public string StandarResikoRendah_7 { get; set; }
        public string StandarResikoRendah_8 { get; set; }
        public string StandarResikoRendah_9 { get; set; }
        public string StandarResikoRendah_10 { get; set; }
        public string ResikoJatuhTinggi_1 { get; set; }
        public string ResikoJatuhTinggi_2 { get; set; }
        public string ResikoJatuhTinggi_3 { get; set; }
        public string ResikoJatuhTinggi_4 { get; set; }
        public string ResikoJatuhTinggi_5 { get; set; }
        public string ResikoJatuhTinggi_6 { get; set; }
        public string ResikoJatuhTinggi_7 { get; set; }
        public string ResikoJatuhTinggi_8 { get; set; }
        public string ResikoJatuhSangatTinggi_1 { get; set; }
        public string ResikoJatuhSangatTinggi_2 { get; set; }
        public string ResikoJatuhSangatTinggi_3 { get; set; }
        public string ResikoJatuhSangatTinggi_4 { get; set; }
        public string ResikoJatuhSangatTinggi_5 { get; set; }
        public string Evaluasi { get; set; }
        public string Petugas { get; set; }
        public string Username { get; set; }
    }
}
