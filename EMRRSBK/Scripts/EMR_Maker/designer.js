﻿var reportinput = [];
var editing = false;
var saved = false;
var activeproperty;
var reportid;
var lookupactive;
var lookuptargetid;
var defaultvalue;
var nextreport = null;
var reportnobukti = '';
var manualjavascript;

document.addEventListener("DOMContentLoaded", function (event) {
    dragElement();
});

function dragElement() {
    $.each($('.report-input.report-input-new'), function (index, value) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        value.onmousedown = dragMouseDown;
        value.onclick = function (e) {
            activeproperty = $(value).find('[id][name]').attr('id');
            $('#labelactiveproperty').html(activeproperty);
        };

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            var top = value.offsetTop - pos2;
            var left = value.offsetLeft - pos1;
            if (top < 0) top = 0;
            if (left < 0) left = 0;
            if (top > reporteditor.offsetHeight - value.offsetHeight) top = reporteditor.offsetHeight - value.offsetHeight;
            if (left > reporteditor.offsetWidth - value.offsetWidth) left = reporteditor.offsetWidth - value.offsetWidth;
            $(value).css('top', top + "px");
            $(value).css('left', left + "px");
        }

        function closeDragElement() {
            document.onmouseup = null;
            document.onmousemove = null;
        }

        $(value).removeClass('report-input-new');
    });
}

//============================================================================ D E S I G N

function btnhomeonclick(url) {
    if (editing && !saved) { if (!confirm("Report belum disimpan, Yakin akan melanjutkan?")) { return; } }
    window.location = url;
}

function btndeletepropertyonclick() {
    if ((activeproperty || '') === '') { alert('Tidak ada property yang aktif'); return; }

    if (confirm('Yakin akan menghapus ' + activeproperty)) {
        $('#' + activeproperty).parent('.report-input').remove();
        activeproperty = '';
        $('#labelactiveproperty').html('');
    }
}

function btnprepertiesonclick() {
    if ((activeproperty || '') === '') { alert('Tidak ada property yang aktif'); return; }
    var input = $('#' + activeproperty);
    var header = $(input.closest('.report-input'));
    var inputtype = gettypeinput(input[0]);

    $('#propertiesvalue').closest('.form-group').addClass('hidden');
    $('#propertiesrequired').closest('.form-group').addClass('hidden');
    $('#propertiesreadonly').closest('.form-group').addClass('hidden');
    $('#propertieshidden').closest('.form-group').addClass('hidden');
    $('#propertiestop').closest('.form-group').addClass('hidden');
    $('#propertiesleft').closest('.form-group').addClass('hidden');
    $('#propertiesheight').closest('.form-group').addClass('hidden');
    $('#propertieswidth').closest('.form-group').addClass('hidden');
    $('#propertiescolor').closest('.form-group').addClass('hidden');
    $('#propertiesfontsize').closest('.form-group').addClass('hidden');
    $('#propertiespadding').closest('.form-group').addClass('hidden');
    $('#propertiestextalign').closest('.form-group').addClass('hidden');
    $('#propertiescustomvalue').addClass('hidden');
    $('#propertiescustomvalueoption').addClass('hidden');

    if (inputtype === 'IMAGE') {
        $('#propertiesheight').closest('.form-group').removeClass('hidden');
        $('#propertieswidth').closest('.form-group').removeClass('hidden');

        $('#propertiesid').val(input.attr('id'));
        $('#propertiesname').val(input.attr('name'));
        var reportinput = $(input).closest('.report-input');
        var canvas = reportinput.find('canvas');
        $('#propertiesheight').val(reportinput.css('height'));
        $('#propertieswidth').val(reportinput.css('width'));

        $('#modalproperties').modal();
    } else {
        $('#propertiesvalue').closest('.form-group').removeClass('hidden');
        $('#propertiesrequired').closest('.form-group').removeClass('hidden');
        $('#propertiesreadonly').closest('.form-group').removeClass('hidden');
        $('#propertieshidden').closest('.form-group').removeClass('hidden');
        $('#propertiestop').closest('.form-group').removeClass('hidden');
        $('#propertiesleft').closest('.form-group').removeClass('hidden');
        $('#propertiesheight').closest('.form-group').removeClass('hidden');
        $('#propertieswidth').closest('.form-group').removeClass('hidden');
        $('#propertiescolor').closest('.form-group').removeClass('hidden');
        $('#propertiesfontsize').closest('.form-group').removeClass('hidden');
        $('#propertiespadding').closest('.form-group').removeClass('hidden');
        $('#propertiestextalign').closest('.form-group').removeClass('hidden');

        $('#propertiesid').val(input.attr('id'));
        $('#propertiesname').val(input.attr('name'));
        $('#propertiesvalue').val(input.attr('value'));
        $('#propertiesrequired')[0].checked = input.attr('required') === 'required';
        $('#propertiesreadonly')[0].checked = inputtype === 'CHECKBOX' || inputtype === 'RADIO' ? input.attr('disabled') === 'disabled' : input.attr('readonly') === 'readonly';
        $('#propertieshidden')[0].checked = $(input).closest('.report-input').css('display') === 'none';
        $('#propertiestop').val(header.css('top'));
        $('#propertiesleft').val(header.css('left'));
        $('#propertiesheight').val(input.css('height'));
        $('#propertieswidth').val(input.css('width'));
        $('#propertiescolor').val(input.css('color'));
        $('#propertiesfontsize').val(input.css('font-size'));
        $('#propertiespadding').val(input.css('padding'));
        $('#propertiestextalign').val(input.css('text-align'));

        if (inputtype === 'TEXT' || inputtype === 'NUMBER' || inputtype === 'DATE' || inputtype === 'TIME') {
            $('#propertiescustomvalue .properties-div-value').html('<input type="text" class="form-control" id="propertiescustomvaluetemp" value="' + input.val() + '"/>');
            $('#propertiescustomvalue').removeClass('hidden');
        } else if (inputtype === 'CHECKBOX' || inputtype === 'RADIO') {
            $('#propertiescustomvalue .properties-div-value').html('<input type="checkbox" id="propertiescustomvaluetemp" ' + (input.attr('checked') || '') + '/>');
            $('#propertiescustomvalue').removeClass('hidden');
        } else if (inputtype === 'SELECT') {
            $('#propertiescustomvalueoption .control-label').html('Options');
            var htmlpropertisoption = '';
            htmlpropertisoption += '<div class="input-group">';
            htmlpropertisoption += '<input type="text" class="form-control" placeholder="Value" style="width:50%;">';
            htmlpropertisoption += '<input type="text" class="form-control" placeholder="Text" style="width:50%;">';
            htmlpropertisoption += '<div class="input-group-btn">';
            htmlpropertisoption += '<button type="button" class="btn btn-default" onclick="addpropertiesoptionselect(this)">Add</button>';
            htmlpropertisoption += '<button type="button" class="btn btn-default" onclick="removepropertiesoptionselect(this)">Remove</button>';
            htmlpropertisoption += '</div>';
            htmlpropertisoption += '</div>';
            $('#propertiescustomvalueoption .properties-div-value').html(htmlpropertisoption);
            $('#propertiescustomvalue .properties-div-value').html('<select id="propertiescustomvaluetemp" class="form-control">' + input.html() + '</select>');
            $('#propertiescustomvalueoption').removeClass('hidden');
            $('#propertiescustomvalue').removeClass('hidden');
        } else if (inputtype === 'TEXTAREA') {
            $('#propertiescustomvalue .properties-div-value').html('<textarea id="propertiescustomvaluetemp" class="form-control">' + input.val() + '</textarea>');
            $('#propertiescustomvalue').removeClass('hidden');
        }
        $('#modalproperties').modal();
    }
}

function modalpropertiesonsubmit(t, e) {
    e.preventDefault();
    var input = $('#' + activeproperty);
    var inputtype = gettypeinput(input[0]);

    input.attr('id', $('#propertiesid').val());
    input.attr('name', $('#propertiesname').val());

    if (inputtype === 'IMAGE') {
        var reportinput = $(input).closest('.report-input');
        var canvas = reportinput.find('canvas');
        reportinput.css('height', $('#propertiesheight').val());
        reportinput.css('width', $('#propertieswidth').val());
        canvas.css('height', $('#propertiesheight').val());
        canvas.css('width', $('#propertieswidth').val());
    } else {
        if ($('#propertiesrequired')[0].checked)
            input.attr('required', 'required');
        else
            input.removeAttr('required');

        if ($('#propertiesreadonly')[0].checked) {
            if (inputtype === 'CHECKBOX' || inputtype === 'RADIO')
                input.attr('disabled', 'disabled');
            else
                input.attr('readonly', 'readonly');
        } else {
            input.removeAttr((inputtype === 'CHECKBOX' || inputtype === 'RADIO') ? 'disabled' : 'readonly');
        }

        input.closest('.report-input').css('display', $('#propertieshidden')[0].checked ? 'none' : 'inline-block');

        input.css('height', $('#propertiesheight').val());
        input.css('width', $('#propertieswidth').val());
        input.css('color', $('#propertiescolor').val());
        input.css('font-size', $('#propertiesfontsize').val());
        input.css('padding', $('#propertiespadding').val());
        input.css('text-align', $('#propertiestextalign').val());

        if (inputtype === 'TEXT' || inputtype === 'NUMBER' || inputtype === 'DATE' || inputtype === 'TIME') {
            input.attr('value', $('#propertiescustomvaluetemp').val());
        } else if (inputtype === 'CHECKBOX' || inputtype === 'RADIO') {
            if ($('#propertiescustomvaluetemp')[0].checked) {
                input.attr('checked', 'checked');
            } else {
                input.removeAttr('checked');
            }
        } else if (inputtype === 'SELECT') {
            input.html($('#propertiescustomvaluetemp').html());
            input.find('option').removeAttr('selected');
            $(input.find('option')[$('#propertiescustomvaluetemp')[0].options.selectedIndex]).attr('selected', 'selected');
        } else if (inputtype === 'TEXTAREA') {
            input.html($('#propertiescustomvaluetemp').val());
        }
    }
    $('#modalproperties').modal('hide');
}

function addpropertiesoptionselect(t) {
    var val = $(t).closest('.input-group').find('[placeholder="Value"]');
    var text = $(t).closest('.input-group').find('[placeholder="Text"]');
    if (val.val() === '' || text.val() === '') { alert('data harus lengkap'); return; }
    if ($('#propertiescustomvaluetemp option[value="' + val.val() + '"]').length > 0) { alert('id sudah digunakan'); return; }
    $('#propertiescustomvaluetemp').append('<option value="' + val.val() + '">' + text.val() + '</option>');
    val.val('');
    text.val('');
}

function removepropertiesoptionselect(t) {
    var val = $(t).closest('.input-group').find('[placeholder="Value"]');
    var option = $('#propertiescustomvaluetemp option[value="' + val.val() + '"]');
    if (option.length === 0) { alert('id tidak ditemukan'); return; }
    option.remove();
    val.val('');
}

function btndesignsaveonclick(t) {
    if (!editing) { alert('Tidak ada report yang di buka'); return; }
    $(t).attr('disabled', 'disabled');
    var datas = getreporteditordata();
    $.ajax({
        url: urlsave,
        method: 'POST',
        data: {
            id: reportid,
            datas: datas,
            javascript: manualjavascript
        },
        beforeSend: function () { },
        success: function (r) {
            r = JSON.parse(r);
            if (r.IsSuccess) {
                saved = true;
            } else {
                alert(r.Message);
            }
        },
        error: function (xhr) { alert(xhr.statusText); },
        complete: function () {
            $(t).removeAttr('disabled');
        }
    });
}

function btnaddpropertyonclick() {
    if (!editing) { alert('Tidak ada report yang di buka'); return; }
    $('#modaladdproperty').modal();
}

function modalnewonsubmit(t, e) {
    e.preventDefault();

    if (editing && !saved) {
        if (!confirm("Report belum disimpan, Yakin akan melanjutkan?")) { return; }
    }
    if (window.FormData === undefined) { alert("FormData is not supported."); return; }

    var formData = new FormData();
    formData.append('Group', $(t).find('#newgroupreport option:selected').val());
    formData.append('Kode', $(t).find('#newkodereport').val());
    formData.append('Kertas', $(t).find('#newkertas option:selected').val());
    formData.append('Halaman', $(t).find('#newhalaman').val());
    formData.append('Landscape', $(t).find('#newlandscape')[0].checked);

    var image = $(t).find('#newgambar')[0].files[0];
    formData.append(image.name, image);
    $(t).find('[type="submit"]').attr('disabled');
    $.ajax({
        url: urlnew,
        method: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () { },
        success: function (r) {
            r = JSON.parse(r);
            if (r.IsSuccess) {
                $('#modalnew').modal('hide');
                manualjavascript = '';
                $('#modalopen').find('#opengroupreport option').removeAttr('selected');
                $('#modalopen').find('#opengroupreport option[value="' + $(t).find('#newgroupreport option:selected').val() + '"]').attr('selected', 'selected');
                $('#modalopen').find('#openkodereport').val($(t).find('#newkodereport').val());
                $('#modalopen').find('#openhalaman').val($(t).find('#newhalaman').val());
                $('#modalopen form').submit();
            } else {
                alert(r.Message);
            }
        },
        error: function (xhr) { alert(xhr.statusText); },
        complete: function () {
            $(t).find('[type="submit"]').removeAttr('disabled');
        }
    });
}

function modalopenonsubmit(t, e) {
    e.preventDefault();
    $(t).find('[type="submit"]').attr('disabled');
    $.ajax({
        url: urlopen,
        method: 'POST',
        data: {
            group: parseInt($(t).find('#opengroupreport').val()),
            kode: $(t).find('#openkodereport').val(),
            halaman: parseInt($(t).find('#openhalaman').val())
        },
        beforeSend: function () { },
        success: function (r) {
            r = JSON.parse(r);
            if (r.IsSuccess) {
                startediting(r);
                reportid = r.Header.Id;
                manualjavascript = r.Header.JavaScript;
                $('#modalopen').modal('hide');
            } else {
                alert(r.Message);
            }
        },
        error: function (xhr) { alert(xhr.statusText); },
        complete: function () {
            $(t).find('[type="submit"]').removeAttr('disabled');
        }
    });
}

function modaladdpropertyonsubmit(t, e) {
    e.preventDefault();
    var type = t['addpropertytype'].value;
    var id = t['addpropertyid'].value.trim();
    var required = t['addpropertyrequired'].checked;
    var readonly = t['addpropertyreadonly'].checked;
    var hidden = t['addpropertyhidden'].checked;
    var height = t['addpropertyheight'].value;
    var width = t['addpropertywidth'].value;
    var textalign = t['addpropertytextalign'].value;
    var fontsize = t['addpropertyfontsize'].value;
    var color = t['addpropertycolor'].value;
    var padding = t['addpropertypadding'].value;
    if (id === '') { alert('Id harus diisi'); return; }
    if ($('#' + id).length > 0) { alert('Id sudah digunakan'); return false; }
    $('#reporteditor').append(generadereportinput(id, type, {
        required: required,
        readonly: readonly,
        hidden: hidden,
        height: height,
        width: width,
        textalign: textalign,
        fontsize: fontsize,
        color: color,
        padding: padding
    }));
    activereportinput();
    dragElement();
    $('#modaladdproperty').modal('hide');
}

function startediting(data) {
    editing = true;
    $('#labelopenkode').html("report : " + data.Header.KodeReport);
    $('#labelopenhalaman').html("halaman : " + data.Header.Halaman);
    var reporteditor = $('#reporteditor');
    reporteditor.attr('class', '');
    reporteditor.addClass('report-div');
    reporteditor.addClass('report-' + data.Header.Kertas + (data.Header.Landscape ? '-landscape' : ''));
    reporteditor.html('');
    reporteditor.append('<div class="report-back-image report-back-image-' + data.Header.Kertas + (data.Header.Landscape ? '-landscape' : '') + '" style="background-image:url(' + urlcontentreport + '/' + data.Header.LokasiGambar + ')"></div>');
    $.each(data.Detail, function (index, value) {
        var html = '<div class="report-input report-input-new" style="' + value.Style + '">';
        html += value.Html;
        html += '</div>';
        reporteditor.append(html);
    });
    dragElement();
}

function btnhtmlonclick() {
    if ((activeproperty || '') === '') { alert('Tidak ada property yang aktif'); return; }
    var input = $('#' + activeproperty).closest('.report-input');
    $('#modalhtml .modal-body textarea').val(input.html());
    $('#modalhtml').modal();
}

function modalhtmlonsubmit(t, e) {
    e.preventDefault();
    var input = $('#' + activeproperty).closest('.report-input');
    input.html($(t).find('textarea').val());
    $('#modalhtml').modal('hide');
}

function btnjavascriptonclick() {
    if (!editing) { alert('Tidak ada report yang di buka'); return; }
    $('#modaljavascript textarea').val(manualjavascript);
    $('#modaljavascript').modal();
}

function modaljavascriptonsubmit(t, e) {
    e.preventDefault();
    manualjavascript = $(t).find('textarea').val();
    $('#modaljavascript').modal('hide');
}

function btnshowallonclick() {
    if (!editing) { alert('Tidak ada report yang di buka'); return; }
    var r = '';
    $.each($('#reporteditor .report-input [id][name]'), function (index, value) {
        r += '<tr onclick="showalldetailonclick(this, \'' + value.id + '\')">';
        r += '<td>' + value.id + '</td>';
        r += '<td style="width:60px;">';
        r += '<button type="button" onclick="propertyswitch(this, \'' + value.id + '\', \'UP\')" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-arrow-up"></i></button>&nbsp;';
        r += '<button type="button" onclick="propertyswitch(this, \'' + value.id + '\', \'DOWN\')" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-arrow-down"></i></button></td>';
        r += '</tr>';
    });
    $('#modalshowall table tbody').html(r);
    $('#modalshowall').modal();
}

function showalldetailonclick(t, id) {
    var jt = $(t);
    if (jt.hasClass('success')) {
        activeproperty = id;
        $('#labelactiveproperty').html(activeproperty);
        $('#modalshowall').modal('hide');
    } else {
        $(t).closest('tbody').find('.success').removeClass('success');
        $(t).addClass('success');
    }
}

function propertyswitch(t, id, type) {
    var inp = $('#reporteditor #' + id).closest('.report-input');
    if (type === 'UP') {
        var prv = inp.prev();
        if (prv.hasClass('report-input')) {
            inp.insertBefore(prv);
            $(t).closest('tr').insertBefore($(t).closest('tr').prev());
        }
    } else if (type === 'DOWN') {
        var nxt = inp.next();
        if (nxt.hasClass('report-input')) {
            inp.insertAfter(nxt);
            $(t).closest('tr').insertAfter($(t).closest('tr').next());
        }
    }
}

//============================================================================ H E L P E R

function gettypeinput(value) {
    var rinput = reportinput.find(x => x.id === value.id);
    if (rinput)
        return rinput.type;
    else {
        var r = '';
        if (value.tagName === 'INPUT') {
            if (value.type.toUpperCase() === 'TEXT') r = 'TEXT';
            else if (value.type.toUpperCase() === 'NUMBER') r = 'NUMBER';
            else if (value.type.toUpperCase() === 'DATE') r = 'DATE';
            else if (value.type.toUpperCase() === 'TIME') r = 'TIME';
            else if (value.type.toUpperCase() === 'CHECKBOX') r = 'CHECKBOX';
            else if (value.type.toUpperCase() === 'RADIO') r = 'RADIO';
        } else if (value.tagName === 'TEXTAREA') r = 'TEXTAREA';
        else if (value.tagName === 'SELECT') r = 'SELECT';
        return r;
    }
}

function setinputvalue(inputid, value, hiddenvalue) {
    var input = $('#' + inputid);
    if (input.length > 0) {
        var type = gettypeinput(input[0]);
        if (type === 'TEXT' || type === 'NUMBER' || type === 'DATE' || type === 'TIME' || type === 'TEXTAREA') {
            input.val(value);
        } else if (type === 'CHECKBOX' || type === 'RADIO') {
            input[0].checked = value === 'true';
        } else if (type === 'SELECT') {
            input.find('option').removeAttr('selected');
            input.find('option[value="' + value + '"]').attr('selected', 'selected');
        } else if (type === 'IMAGE') {
            input[0].src = 'data:image/png;base64,' + value;
        } else if (type === 'NRM' || type === 'PERAWAT' || type === 'DOKTER' || type === 'SECTION' || type === 'BARANG') {
            input.val(value);
            reportinput.find(x => x.id === inputid).hiddenvalue = hiddenvalue;
        }
    }
}

function generadereportinput(id, type, option = {}, editor = true) {
    if (id === '') { alert('Id harus diisi'); return; }
    if ($('#' + id).length > 0) { alert('Id sudah digunakan'); return false; }
    var r = '';

    if (type === 'IMAGE') {
        r += '<div style="width:300px; height:200px; border:solid 1px black;" class="report-input report-type-image ' + (editor ? 'report-input-new' : '') + '">';
        r += '<img id="' + id + '" name="' + id + '" style="display:none" class="report-image-img" />';
        r += '<canvas style="width:300px; height:200px;"></canvas>';
        r += '<input type="number" class="report-image-fontsize" placeholder="font-size" style="width:80px;" />';
        r += '<input type="text" class="report-image-text" placeholder="text" style="width:calc(100% - 192px);" />';
        r += '<button type="button" style="width:50px;" onclick="reportinput.find(x => x.id == \'' + id + '\').model.text(this);">add</button>';
        r += '<button type="button" style="width:50px;" onclick="reportinput.find(x => x.id == \'' + id + '\').model.clear();">clear</button>';
        // script
        r += '<script> reportinput.push({ id: \'' + id + '\', type: \'IMAGE\', ximage : new Image() }); </script>';
        r += '</div>';
    } else {
        var attr = 'id="' + id + '" name="' + id + '" ' + (option.required ? 'required' : '') + ' ' + (option.readonly ? 'readonly' : '');
        var style = 'height:' + option.height + '; width:' + option.width + '; text-align:' + option.textalign + '; font-size:' + option.fontsize + '; color:' + option.color + '; padding:' + option.padding + ';';
        r += '<div class="report-input ' + (editor ? 'report-input-new' : '') + '" style="' + (option.hidden ? 'display:none;' : 'display:inline-block') + '">';
        if (type === 'TEXT') {
            r += '<input type="text" ' + attr + ' style="' + style + '"/>';
        } else if (type === 'NUMBER') {
            r += '<input type="number" step="any" ' + attr + ' style="' + style + '"/>';
        } else if (type === 'DATE') {
            r += '<input type="date" ' + attr + ' style="' + style + '"/>';
        } else if (type === 'TIME') {
            r += '<input type="time" ' + attr + ' style="' + style + '"/>';
        } else if (type === 'TEXTAREA') {
            r += '<textarea ' + attr + ' style="resize: none;' + style + '"></textarea>';
        } else if (type === 'CHECKBOX') {
            r += '<input type="checkbox" ' + attr + ' style="' + style + '"/>';
        } else if (type === 'RADIO') {
            r += '<input type="radio" ' + attr + ' style="' + style + '"/>';
        } else if (type === 'SELECT') {
            r += '<select ' + attr + ' style="' + style + '"></select>';
        } else if (type === 'NRM' || type === 'PERAWAT' || type === 'DOKTER' || type === 'SECTION' || type === 'BARANG') {
            r += '<textarea ' + attr + ' style="resize: none;' + style + '"></textarea>';
            r += '<button type="button" onclick="openmodallookup(\'' + type + '\', \'' + id + '\')" style="position: inherit;">..</button>';
            r += '<script> reportinput.push({ id: \'' + id + '\', type: \'' + type + '\' }); </script>';
        }
        r += '</div>';
    }
    return r;
}

function generatefootertable(totalcount, pagesize, currentpage, functionname) {
    var startdata;
    if (totalcount === 0) startdata = 0;
    else startdata = currentpage * pagesize + 1;
    var maxpage = Math.ceil(totalcount / pagesize);
    var enddata = startdata + pagesize - 1;
    if (enddata > totalcount) enddata = totalcount;
    var datainfo = 'Data ' + startdata + '-' + enddata +
        ' of ' + totalcount +
        ' Page (' + (totalcount === 0 ? 0 : currentpage + 1) + '/' + maxpage + ')';

    var pagination = "";
    if (totalcount > 0) {
        var start_page = currentpage > 0 ? currentpage - 1 : 0;
        var end_page = currentpage + 1;
        if (currentpage === 0) end_page++;
        end_page = end_page > maxpage - 1 ? maxpage - 1 : end_page;
        if (currentpage >= end_page && start_page > 0) start_page--;
        var disable = ' class="disabled"';
        var string_page = '<li' + (currentpage === 0 ? disable : '') + '><a' + (currentpage === 0 ? '' : ' onclick="' + functionname + '({ page : 0 })"') + '><span>&laquo;</span></a></li>';
        string_page += '<li' + (currentpage === 0 ? disable : '') + '><a' + (currentpage === 0 ? '' : ' onclick="' + functionname + '({ page : ' + (currentpage - 1) + '})"') + '><span>&lsaquo;</span></a></li>';
        if (start_page > 0)
            string_page += '<li><a onclick="' + functionname + '({ page : ' + (start_page - 1) + '})">..</a></li>';
        for (var i = start_page; i <= end_page; i++) {
            var active = i === currentpage;
            string_page += '<li' + (active ? ' class="active"' : '') + '><a' + (active ? '' : ' onclick=' + functionname + '({page:' + i + '})') + '>' + (i + 1) + '</a></li>';
        }
        if (end_page + 1 < maxpage)
            string_page += '<li><a onclick="' + functionname + '({ page : ' + (end_page + 1) + '})">..</a></li>';
        string_page += '<li' + (currentpage + 1 === maxpage ? disable : '') + '><a' + (currentpage + 1 === maxpage ? '' : ' onclick="' + functionname + '({ page : ' + (currentpage + 1) + '})"') + '><span>&rsaquo;</span></a></li>';
        string_page += '<li' + (currentpage + 1 === maxpage ? disable : '') + '><a' + (currentpage + 1 === maxpage ? '' : ' onclick="' + functionname + '({ page : ' + (maxpage - 1) + '})"') + '><span>&raquo;</span></a></li>';
        pagination = string_page;
    }
    return {
        datainfo: datainfo,
        pagination: pagination
    };
}

function lookuptablebodyonclick(t, id, val) {
    if ($(t).hasClass('success')) {
        reportinput.find(x => x.id === lookuptargetid).hiddenvalue = id;
        $('#' + lookuptargetid).val(val);
        $('#' + lookupactive).modal('hide');
    } else {
        $(t).closest('tbody').find('.success').removeClass('success');
        $(t).addClass('success');
    }
}

function openmodallookup(type, id) {
    lookupactive = type;
    lookuptargetid = id;
    if (type === 'NRM') {
        lookupactive = 'lookuppasien';
    } else if (type === 'OBAT') {
        lookupactive = 'lookupobat';
    } else if (type === 'DOKTER') {
        lookupactive = 'lookupdokter';
    } else if (type === 'PERAWAT') {
        lookupactive = 'lookupPerawat';
    } else if (type === 'SECTION') {
        lookupactive = 'lookupsection';
    } else if (type === 'BARANG') {
        lookupactive = 'lookupbarang';
    }
    $('#' + lookupactive + ' tbody .success').removeClass('success');
    $('#' + lookupactive).modal();
}

//============================================================================ F U N C T I O N

function activereportinput(documentview) {
    if (!editing) {
        $.each(reportinput, function (index, value) {
            if (!value.active) {
                if (value.type === 'IMAGE' && value) {
                    var canvas = $('#' + value.id).closest('.report-input').find('canvas')[0];
                    value.model = new iPainter(canvas, documentview);
                    value.active = true;
                }
            }
        });
    }
}

function getreporteditordata() {
    var array = [];
    $.each($('#reporteditor .report-input'), function (index, value) {
        var id = $(value).find('[id][name]').attr('id');
        var m = {
            id: id,
            style: $(value).attr('style'),
            html: $(value).html()
        };
        array.push(m);
    });
    return array;
}

//============================================================================ L I B

class iPainter {
    constructor(canvas, documentview = false, options = {}) {
        canvas.setAttribute('width', canvas.style.width);
        canvas.setAttribute('height', canvas.style.height);
        this.canvas = canvas;
        this.inputtext;
        this.fontsize;
        this.context = this.canvas.getContext('2d');
        this.color = options.color || "red";
        this.line = options.line || 3;
        this.mousepressed = false;
        this.lastx;
        this.lasty;

        var t = this;

        this.img = $(this.canvas).closest('.report-input').find('.report-image-img')[0];
        $(this.img).on("load", function () {
            t.context.drawImage(t.img, 0, 0, t.canvas.width, t.canvas.height);
        });

        if (!documentview) {
            this.canvas.addEventListener('mousedown', function (e) {
                if ((t.inputtext || '') === '') {
                    t.mousepressed = true;
                    Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
                } else {
                    t.context.fillStyle = t.color;
                    t.context.font = 'normal ' + t.fontsize + 'px sans-serif';
                    t.context.fillText(t.inputtext, e.pageX - $(this).offset().left, e.pageY - $(this).offset().top);
                    t.inputtext = '';
                }
            });
            this.canvas.addEventListener('mousemove', function (e) {
                if (t.mousepressed) {
                    Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
                }
            });
            this.canvas.addEventListener('mouseup', function (e) {
                t.mousepressed = false;
            });
            this.canvas.addEventListener('mouseleave', function (e) {
                t.mousepressed = false;
            });
        }

        function Draw(x, y, isdown) {
            if (isdown) {
                t.context.beginPath();
                t.context.strokeStyle = t.color;
                t.context.lineWidth = t.line;
                t.context.lineJoin = "round";
                t.context.moveTo(t.lastx, t.lasty);
                t.context.lineTo(x, y);
                t.context.closePath();
                t.context.stroke();
            }
            t.lastx = x; t.lasty = y;
        }
    }
    changecolor(color) {
        this.color = color || "#df4b26";
    }
    clear() {
        this.clickX = new Array();
        this.clickY = new Array();
        this.clickDrag = new Array();
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    }
    text(t) {
        this.inputtext = $(t).closest('.report-input').find('.report-image-text').val() || '';
        this.fontsize = $(t).closest('.report-input').find('.report-image-fontsize').val() || 14;
        this.line = $(t).find('option:selected').data('line') || 3;
    }
    convert() {
        var ImageData = this.canvas.toDataURL("image/png");
        return ImageData.replace(/^data:image\/[a-z]+;base64,/, "");
    }
}

//============================================================================ R E P O R T

function reportvieweronsubmit(t, e) {
    e.preventDefault();
    $(t).find('[type="submit"]').attr('disabled', 'disabled');
    var datas = [];
    $.each($('#formreportviewer').find('.report-input [id][name]'), function (index, value) {
        var v = '';
        var hv;
        var type = gettypeinput(value);
        if (type === 'TEXT' || type === 'TEXTAREA' || type === 'DATE' || type === 'TIME' || type === 'NUMBER')
            v = value.value;
        else if (type === 'CHECKBOX' || type === 'RADIO')
            v = value.checked.toString();
        else if (type === 'SELECT') {
            var option = $(value).find('option:selected');
            v = option.length > 0 ? option.val() : null;
        }
        else if (type === 'IMAGE')
            v = reportinput.find(x => x.id === value.id).model.convert();
        else if (type === 'NRM' || type === 'PERAWAT' || type === 'DOKTER' || type === 'SECTION' || type === 'BARANG') {
            v = value.value;
            hv = reportinput.find(x => x.id === value.id).hiddenvalue;
        }
        var data = {
            Id_Input: value.id,
            Type: type,
            Nama: value.name,
            Value: v,
            HiddenValue: hv
        };
        datas.push(data);
    });
    $.ajax({
        url: urlsave,
        method: 'POST',
        data: { reportid: reportid, nobukti: reportnobukti, datas: datas },
        beforeSend: function () { },
        success: function (r) {
            r = JSON.parse(r);
            if (r.IsSuccess) {
                reportnobukti = r.No;
                if (nextreport) {
                    loadreport(nextreport, reportnobukti, defaultvalue);
                    window.scrollTo(0, 0);
                }
            } else {
                alert(r.Message);
            }
        },
        error: function (xhr) { alert(xhr.statusText); },
        complete: function () { $(t).find('[type="submit"]').removeAttr('disabled'); }
    });
}

function loadreport(designid, nobukti, dvalue, target = 'reportviewer', documentview = false) {
    $.ajax({
        url: urlloaddesign,
        method: 'POST',
        data: { reportid: designid },
        beforeSend: function () { },
        success: function (r) {
            r = JSON.parse(r);
            if (r.IsSuccess) {
                openreport(r, r.Header.JavaScript, target);
                reportid = r.Header.Id;
                defaultvalue = dvalue;
                loadreportvalues(designid, nobukti, target, documentview);
                nextreport = r.NextReport;
                if (nextreport) {
                    $('#formreportviewer [type="submit"] text').html("Simpan Dan Lanjutkan");
                } else {
                    $('#formreportviewer [type="submit"] text').html("Simpan");
                }
            } else {
                alert(r.Message);
            }
        },
        error: function (xhr) { alert(xhr.statusText); },
        complete: function () { }
    });
}

function loadreportvalues(designid, nobukti, target, documentview) {
    $.ajax({
        url: urlloadvalues,
        method: 'POST',
        data: {
            nodesign: designid,
            nobukti: nobukti,
            defaultvalue: defaultvalue
        },
        beforeSend: function () { },
        success: function (r) {
            r = JSON.parse(r);
            if (r.IsSuccess) {
                if (r.Data === null) {
                    reportnobukti = nobukti;
                    if (r.DefaultValue !== null) {
                        for (var i = 0; i < r.DefaultValue.length; i++) {
                            setinputvalue(r.DefaultValue[i].Text, r.DefaultValue[i].Value, r.DefaultValue[i].HiddenValue);
                        }
                    }
                } else {
                    reportnobukti = r.Data.NoBukti;
                    if (r.Detail !== null) {
                        for (var j = 0; j < r.Detail.length; j++) {
                            setinputvalue(r.Detail[j].Id_Input, r.Detail[j].Value, r.Detail[j].HiddenValue);
                        }
                    }
                }
                activereportinput(documentview);
                if (documentview) {
                    $.each($('#' + target + ' .report-input [id][name]'), function (index, value) {
                        var t = gettypeinput(value);
                        if (t === 'TEXT' || t === 'NUMBER' || t === 'DATE' || t === 'TIME' || t === 'TEXTAREA' || t === 'SELECT') {
                            $(value).css('border', 'none');
                            $(value).attr('readonly', 'readonly');
                        } else if (t === 'NRM' || t === 'PERAWAT' || t === 'DOKTER' || t === 'SECTION' || t === 'BARANG') {
                            $(value).css('border', 'none');
                            $(value).attr('readonly', 'readonly');
                            $(value).closest('.report-input').find('button').css('display', 'none');
                        } else if (t === 'CHECKBOX' || t === 'RADIO') {
                            $(value).css('display', 'none');
                            if (value.checked) {
                                $(value).closest('.report-input').append('<label style="font-size:' + $(value).css('height') + ';">✔</label>');
                            }
                        } else if (t === 'IMAGE') {
                            $(value).closest('.report-input').css('border', 'none');
                            $(value).closest('.report-input').find('.report-image-fontsize').css('display', 'none');
                            $(value).closest('.report-input').find('.report-image-text').css('display', 'none');
                            $(value).closest('.report-input').find('button').css('display', 'none');
                        }
                    });
                }

            } else {
                alert(r.Message);
            }
        },
        error: function (xhr) { alert(xhr.statusText); },
        complete: function () { }
    });
}

function openreport(data, js, target) {
    $('#labelopenkode').html("report : " + data.Header.KodeReport);
    $('#labelopenhalaman').html("halaman : " + data.Header.Halaman);
    var reportviewer = $('#' + target);
    reportviewer.attr('class', '');
    reportviewer.addClass('report-div');
    reportviewer.addClass('report-' + data.Header.Kertas + (data.Header.Landscape ? '-landscape' : ''));
    reportviewer.html('');
    reportviewer.append('<div class="report-back-image report-back-image-' + data.Header.Kertas + (data.Header.Landscape ? '-landscape' : '') + '" style="background-image:url(' + urlcontentreport + '/' + data.Header.LokasiGambar + ')"></div>');
    $.each(data.Detail, function (index, value) {
        var html = '<div class="report-input" style="' + value.Style + '">';
        html += value.Html;
        html += '</div>';
        reportviewer.append(html);
    });
    var sc = '\x3Cscript type="text/javascript">';
    sc += 'function runcustomjavascript(){';
    sc += js;
    sc += '}';
    sc += '\x3C/script>';
    reportviewer.append(sc);
    runcustomjavascript();
}